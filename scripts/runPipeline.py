#!/usr/bin/env python
#
# Run CLUSEAN for all *.embl or *.gbk files in the current directory

logfile = open("cluseanrun.log","w")

import sys
import os
from os import path
from optparse import OptionParser

logfile.write("Adding folders to os path environment\n")
if not os.environ.has_key('CLUSEANROOT'):
    os.environ['CLUSEANROOT'] = path.dirname(path.dirname(path.abspath(__file__)))

if not os.environ.has_key('PERL5LIB'):
    os.environ['PERL5LIB'] = os.environ['CLUSEANROOT']
    os.environ['PERL5LIB'] += os.pathsep + path.join(os.environ['CLUSEANROOT'], "perllibs")
else:
    os.environ['PERL5LIB'] += os.pathsep + os.environ['CLUSEANROOT']
    os.environ['PERL5LIB'] += os.pathsep + path.join(os.environ['CLUSEANROOT'], "perllibs")

os.environ['PATH'] += os.pathsep + os.environ['CLUSEANROOT']
os.environ['PATH'] += os.pathsep + path.join(os.environ['CLUSEANROOT'], "bin")
os.environ['BLASTDB'] = path.join(os.environ['CLUSEANROOT'], "db")
sys.path.append(os.environ['CLUSEANROOT'])
logfile.write("Importing modules\n")
from CLUSEAN.parseconfig import ParseConfig, ConfigParserError
from CLUSEAN.pipeline import Pipeline
from CLUSEAN.check_prereqs import check_prereqs
from CLUSEAN.prepare_analysis import prepare_analysis
from CLUSEAN.calculate_annotation import calculate_annotation
from CLUSEAN.annotate import annotate, annotate_antismash
from CLUSEAN.cluster_finder import cluster_finder
from CLUSEAN.cleanup import cleanup
from CLUSEAN.utils import status_to_file, CluseanError, error_page_template

# Usage overview, currently there's no required arguments
usage = "%prog [options]"

# Version
version = "%prog 1.9"

def register_functions(pipeline):
    """Register functions that should run as part of the pipeline"""
    pipeline.register(check_prereqs)
    pipeline.register(prepare_analysis)
    pipeline.register(calculate_annotation)
    pipeline.register(annotate)
    pipeline.register(cluster_finder)
    pipeline.register(annotate_antismash)
    pipeline.register(cleanup)

def handle_clusean_error(error, step, desc):
    """Create a nice HTML error page for the web frontend"""
    handle = None
    try:
        handle = open('error.html', 'w')
        error_vars = {}
        error_vars['step'] = str(step)
        error_vars['description'] = desc
        error_vars['message'] = str(error)
        handle.write(error_page_template % error_vars)
    finally:
        if handle is not None:
            handle.close()
        print >>sys.stderr, "Run failed at step %s (%s): %s" % (step, desc, error)
        sys.exit(step)

def main():
    """Run CLUSEAN for all suitable input files

    Currently, suitable input files are EMBL (*.embl) and GenBank (*gbk) files.
    The pipeline runs in steps creating intermediate files for reference.
    In the end, for every input file foo.embl or bar.gbk, there will be a file
    called foo.final.embl or bar.final.embl with the full annotations.

    NOTE: Regardless of the type of the input file, the intermediate and final
    files always are EMBL files.
    """
    pipeline = Pipeline()
    register_functions(pipeline)

    parser = OptionParser(usage=usage, version=version)
    parser.add_option("-s", "--start-at", dest="start_at", type="int",
                      help="start the pipeline at a certain step",
                      default=1)
    parser.add_option("-u", "--until", dest="until", type="int",
                      help="run the pipeline up to a certain step",
                      default=pipeline.count())
    parser.add_option("-c", "--continue", dest="cont", action="store_true",
                      help="keep existing data instead of overwriting it")
    parser.add_option("-l", "--list-steps", dest="list_steps",
                      action="store_true", help="list available steps")
    parser.add_option("-C", "--configfile", dest="configfile",
                      help="Specify the configuration file to use",
                      default=os.environ['CLUSEANROOT'] + os.sep + 'etc' + \
                              os.sep + "CLUSEAN.cfg")
    parser.add_option("--without-blast", dest="without_blast",
                      help="Don't run full-genome-blast", action="store_true",
                      default=False)
    parser.add_option("--without-hmmer", dest="without_hmmer",
                      help="Don't run full-genome-hmmer", action="store_true",
                      default=False)
    parser.add_option("--cpus", dest="cpus", type="int",
                      help="set maximum number of CPUs to use",
                      default=0)
    parser.add_option("--blastdbpath", dest="blastdbpath",
                      help="path to the blast database files",
                      default=None)
    parser.add_option("--pfamdbpath", dest="pfamdbpath",
                      help="path to HMMer profiles",
                      default=None)
    parser.add_option("--statusfile", dest="statusfile",
                      help="path to status file for we frontend",
                      default=None)
    (options, args) = parser.parse_args()


    if options.list_steps:
        print pipeline.list_steps()
        sys.exit(0)

    if options.until > pipeline.count() + 1:
        options.until = pipeline.count() + 1
    elif options.until < 0:
        options.until = 0
#    defaults = {'Environment': {'BLASTDB': os.environ['BLASTDB'],
#                                'HMMERDB': path.join(os.environ['CLUSEANROOT'], "db")}}
    pc = ParseConfig(interpolate=True)
    pc.parse_file(options.configfile)

    if options.without_blast:
        pc.config['Environment']['RUN_BLAST'] = "false"

    if options.without_hmmer:
        pc.config['Environment']['RUN_HMMER'] = "false"

    if options.cpus > 0:
        pc.config['Environment']['CPUS'] = str(options.cpus)

    if options.blastdbpath:
        pc.config['Environment']['BLASTDB'] = options.blastdbpath
        os.environ['BLASTDB'] = options.blastdbpath

    if options.pfamdbpath:
        pc.config['Environment']['HMMERDB'] = options.pfamdbpath

    # Call all the pipeline functions
    for step in xrange(options.start_at, options.until+1):
        func = pipeline.get_step(step)
        status_to_file(options, func.func_doc)
        try:
            retcode = func(step, pc.config)
            if retcode != 0:
                logfile.write("Run failed at step %s (%s)" % (step, func.func_name))
                raise CluseanError("Unknown failure")
        except CluseanError, e:
            handle_clusean_error(e, step, func.func_doc)
        step += 1

if __name__ == "__main__":
    logfile.write("Running CLUSEAN pipeline...\n")
    main()

