#!/usr/bin/env python
#
# Test pipeline helper for CLUSEAN
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>

import sys
from nose.tools import *

sys.path.append('.')
sys.path.append('..')
from CLUSEAN.pipeline import *

def _dummy(step, options):
    """Dummy function for testing"""
    return 0

def test_register():
    p = Pipeline()
    assert_equal(p.functions, [])
    p.register(_dummy)
    assert_equal(p.functions, [_dummy])

def test_list_steps():
    p = Pipeline()
    p.register(_dummy)
    exp = """Available steps in the pipeline:
Step 1\t                              dummy:\tDummy function for testing
"""
    assert_equal(p.list_steps(), exp)

def test_count():
    p = Pipeline()
    p.register(_dummy)
    assert_equal(p.count(), 1)

def test_get_step():
    p = Pipeline()
    p.register(_dummy)
    assert_equal(p.get_step(1), _dummy)
    assert_raises(IndexError, p.get_step, 2)
