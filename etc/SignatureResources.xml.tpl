<?xml version="1.0" encoding="UTF-8"?>

<resource xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<PKSI_KR_Stereo type="prediction">
		<Prerequisite>
			<primary_tag_type>CDS_motif</primary_tag_type>
			<tag>label</tag>
			<tag_value>PKSI-KR</tag_value>
		</Prerequisite>
		<Execute program="hmmpfam" CaptureConsole="TRUE">
			<!--Currently, the location of the hmmpfam binary and the database location 
				is inferred from the CLUSEAN.cfg configuration file! Maybe in a future version 
				we might want to put it here... -->
			<!-- If prefixes are required you can set them here <inputfileprefix></inputfileprefix> 
				<outputfileprefix></outputfileprefix> <dbprefix></dbprefix> -->
			<parameters>--cpu 1</parameters>
			<database>PKSI-KR.hmm</database><evalue>0.1</evalue>
			<score>0</score>
		</Execute>
		<Alignment>
			<scaffold>
				<scaffoldOffset>149, 162, 166</scaffoldOffset>
				<scaffoldValue>S,Y,N</scaffoldValue>
			</scaffold>
			<choice result="D">
				<offset>102</offset>
				<value>D</value>
				<comment>KR domain putatively catalyzing D-conformation product formation</comment>
			</choice>
			<choice result="L">
				<offset>102</offset>
				<value>^D</value>
				<comment>KR domain putatively catalyzing L-conformation product formation</comment>
			</choice>
		</Alignment>
		<description>Pediction of PKSI KR specificities according to Reid et al., Biochemistry 2003, 42, 72-79</description>
		<referenceList>
			<reference>Reid, R., M. Piagentini, E. Rodriguez, G. Ashley, N. Viswanathan, J. Carney, D. V. Santi, C. R. Hutchinson, and R. McDaniel. 2003. A model of structure and catalysis for ketoreductase domains in modular polyketide synthases. Biochemistry 42:72-79.</reference>
		</referenceList>
	</PKSI_KR_Stereo>
	<ASP_KS type="prediction">
		<Prerequisite>
			<primary_tag_type>CDS_motif</primary_tag_type>
			<tag>label</tag>
			<tag_value>PKSI-KS</tag_value>
		</Prerequisite>
		<Execute program="hmmpfam" CaptureConsole="TRUE">
			<!--Currently, the location of the hmmpfam binary and the database location 
				is inferred from the CLUSEAN.cfg configuration file! Maybe in a future version 
				we might want to put it here... -->
			<!-- If prefixes are required you can set them here <inputfileprefix></inputfileprefix> 
				<outputfileprefix></outputfileprefix> <dbprefix></dbprefix> -->
			<parameters>--cpu 1</parameters>
			<database>PKSI-KS_N.hmm</database><evalue>0.1</evalue>
			<score>0</score></Execute>
		<Alignment>
			<scaffold>
				<scaffoldOffset>176,186,187,188</scaffoldOffset>
				<scaffoldValue>G,S,S,S</scaffoldValue>
				<scaffoldEmission>0.99,.0.9,0.81,0.81</scaffoldEmission>
			</scaffold>
			<choice result="Active site cysteine">
				<offset>185</offset>
				<value>C</value>
				<valueEmission>0.98</valueEmission>
				<comment>Active site cysteine</comment>
			</choice>
		</Alignment>
		<description>KS active site cysteine according to Huang et al Microbiology 147 (2001), 631-642</description>
		<referenceList>
			<reference>Guozhong Huang, Lianhui Zhang and Robert G. Birch, A multifunctional polyketide–peptide synthetase essential for albicidin biosynthesis in Xanthomonas albilineans, Microbiology 147 (2001), 631-642</reference>
		</referenceList>
	</ASP_KS>
	<ASP_KS_C type="prediction">
		<Prerequisite>
			<primary_tag_type>CDS_motif</primary_tag_type>
			<tag>label</tag>
			<tag_value>PKSI-KS_C</tag_value>
		</Prerequisite>
		<Execute program="hmmpfam" CaptureConsole="TRUE">
			<!--Currently, the location of the hmmpfam binary and the database location 
				is inferred from the CLUSEAN.cfg configuration file! Maybe in a future version 
				we might want to put it here... -->
			<!-- If prefixes are required you can set them here <inputfileprefix></inputfileprefix> 
				<outputfileprefix></outputfileprefix> <dbprefix></dbprefix> -->
			<parameters>--cpu 1</parameters>
			<database>PKSI-KS_C.hmm</database><evalue>0.1</evalue>
			<score>0</score>
		</Execute>
		<Alignment>
			<scaffold>
				<scaffoldOffset>47,50,51,52,53,56,57,60,106,110,117,123</scaffoldOffset>
				<scaffoldValue>E,G,T,G,T,G,D,E,K,G,G,K</scaffoldValue>
				<scaffoldEmission>0.96,0.92,0.95,0.94,0.95,0.96,0.99,0.99,1,0.99,0.96,0.93</scaffoldEmission>
			</scaffold>
			<choice result="Active site histidine">
				<offset>49</offset>
				<value>H</value>
				<valueEmission>0.95</valueEmission>
				<comment>Active site his</comment>
			</choice>
			<choice result="Active site histidine2">
				<offset>111</offset>
				<value>H</value>
				<valueEmission>1</valueEmission>
				<comment>Active site his</comment>
			</choice>
		</Alignment>
		<description>KS active site Histidine according to Huang et al Microbiology 147 (2001), 631-642</description>
		<referenceList>
			<reference>Guozhong Huang, Lianhui Zhang and Robert G. Birch, A multifunctional polyketide–peptide synthetase essential for albicidin biosynthesis in Xanthomonas albilineans, Microbiology 147 (2001), 631-642</reference>
		</referenceList>
	</ASP_KS_C>
	<ASP_AT type="prediction">
		<Prerequisite>
			<primary_tag_type>CDS_motif</primary_tag_type>
			<tag>label</tag>
			<tag_value>PKSI-AT</tag_value>
		</Prerequisite>
		<Execute program="hmmpfam" CaptureConsole="TRUE">
			<!--Currently, the location of the hmmpfam binary and the database location 
				is inferred from the CLUSEAN.cfg configuration file! Maybe in a future version 
				we might want to put it here... -->
			<!-- If prefixes are required you can set them here <inputfileprefix></inputfileprefix> 
				<outputfileprefix></outputfileprefix> <dbprefix></dbprefix> -->
			<parameters>--cpu 1</parameters>
			<database>PKSI-AT.hmm</database><evalue>0.1</evalue>
			<score>0</score>
		</Execute>
		<Alignment>
			<scaffold>
				<scaffoldOffset>93,94,97,98</scaffoldOffset>
				<scaffoldValue>G,H,G,E</scaffoldValue>
				<scaffoldEmission>1,1,1,0.94</scaffoldEmission>
			</scaffold>
			<choice result="Active site serine">
				<offset>95</offset>
				<value>S</value>
				<valueEmission>1</valueEmission>
				<comment>Active site serine</comment>
			</choice>
		</Alignment>
		<description>citation missing</description>
		<referenceList>
			<reference>citation missing</reference>
		</referenceList>
	</ASP_AT>
	<ASP_ACP type="prediction">
		<Prerequisite>
			<primary_tag_type>CDS_motif</primary_tag_type>
			<tag>label</tag>
			<tag_value>PKSI-ACP</tag_value>
		</Prerequisite>
		<Execute program="hmmpfam" CaptureConsole="TRUE">
			<!--Currently, the location of the hmmpfam binary and the database location 
				is inferred from the CLUSEAN.cfg configuration file! Maybe in a future version 
				we might want to put it here... -->
			<!-- If prefixes are required you can set them here <inputfileprefix></inputfileprefix> 
				<outputfileprefix></outputfileprefix> <dbprefix></dbprefix> -->
			<parameters>--cpu 1</parameters>
			<database>PKSI-ACP.hmm</database><evalue>0.1</evalue>
			<score>0</score>
		</Execute>
		<Alignment>
			<scaffold>
				<scaffoldOffset>28,30</scaffoldOffset>
				<scaffoldValue>G,D</scaffoldValue>
				<scaffoldEmission>0.97,0.9</scaffoldEmission>
			</scaffold>
			<choice result="Active site serine">
				<offset>31</offset>
				<value>S</value>
				<valueEmission>0.98</valueEmission>
				<comment>Active site serine</comment>
			</choice>
		</Alignment>
		<description>ACP active site serine according to Huang et al Microbiology 147 (2001), 631-642</description>
		<referenceList>
			<reference>Guozhong Huang, Lianhui Zhang and Robert G. Birch, A multifunctional polyketide–peptide synthetase essential for albicidin biosynthesis in Xanthomonas albilineans, Microbiology 147 (2001), 631-642</reference>
		</referenceList>
	</ASP_ACP>
	<ASP_PKSI-DH type="prediction">
		<Prerequisite>
			<primary_tag_type>CDS_motif</primary_tag_type>
			<tag>label</tag>
			<tag_value>PKSI-DH</tag_value>
		</Prerequisite>
		<Execute program="hmmpfam" CaptureConsole="TRUE">
			<!--Currently, the location of the hmmpfam binary and the database location 
				is inferred from the CLUSEAN.cfg configuration file! Maybe in a future version 
				we might want to put it here... -->
			<!-- If prefixes are required you can set them here <inputfileprefix></inputfileprefix> 
				<outputfileprefix></outputfileprefix> <dbprefix></dbprefix> -->
			<parameters>--cpu 1</parameters>
			<database>PKSI-DH.hmm</database><evalue>0.1</evalue>
			<score>0</score>
		</Execute>
		<Alignment>
			<scaffold>
				<scaffoldOffset>4,5,6,30,32,34</scaffoldOffset>
				<scaffoldValue>L,L,G,P,L,D</scaffoldValue>
				<scaffoldEmission>0.9,0.78,0.86,0.7,0.85,0.78</scaffoldEmission>
			</scaffold>
			<choice result="Catalytic triade">
				<offset>35,39,44</offset>
				<value>H,G,P</value>
				<valueEmission>1,0.62,0.97</valueEmission>
				<comment>Catalytic triade</comment>
			</choice>
		</Alignment>
		<description>citation missing</description>
		<referenceList>
			<reference>citation missing</reference>
		</referenceList>
	</ASP_PKSI-DH>
	<ASP_PKSI-KR type="prediction">
		<Prerequisite>
			<primary_tag_type>CDS_motif</primary_tag_type>
			<tag>label</tag>
			<tag_value>PKSI-KR</tag_value>
		</Prerequisite>
		<Execute program="hmmpfam" CaptureConsole="TRUE">
			<!--Currently, the location of the hmmpfam binary and the database location 
				is inferred from the CLUSEAN.cfg configuration file! Maybe in a future version 
				we might want to put it here... -->
			<!-- If prefixes are required you can set them here <inputfileprefix></inputfileprefix> 
				<outputfileprefix></outputfileprefix> <dbprefix></dbprefix> -->
			<parameters>--cpu 1</parameters>
			<database>PKSI-KR.hmm</database><evalue>0.1</evalue>
			<score>0</score>
		</Execute>
		<Alignment>
			<scaffold>
				<scaffoldOffset>33,65,92,97,120,148</scaffoldOffset>
				<scaffoldValue>R,D,G,A,K,S</scaffoldValue>
				<scaffoldEmission>0.93,0.88,0.93,0.9,0.99,0.87</scaffoldEmission>
			</scaffold>
			<choice result="Active site">
				<offset>149,162,166</offset>
				<value>S,Y,N</value>
				<valueEmission>0.98,0.99,0.83</valueEmission>
				<comment>Active site</comment>
			</choice>
		</Alignment>
		<description>Active site prediction according to Reid et al Biochemistry 42:72-79 (2003)</description>
		<referenceList>
			<reference>Reid, R., M. Piagentini, E. Rodriguez, G. Ashley, N. Viswanathan, J. Carney, D. V. Santi, C. R. Hutchinson, and R.
				McDaniel. 2003. A model of structure and catalysis for ketoreductase domains in modular polyketide synthases. Biochemistry 42:72-79.</reference>
		</referenceList>
	</ASP_PKSI-KR>
	<ASP_Thioesterase type="prediction">
		<Prerequisite>
			<primary_tag_type>CDS_motif</primary_tag_type>
			<tag>label</tag>
			<tag_value>Thioesterase</tag_value>
		</Prerequisite>
		<Execute program="hmmpfam" CaptureConsole="TRUE">
			<!--Currently, the location of the hmmpfam binary and the database location 
				is inferred from the CLUSEAN.cfg configuration file! Maybe in a future version 
				we might want to put it here... -->
			<!-- If prefixes are required you can set them here <inputfileprefix></inputfileprefix> 
				<outputfileprefix></outputfileprefix> <dbprefix></dbprefix> -->
			<parameters>--cpu 1</parameters>
			<database>Thioesterase.hmm</database><evalue>0.1</evalue>
			<score>0</score>
		</Execute>
		<Alignment>
			<scaffold>
				<scaffoldOffset>73,79,83,87,108</scaffoldOffset>
				<scaffoldValue>G,G,G,A,D</scaffoldValue>
				<scaffoldEmission>0.93, 1, 0.99, 0.9, 0.9</scaffoldEmission>
			</scaffold>
			<choice result="active site serine">
				<offset>81</offset>
				<value>S</value>
				<valueEmission>0.93</valueEmission>
				<comment>active site serine</comment>
			</choice>
		</Alignment>
		<description>citation missing</description>
		<referenceList>
			<reference>citation missing</reference>
		</referenceList>
	</ASP_Thioesterase>
	<PKSI_ER_Stereo type="prediction">
		<Prerequisite>
			<primary_tag_type>CDS_motif</primary_tag_type>
			<tag>label</tag>
			<tag_value>PKSI-ER</tag_value>
		</Prerequisite>
		<Execute program="hmmpfam" CaptureConsole="TRUE">
			<!--Currently, the location of the hmmpfam binary and the database location 
				is inferred from the CLUSEAN.cfg configuration file! Maybe in a future version 
				we might want to put it here... -->
			<!-- If prefixes are required you can set them here <inputfileprefix></inputfileprefix> 
				<outputfileprefix></outputfileprefix> <dbprefix></dbprefix> -->
			<parameters>--cpu 1</parameters>
			<database>PKSI-ER.hmm</database><evalue>0.1</evalue>
			<score>0</score>
		</Execute>
		<Alignment>
			<scaffold>
				<scaffoldOffset>31,135,137,144,146,227</scaffoldOffset>
				<scaffoldValue>D,L,H,G,A,G</scaffoldValue>
			</scaffold>
			<choice result="2S">
				<offset>39</offset>
				<value>Y</value>
				<comment>ER domain putatively catalyzing 2S-configuration product formation</comment>
			</choice>
			<choice result="2R">
				<offset>39</offset>
				<value>^Y</value>
				<comment>ER domain putatively catalyzing 2R-configuration product formation</comment>
			</choice>
		</Alignment>
		<description>Pediction of PKSI ER specificities according to Kwan et al. ChemBiol 15:1231-1240, 2008</description>
		<referenceList>
			<reference> Kwan, D. H., Y. Sun, F. Schulz, H. Hong, B. Popovic, J. C. Sim-Stark, S. F. Haydock, and P. F. Leadlay. 2008. Prediction and Manipulation of the Stereochemistry of Enoylreduction in Modular Polyketide Synthases. Chem.Biol 15:1231-1240.
    	</reference>
		</referenceList>
	</PKSI_ER_Stereo>

	<PKSI_AT_Spec type="prediction">
		<Prerequisite>
			<primary_tag_type>CDS_motif</primary_tag_type>
			<tag>label</tag>
			<tag_value>PKSI-AT.*</tag_value>
		</Prerequisite>
		<Execute program="hmmpfam" CaptureConsole="TRUE">
			<!--Currently, the location of the hmmpfam binary and the database location 
				is inferred from the CLUSEAN.cfg configuration file! Maybe in a future version 
				we might want to put it here... -->
			<!-- If prefixes are required you can set them here <inputfileprefix></inputfileprefix> 
				<outputfileprefix></outputfileprefix> <dbprefix></dbprefix> -->
			<parameters>--cpu 1</parameters>
			<database>PKSI-AT.hmm</database><evalue>0.1</evalue>
			<score>0</score>
		</Execute>
		<Alignment>
			<scaffold>
				<scaffoldOffset>93,94,95,120,196,198,199,227,244,245</scaffoldOffset>
				<scaffoldVal>G,H,S,R,A,H,S,S,Y,W</scaffoldVal>
			</scaffold>
			<choice result="Malonyl-CoA specific">
				<offset>195,197</offset>
				<value>H,F</value>
				<comment>AT domain specific for malonyl-CoA</comment>
			</choice>
			<choice result="Methylmalonyl-CoA specific">
				<offset>195,197</offset>
				<value>Y,S</value>
				<comment>AT domain specific for methylmalonyl-CoA</comment>
			</choice>
		</Alignment>
		<description>Pediction of PKSI AT specificities according to Del Vecchio et al., 1999, J Ind. Microbiol Biotechnol 30:489-494.</description>
		<referenceList>
			<reference>Del Vecchio, F., H. Petkovic, S. G. Kendrew, L. Low, B. Wilkinson, R. Lill, J. Cortes, B. A. Rudd, J. Staunton, and P. F. Leadlay. 2003. Active-site residue, domain and module swaps in modular polyketide synthases. J Ind. Microbiol Biotechnol	30:489-494.</reference>
		</referenceList>
	</PKSI_AT_Spec>

	<StachelhausExtract type="ResidueExtract">
		<Prerequisite>
			<primary_tag_type>CDS_motif</primary_tag_type>
			<tag>label</tag>
			<tag_value>NRPS-A-domain</tag_value>
		</Prerequisite>
		<Execute program="hmmpfam" CaptureConsole="TRUE">
			<!--Currently, the location of the hmmpfam binary and the database location 
				is inferred from the CLUSEAN.cfg configuration file! Maybe in a future version 
				we might want to put it here... -->
			<!-- If prefixes are required you can set them here <inputfileprefix></inputfileprefix> 
				<outputfileprefix></outputfileprefix> <dbprefix></dbprefix> -->
			<parameters>--cpu 1</parameters>
			<database>aa-activating.aroundLys.hmm</database><evalue>0.1</evalue>
			<score>0</score>
		</Execute>
		<Alignment>
			<scaffold>
				<scaffoldOffset>47,48,51,93,125,127,154,162,163</scaffoldOffset>
				<scaffoldVal>D,A,W,T,I,A,A,I,C</scaffoldVal>
			</scaffold>
		</Alignment>
		<description>Extracts specificity conferring amino acids according to Stachelhaus et al. 1999</description>
		<referenceList>
			<reference>Stachelhaus, T., H. D. Mootz, and M. A. Marahiel. 1999. The specificity-conferring code of adenylation domains in nonribosomal peptide synthetases. Chem. Biol. 6:493-505.</reference>
		</referenceList>
	</StachelhausExtract>
</resource>
