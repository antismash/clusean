# Central configuration file for all CLUSEAN scripts and modules
# All user configurable settings should be made here and not in the
# perl scripts and modules (otherwise blame the author aka me...)

# THESE SETTINGS MUST BE EDITED ACCORDING TO YOUR SYSTEM'S CONFIGURATION!
# ESSENTIAL SETTINGS

# Root directory for all CLUSEAN scripts
# Note: Can be overridden by CLUSEANROOT environment variable!!
CLUSEANROOT = __CLUSEANROOT__

# In Windows environments use this syntax
#CLUSEANROOT = c:/CLUSEAN

# Number of CPUs available for Clusean runs
CPUS = 4

# Set up the paths needed by the shell scripts in the pipeline
<Environment>
    PATH=__BASE_PATH__

    # set directories for blast
    BLASTDB=__BLASTDB_PATH__

    # set directory for HMMer
    HMMERDB=__HMMERDB_PATH__

    PATH=$CLUSEANROOT/NRPSPredictor

    CPUS=__NUM_CPUS__

    RUN_BLAST_KEGG=false

</Environment>

# OPTIONAL SETTINGS

<Formatting>
# String used for concatenation of array results in annotations / tables

Separator = "|"
</Formatting>

# Settings for Blast annotations
# (Changes are only required if you intend to use the --run parameters (not recommended)
<Blast>
    # Path of blastall executable; if blastall executable is not in $PATH add full pathname here
    Exe     = blastall
    # In Windows environments use this syntax:
    # Exe =c:/blast/bin/blastall


    # Cmdline Parameters
    Params  = "-a4 -FF -v 300 -b 300"

    # Database to search; if database is not accessible directly via command line add full pathname here
    DB      = clusean

    # Name of subdirectory
    Dir     = blastp

    # Tag used for reference in EMBL file
    Tag     = blastp_file
</Blast>


# Settings for EC-predicitions annotations
# NOT YET IMPLEMENTED!! Variables hardcoded in assignFunction.pl!!!
#

<ECpred>
    # NOT YET IMPLEMENTED
    # Path of blastall executable; if blastall executable is not in $PATH add full pathname here

    Exe     = blastall
    # In Windows environments use this syntax:
    # Exe =c:/blast/bin/blastall


    # Cmdline Parameters
    Params  = "-a4 -FF -v 300 -b 300"

    # Database to search; if database is not accessible directly via command line add full pathname here
    DB      = kegg

    # Name of subdirectory
    Dir     = kegg_blastp

    # Tag used for reference in EMBL file
    Tag     = ECpred_file

    # Qualifier used for EC-Number annotation
    ECqual  = EC

</ECpred>

# Settings for HMMer annotations
# (Changes are only required if you intend to use the --run parameters (not recommended)
<HMMer>
    # Path of hmmscan executable; if blastall executable is not in $PATH add full pathname here
    Exe        = hmmscan

    # Cmdline parameters
    # Params  = --cpu 2

    # Database to search ; if database is not accessible directly via command line add full pathname here
    DB        = Pfam-A

    # Name of subdirectory
    Dir       = HMMer

    # Tag used for reference in EMBL file
    Tag       = HMMer_file

    # HMMer qualifier used to annotate domains
    HMMerQual = CDS_motif
</HMMer>

# Settings for NRPSPredictor
#
<NRPSPredictor>
    # Path of wrapper executable (if not present in CLUSEANROOT/bin
    # Exe    =  /vol/biosoft/bin/NRPSPredWrap.pl

    # Path to NRPSPredictor executables
    NRPSBinDir = __CLUSEANROOT__/NRPSPredictor
    #NRPSBinDir = /vol/biosoft/NRPSPredictor

    # Name of NRPSPredictor script
    NRPSBin = nrpsSpecPredictor.pl

    # Name of subdirectory to store query and result files
    Dir = NRPSPred

    # Tag used for reference in EMBL file
    Tag = NRPSPred_file

    # Tag used for assigning NRPS
    NRPSLabel = label

    # Qualifier to annotate specificities
    NRPSPredQual = misc_feature
</NRPSPredictor>

# SYSTEM SETTINGS (Change only if you're exactly know what you're doing...)

# Location of the recognition tables, relative to CLUSEANROOT
<RecogTables>
    BlastTab_all    = lib/recogTable.tab
    BlastTab_first  = lib/recogTable.tab
    HMMTab          = lib/HMM-Keywords.tab
</RecogTables>

# Settings for evaluateDomains script
<EvaluateDomains>
    HMMerQual    = CDS_motif        # recommendation: use same tag as in <HMMer> section
    OverlapFrac  = 0.5
</EvaluateDomains>

# Location of SignatureResources XML file
<SignatureResources>
    SignatureResourceFile  = etc/SignatureResources.xml
    DBDir                  = lib
</SignatureResources>

# Location of ProfileResources XML file
<ProfileResources>
    ProfileResourceFile  = etc/ProfileResources.xml
    DBDir                = lib
</ProfileResources>
