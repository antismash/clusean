use strict;
use Bio::SearchIO;

sub digestHmmscan
{
    my ( $hmmscan_file, $seq_ids, $descriptions, $detected_hmms ) = @_;

    my $seq = Bio::SearchIO->new(-format => "hmmer3",
                                 -file   => $hmmscan_file);

    while (my $result = $seq->next_result) {
        push(@$seq_ids, $result->query_name);

        my $description = $result->query_description;

        # Hack to work around the fact that descriptions of queries don't
        # default to '[none]' anymore
        if ($description eq '') {
            $description = '[none]';
        }

        $$descriptions{$result->query_name} = $description;

        if ($result->num_hits() == 0) {
            $$detected_hmms{$result->query_name} = [ "[no hits above thresholds]" ];
            next;
        }
        my @domains;

        while (my $hit = $result->next_hit) {
            my $num_hsps = $hit->num_hsps();
            my $dom_count = 1;

            while (my $hsp = $hit->next_domain) {
                my $hmm_string = "";

                $hmm_string .= $hit->name . "\t\t\t";
                $hmm_string .= "$dom_count\t\t\t$num_hsps\t\t\t";
                $dom_count++;
                $hmm_string .= $hsp->query->start . "\t\t\t";
                $hmm_string .= $hsp->query->end . "\t\t\t";
                $hmm_string .= $hsp->score . "\t\t\t";
                $hmm_string .= $hsp->hit_string . "\t\t\t";
                $hmm_string .= $hsp->query_string;

                #XXX: This kludge compensates for the fact that with hmmer3, the Lys profile sometimes
                #     matches for two domains. The regexes below make sure the hmm string either matches
                #     an AMP domain or the correct domain of the Lys profile.
                if ($hmm_string =~ m/KGVmveHrnvvnlvkwl/ || $hmm_string =~ m/fvvLdalPLTpNGKlDRkALPaPd/) {
                    push(@domains, $hmm_string);
                }
            }
        }
        $$detected_hmms{$result->query_name} = \@domains;

    }
}
1;
