use strict;
use warnings;

use mystatistics;

#Usage of this sub in main program:
#my $featureVector = CODE2features ($scriptHomeDir, $code_8A, "aa-hydrogenbond.aaindex", "aa-hydrophobicity-neu1.aaindex", "aa-hydrophobicity-neu2.aaindex", "aa-hydrophobicity-neu3.aaindex", "aa-polar-zimmerman.aaindex", "aa-polar-radzicka.aaindex", "aa-polar-grantham.aaindex", "aa-volume.aaindex", "aa-beta-turn.aaindex", "aa-beta-sheet.aaindex", "aa-alpha-helix.aaindex", "aa-isoelectric.aaindex");


sub CODE2features {
	my $scriptHomeDir = shift;
	my $code_8A       = shift;
	my @aaindexFiles  = @_;

#----------------------------------------------------------------------------
# Now take the aa-index-files
# create hash %aaHash
# with function readAaindexFileExtendHash (which evaluates the given index-file)
# fill this hash (keys: aa, values: their value according to the aa-index-file)
# function minAndMaxValueOfHash gets the min and max value of the hash
# Note: RESULT:
# return $featureVector_temp;
#----------------------------------------------------------------------------

	my $featureVector_temp;
	
	for ( my $i = 0 ; $i < @aaindexFiles ; $i++ ) {
		testIfFileExistsAndIfOpenable("$scriptHomeDir/$aaindexFiles[$i]");
		my %aaHash;
		my $one_aaindexFile = "$scriptHomeDir/$aaindexFiles[$i]";
		#print "One:$one_aaindexFile\n";
		readAaindexFileExtendHash( \$one_aaindexFile, \%aaHash );

		#=============(done with filling aaHash for current aa-index-file)
	#my $min; #(used in earlier version)
	#my $max;
	#minAndMaxValueOfHash(\%aaHash,\$min, \$max);
	my $mean;
	my $std_dev;
	my $min;
	my $max;
	my @hash_values=values(%aaHash);
	std_dev(\@hash_values, \$mean, \$std_dev, \$min, \$max);

		#print X "Min:$min, Max:$max\n";
		#=============(done: determined min and max of this hash)
		{
			for ( my $j = 0 ; $j < length($code_8A) ; $j++ ) {
				my $char_at_current_position = substr( $code_8A, $j, 1 );
				my $value_temp;
			if (exists $aaHash{$char_at_current_position}) { # if the value is known for this amino acid
				$value_temp = $aaHash{$char_at_current_position};
				}
			else {
				$value_temp=$mean; # else: unknown amino acid or gap sign.
			}
			#normalize interval mean+-std_dev to [-1, +1]
			$value_temp = ($value_temp-$mean)/$std_dev;
				$featureVector_temp .=( 1 + $j + ( $i + 0 ) * length($code_8A) ).":$value_temp "; # for each aa-index-file ($i=1...) and each position in the code
			}
		}
	}
$featureVector_temp=~s/\s+$//g; # remove ending white spaces
$featureVector_temp="0 ".$featureVector_temp."\n"; # needs that ...
return $featureVector_temp;
}


#----------------------------------------------------------------------------
#============================================================================
# SUBs:
#============================================================================
#----------------------------------------------------------------------------
sub printHash {
	my ($hash_reference) = @_;
	foreach my $key ( keys %$hash_reference ) {
		print "Key:" . $key . ", Value:" . $$hash_reference{$key} . "\n";
	}
}

sub minAndMaxValueOfHash {
	my ( $aa2value_hash, $min_reference, $max_reference ) =
	  @_;    # sort the hash's values
	my @values = sort { $a <=> $b } ( values %$aa2value_hash );
	$$min_reference = $values[0];
	$$max_reference = $values[ (@values) - 1 ];
}

sub readAaindexFileExtendHash {
	my ( $aaindexFile, $aa2value_hash ) = @_;
	open( INDEX, $$aaindexFile );
	my @linesOfIndexFile = <INDEX>;
	close(INDEX);

	#my %aa2value_hash;
	for ( my $i = 0 ; $i < @linesOfIndexFile ; $i++ ) {
		chomp( $linesOfIndexFile[$i] );
		unless ( $linesOfIndexFile[$i] =~ /^I\s+/ ) {
			next;
		}    # scroll through the file until reaching the Index section
		$linesOfIndexFile[ $i + 1 ] =~ s/^\s+//;
		$linesOfIndexFile[ $i + 2 ] =~ s/^\s+//;
		my @ARNDCQEGHI = split( /\s+/, $linesOfIndexFile[ $i + 1 ] );
		my @LKMFPSTWYV = split( /\s+/, $linesOfIndexFile[ $i + 2 ] );
		$$aa2value_hash{"A"} = $ARNDCQEGHI[0];
		$$aa2value_hash{"R"} = $ARNDCQEGHI[1];
		$$aa2value_hash{"N"} = $ARNDCQEGHI[2];
		$$aa2value_hash{"D"} = $ARNDCQEGHI[3];
		$$aa2value_hash{"C"} = $ARNDCQEGHI[4];
		$$aa2value_hash{"Q"} = $ARNDCQEGHI[5];
		$$aa2value_hash{"E"} = $ARNDCQEGHI[6];
		$$aa2value_hash{"G"} = $ARNDCQEGHI[7];
		$$aa2value_hash{"H"} = $ARNDCQEGHI[8];
		$$aa2value_hash{"I"} = $ARNDCQEGHI[9];
		$$aa2value_hash{"L"} = $LKMFPSTWYV[0];
		$$aa2value_hash{"K"} = $LKMFPSTWYV[1];
		$$aa2value_hash{"M"} = $LKMFPSTWYV[2];
		$$aa2value_hash{"F"} = $LKMFPSTWYV[3];
		$$aa2value_hash{"P"} = $LKMFPSTWYV[4];
		$$aa2value_hash{"S"} = $LKMFPSTWYV[5];
		$$aa2value_hash{"T"} = $LKMFPSTWYV[6];
		$$aa2value_hash{"W"} = $LKMFPSTWYV[7];
		$$aa2value_hash{"Y"} = $LKMFPSTWYV[8];
		$$aa2value_hash{"V"} = $LKMFPSTWYV[9];

		#print "Intern:".$$aa2value_hash{"A"}."\n";
	}

#return(\%aa2value_hash); # keine Rückgabe möglich, da der Funktion eine Referenz übergeben wurde.
}


1;
