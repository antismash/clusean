#!/usr/bin/perl
#
#
# Usage ICcollectStatistics.pl <Filename>
#
# Generate statistics file for identifyClusters.pl
#
# Filename: Output-File of EMBL2CSV (used with parameter -a -t)
#

use warnings;

# open EMBL2CSV file
open (INFILE, $ARGV[0]) || die "Couldn't open $ARGV[0]!";

# Define global variables

my %hAssigNo;
my %hAssScore;

# now cycle through CSV file

while (my $readln=<INFILE>) {
	chomp ($readln);
	
	# Skip header line
	next if ($readln =~ /^Filename.*/);
	
	# Get Assignment and Assignment Score
	@fields = split (/\|/, $readln);
	$Assignment = $fields[7];
	$AssScore   = $fields[8];
	
	# If haskey is not yet defined add it...
	if (!defined $hAssScore{$Assignment}) {
		$hAssScore{$Assignment} = $AssScore;
		$hAssigNo{$Assignment} = 1;
	}
	
	# otherwise add score and increase number;
	else
	{
		$hAssScore{$Assignment} = $hAssScore{$Assignment} + $AssScore;
		$hAssigNo{$Assignment}++;
	}
}

# now generate output

# we can reuse $Assignment and $AssScore
$Assignment = undef;
$AssScore = undef;

# Write one line for every Assignment
# Assignment, Sum of Assignment scores, No of occurences, mean score

foreach $Assignment (keys %hAssScore) {
	print $Assignment,",",$hAssScore{$Assignment},",",$hAssigNo{$Assignment},",";
	my $meanScore = $hAssScore{$Assignment} / $hAssigNo{$Assignment};
	printf ("%.3f\n", $meanScore);
	
}
