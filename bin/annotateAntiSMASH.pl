#!/usr/bin/perl -w
#
=head1 NAME

annotateAntiSMASH.pl

=head1 SYNOPSIS

annotateAntiSMASH.pl --input filename --table filename --output filename

=head1 DESCRIPTION

Annotate embl file with the antiSMASH results.

=head2 PARAMETERS

=head3
   --input <filename>                :       Input filename
   --table <filename>                :       antiSMASH predictions.
   --output <filename>               :       Output filename
   --verbose                         :       verbose output
   --help                            :       print help

=head3 AUTHOR

 Kai Blin
 Microbiology/Biotechnology
 Auf der Morgenstelle 28
 72076 Tuebingen
 Germany
 Email: kai.blin@biotech.uni-tuebingen.de

=cut
use strict;
use Getopt::Long;
use Bio::SeqIO;

my $in_file = undef;
my $tab_file = undef;
my $out_file = undef;
my $verbose = 0;
my $help = 0;

GetOptions(
    'input|i=s'    => \$in_file,
    'table|t=s'    => \$tab_file,
    'output|o=s'   => \$out_file,
    'verbose|v'    => \$verbose,
    'help|h'       => \$help
);

if ($help) {
    print "\n";
    eval { require Pod::Text };
    if (@$) {
        print
          "Error formatting help content! Perl-Module Pod::Text required!\n";
        print
          "To view help, please install Pod::Text module or look directly\n";
        print "into the perl script $0\n\n";
    }
    else {
        my $pod = Pod::Text->new();
        $pod->parse_from_file($0);
    }
    exit 0;
}

if (! -e $in_file ) {
    die "Input file $in_file does not exist";
}

if (! -e $tab_file ) {
    die "antiSMASH results file $tab_file does not exist";
}

my $in = Bio::SeqIO->new(
    '-file'   => $in_file,
    '-format' => 'EMBL'
);
my $in_seq = $in->next_seq;

my %cds_by_name;

foreach my $cds (grep( $_->primary_tag eq 'CDS', $in_seq->top_SeqFeatures)) {
    my $name;
    if ($cds->has_tag('gene')) {
        ($name) = $cds->get_tag_values('gene');
    } elsif ($cds->has_tag('protein_id')) {
        ($name) = $cds->get_tag_values('protein_id');
    }
    $cds_by_name{$name} = $cds;
}

open(TABFILE, '<', $tab_file) || die "Failed to open $tab_file";
my (@lines) = <TABFILE>;
close(TABFILE);

my $out = Bio::SeqIO->new(
    '-file'   => ">$out_file",
    '-format' => 'EMBL'
);

my @state = ("annotations", "new_features1", "new_features2");
my $state_idx = 0;

sub add_annotation {
    my ($seq, $line) = @_;

    my ($feature, @notes) = split /\t/, $line;
    return if ($#notes < 0);
    my $cds = $cds_by_name{$feature};
    return if (!defined $cds);
    foreach my $note (@notes) {
        $cds->add_tag_value('note', $note);
    }
}

sub new_feature {
    my ($seq, $tag, $pos, $notes, $color, $evalue, $bitscore) = @_;

    $color =~ s#/colour=##g;
    my ($start, $stop) = ($pos =~ /(\d+)\.\.(\d+)/);
    $notes =~ s/;$//g;

    my $feature = Bio::SeqFeature::Generic->new(
            '-primary_tag' => $tag,
            '-start' => $start,
            '-end' => $stop);
    $feature->add_tag_value('notes', $notes);

    if (defined($evalue)) {
        $feature->add_tag_value('notes', $evalue);
    }
    if (defined($bitscore)) {
        $feature->add_tag_value('notes', $bitscore);
    }

    $feature->add_tag_value('colour', $color);
    $seq->add_SeqFeature($feature);
}

sub new_features1 {
    my $line = shift;
    my $seq = shift;
    my ($tag, $pos, $notes, $evalue, $bitscore, $color) = split /\t/, $line;

    new_feature($seq, $tag, $pos, $notes, $color, $evalue, $bitscore);
}

sub new_features2 {
    my $line = shift;
    my $seq = shift;

    my ($tag, $pos, $notes, $color) = split /\t/, $line;
    new_feature($seq, $tag, $pos, $notes, $color);
}

my $seq = $in_seq;
foreach $_ (@lines){
    chomp;
    next if (/^$/);
    if (/>>/) {
        $state_idx++;
        next;
    }
    if ($state[$state_idx] eq "annotations") {
        add_annotation($seq, $_);
    } elsif ($state[$state_idx] eq "new_features1") {
        new_features1($_, $seq);
    } elsif ($state[$state_idx] eq "new_features2") {
        new_features2($_, $seq);
    }
}

$out->write_seq($seq);
