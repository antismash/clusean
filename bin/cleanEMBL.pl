#!/usr/bin/perl
#
# Usage cleanEMBL.pl --input <inputfile> --output <outputfile>
#
# Function: Removes any Qualifiers and CDS_motif features from EMBL-Files
#
# Useful if you made manual changes to the Gene Prediction and want to run CLUSEAN again
#
#
use strict;
use Bio::SeqIO;
use Data::Dumper;
use Getopt::Long;
my $sInputFN;
my $sOutputFN;
my $bVerbose;
GetOptions(
			'input|i=s'  => \$sInputFN,
			'output|o=s' => \$sOutputFN,
			'verbose|v'  => \$bVerbose
);
my $oSeqIOIn = Bio::SeqIO->new( '-file' => $sInputFN );
my $oSeqIOOut = Bio::SeqIO->new( '-file'   => ">" . $sOutputFN,
								 '-format' => 'EMBL' );

while ( my $oSeq = $oSeqIOIn->next_seq ) {
	my @aFeatures = $oSeq->get_SeqFeatures;
	$oSeq->remove_SeqFeatures;
	foreach my $oFeature (@aFeatures) {
		next if ( $oFeature->primary_tag eq "CDS_motif" );
		my @aTags = $oFeature->get_all_tags;
		foreach my $tag (@aTags) {
			if (    ( $tag ne 'gene' )
				 && ( $tag ne 'locus' ) )
			{
				$oFeature->remove_tag($tag);
			}
		}
		$oSeq->add_SeqFeature($oFeature);
	}
	$oSeqIOOut->write_seq($oSeq);
}
