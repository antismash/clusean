#!/usr/bin/perl -w
#
# Converts GenBank-DNA-File to EMBL
#
#
=head1 NAME

genbank2embl.pl

=head1 DESCRIPTION

Converts GenBank-formatted file into EMBL-format

=head1 USAGE

genbank2embl.pl --input <GenBank-filename> --output <EMBL-filename> [--help]

=head1 AUTHOR

Tilmann Weber

Microbiology/Biotechnology

Auf der Morgenstelle28

72076 Tuebingen

Germany

Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

use strict;
use Getopt::Long;
use Bio::Seq;
use Bio::SeqIO;

# CmdLine parameters

my $sInputFile;            # FN of Inputfile
my $sOutputFile;           # FN of OutputFile
my $bNoComment;            # Flag to omit comment
my $bVerbose;              # Flag to trigger verbose mode
my $bHelp;                 # Flag to trigger help


# Bio-Objects

my $oInputSeqIO;           # Bio::SeqIO input
my $oOutputSeqIO;          # Bio::SeqIO output

my $oInputSeq;             # Bio::Seq input


sub printHelp;

GetOptions ('input|i=s'                 =>  \$sInputFile,
	    	'output|o=s'                =>  \$sOutputFile,
	    	'verbose|v!'                =>  \$bVerbose,
            'help|h!'                   =>  \$bHelp);

if ($bHelp) {
  print "\n\n";
  eval {require Pod::Text};
  if (@$) {
    print "Error formatting help content! Perl-Module Pod::Text required!\n";
    print "To view help, please install Pod::Text module or look directly into\n";
    print "the perl script $0\n\n";
  }
  else {
    my $oPodParser = Pod::Text->new ();
    $oPodParser->parse_from_file($0);
  }

  exit 0;
}	    
if (! $sInputFile) {
    die("Syntax error: parameter --input required\n\nType $0 --help for more information.\n\n");
}

if (! $sOutputFile) {
    if ($sInputFile =~ /fasta/) {
	$sOutputFile = $sInputFile;
        $sOutputFile =~ s/fasta/embl/;
    }
    else {
	$sOutputFile .= $sInputFile.".embl";
    }
}

$oInputSeqIO = Bio::SeqIO->new ('-file'       => $sInputFile,
				'-format'     => 'GenBank');

$oOutputSeqIO = Bio::SeqIO->new ('-file'      => ">$sOutputFile",
				 '-format'    => 'EMBL');

while ($oInputSeq=$oInputSeqIO->next_seq) {
    $oOutputSeqIO->write_seq($oInputSeq);
}
