#!/usr/bin/perl -w
#


=head1 NAME

EMBLtoCSV.pl

=head1 SYNOPSIS

EMBLtoCSV.pl <EMBL-Filename> [<delimiter> --printDNA --printAA --printAssTab
--printDesc --printBlastP --uniprot --noBlastSkip --excel <filename> --paperlist(2) 
--noHeader --help]

=head1 DESCRIPTION

EMBLtoCSV.pl extracts CDS information of an EMBL file and generates a CSV
spreadsheet out of the data including DNA and aa sequence, description, best BlastHit
assigned function,...

Best used with files generated with ABDB secondary metabolite biosynthesis gene
annotation pipeline.

=head1 OPTIONS

=head2 required parameter

  <EMBL-Filename>     EMBL-File to be processed

=head2 optional parameters

  <delimiter>         Delimiter char used in csv, default "|"

  --printDNA, -d      Include DNA sequence

  --printAA, -p       Include protein sequence

  --printAssTag, -a   Include assignment table

  --printDesc, -t     Include EMBL file description (Bio::Seq->description)

  --printBlastP, -b   Include best BlastP hitbut not hit against own sequence
  
  --noBlastSkip       Don't skip best BlastP-Hit if 100% sequence identiy

  --uniprot, -u  BLAST search was done against UNIPROT DB

  --excel, -e         Export Data as a MS-Excel Spreadsheet. Pleas give filename as parameter

  --paperlist         Generate publication-like table
  --paperlist2

  --noHeader          Don't output headerline

  --help, -h          Show this help Text

=head1 AUTHOR

 Dr. Tilmann Weber
 Microbiology/Biotechnology
 University of Tuebingen
 72076 Tuebingen
 Germany

 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

#use lib "/home/weber/perllib";

use Bio::Seq;
use Bio::SeqIO;
use Bio::SearchIO;
use Getopt::Long;
use Spreadsheet::WriteExcel::Simple;
# use Data::Dumper;
use strict;

sub checkDelim;     # Ensure unique delimitar character



my ($sPrintDNASeq,
    $sPrintAASeq,
    $sPrintAssTab,
    $sPrintDesc,
    $sPrintBlastP,
    $bNoBlastSkip,
    $buniprotBlast,
    $buniprotBlastOld,
    $sExportExcel,
    $bNoHeader,
    $bPaperList,
    $bPaperList2,
    $bVerbose,
    $bHelp);

# Get Cmdline options
GetOptions ('printDNA|d'    =>   \$sPrintDNASeq,
	    'printAA|p'     =>   \$sPrintAASeq,
	    'printAssTab|a' =>   \$sPrintAssTab,
	    'printDesc|t'   =>   \$sPrintDesc,
	    'printBlastP|b' =>   \$sPrintBlastP, 
	    'noBlastSkip'   =>   \$bNoBlastSkip,
	    'uniprot|u'     =>   \$buniprotBlast,
	    'uniprotOld'    =>   \$buniprotBlastOld,
	    'excel|e=s'     =>   \$sExportExcel,
	    'noHeader|n'    =>   \$bNoHeader,
	    'paperlist'     =>   \$bPaperList,
	    'paperlist2'    =>   \$bPaperList2,
	    'verbose|v'     =>   \$bVerbose,
	    'help|h'        =>   \$bHelp);






if ($bHelp) {
  print "\n\n";
  eval {require Pod::Text};
  if ( @$ ) {
    print "Error formatting help content! Perl-Module Pod::Text required!\n";
    print "To view help, please install Pod::Text module or look directly into\n";
    print "the perl script $0\n\n";
  }
  else {
    my $oPodParser = Pod::Text->new ();
    $oPodParser->parse_from_file($0);
  }
  exit 0;
}

die "Error: File not found. See --help for information." if (! defined $ARGV[0] || (! -e $ARGV[0]));
my $oExcelSpreadsheet;
if ($sExportExcel) {
  eval {require Spreadsheet::WriteExcel::Simple};
  if (@$ ) {
    print "Perl module Spreadsheet::WriteExcel::Simple required for Excel export\n";
    print "Please install this module prior to usage of the -excel switch";
    die 1;
  }
  else {
  	if (-e $sExportExcel) {
  			die ("$sExportExcel file exists. Cannot overwrite!\n");
  	} 
    $oExcelSpreadsheet = Spreadsheet::WriteExcel::Simple->new;
  }
}

# Set delimiter character
my $sDelimiter;
if (($ARGV[1]) && ($sExportExcel)) {
  $sDelimiter = "|";
}
else {
  $sDelimiter = $ARGV[1] || "|";
}

#we have to assing tab again, as otherwise '\t' is inserted as string
$sDelimiter =~ s/\\t/\t/g if ($sDelimiter =~ /\\t/);


# Open input file
my $oSeqIOObj = Bio::SeqIO->new(-file    => $ARGV[0],
				-format  => 'EMBL');

my $sLine;  # $sLine contains everything...

my $sHeading; # Headline
my $sHeadPrint = "TRUE"; #Print Header flag

# Assemble header line

if (! $bNoHeader) {
  $sHeading = join ($sDelimiter, qw (Filename SequenceID SubstanceName GeneName LocusTag ProductTAG FunctionTag NoteTag
                                   EC-Number BestAssignment AssignmentScore Start End LengthNT LenghtAA));
  if (( $bPaperList) || ($bPaperList2)) {
     $sHeading = join ($sDelimiter, qw (Filename SequenceID SubstanceName GeneName LocusTag ProductTAG FunctionTag NoteTag
                                   EC-Number BestAssignment AssignmentScore Start/End LengthNT/LenghtAA));
  }
  if ($sPrintDNASeq) {
    $sHeading .= $sDelimiter."Strand".$sDelimiter."DNASeq";
  }
  if ($sPrintAASeq) {
    $sHeading .= $sDelimiter."AASeq";
  }
  if ($sPrintAssTab) {
    $sHeading .= $sDelimiter."AssTable";
  }
  if ($sPrintDesc) {
    $sHeading .= $sDelimiter."Species".$sDelimiter."EntryDesc";
  }
  if ($sPrintBlastP) {
    if ( $bPaperList) {
     $sHeading .= $sDelimiter.join ($sDelimiter, qw (HitDescCompl HitDesc HitScore/HitEval HitLength/HSPLength
                                                     HitConserved/HitIdentical HitGaps HitOrganism HitDB HitDBRef ));
   }
    elsif ($bPaperList2) {
     $sHeading .= $sDelimiter."BlastHit";
  
    }
    else {
      $sHeading .= $sDelimiter.join ($sDelimiter, qw (HitDescCompl HitDesc HitScore HitEval HitLength HSPLength 
                                                      HitConserved HitIdentical HitGaps HitOrganism HitDB HitDBRef ));
    }
  
  }
  $sHeading .= "\n";
}

while (my $oSeq = $oSeqIOObj->next_seq) {

  # Try to extract compound name
  my $sSubstance;
  if (defined $oSeq->desc) {
    
    ($sSubstance) = $oSeq->desc =~ /.*\b(.*?m[yi]cin\b)/;
    if (!defined $sSubstance) {
      ($sSubstance) = $oSeq->desc =~ /.*\b(.*?icidin\b)/;
    }
    if (!defined $sSubstance) {
      ($sSubstance) = $oSeq->desc =~ /.*\b(.*cyclin.*?\b)/;
    }
    if (!defined $sSubstance) {
      ($sSubstance) = $oSeq->desc =~ /.*\b(\w{2,}?[io][on]\b)/;
    }
    if ((defined $sSubstance) && 
      (($sSubstance eq "protein"   ) ||
       ($sSubstance eq "section"   ) ||
       ($sSubstance eq "strain"    ) ||
       ($sSubstance eq "region"    ) ||
       ($sSubstance eq "operon"    ) ||
       ($sSubstance eq "insertion" ))) {
      $sSubstance = "";
    }
    else {
      $sSubstance = "";
    }
  }
  else {
    $sSubstance="noSubstanceName";
  }

  # Now cycle through all Features
  my @oaFeat = $oSeq->all_SeqFeatures();
  foreach my $oFeature (grep ( $_->primary_tag eq 'CDS',
			       $oSeq->top_SeqFeatures)) {
    $sLine=$ARGV[0].$sDelimiter.$oSeq->accession.$sDelimiter.$sSubstance;

    if ($oFeature->has_tag('gene')) {
      $sLine .= $sDelimiter.checkDelimiter(join (" ",
					   $oFeature->each_tag_value('gene')));
    }
    else {
      $sLine .= $sDelimiter."noGeneName";
    }
    if ($oFeature->has_tag('locus_tag')){
		$sLine .= $sDelimiter.checkDelimiter(join (" ", $oFeature->get_tag_values('locus_tag')));
    }
    else {
      $sLine .= $sDelimiter."noLocusTag";
    }
    if ($oFeature->has_tag('product')) {
      my $sProduct = checkDelimiter(join (" ",
                                    $oFeature->each_tag_value('product')));
      $sLine .= $sDelimiter.$sProduct;
    }
    else {
      $sLine .= $sDelimiter."noProductTag";
    }
    if ($oFeature->has_tag('function')) {
      $sLine .= $sDelimiter.checkDelimiter(join (" ",
                                       $oFeature->each_tag_value('function')));
    }
    else {
      $sLine .= $sDelimiter."noFunctionTag";
    }
    if ($oFeature->has_tag('note')) {
      my @aNoteLine;
      foreach my $sNoteContent ($oFeature->each_tag_value('note')) {
	next if ($sNoteContent =~ /CRITICA/);
	next if ($sNoteContent =~ /^Initiation/);
	next if ($sNoteContent =~ /^Putative/);
	next if ($sNoteContent =~ /^Assignments/);
	push (@aNoteLine, checkDelimiter($sNoteContent));
      }
      if (@aNoteLine) {
	$sLine .= $sDelimiter.join(" ", @aNoteLine);
      }
      else {
	$sLine .= $sDelimiter."noNoteTags";
      }
    }
    else {
      $sLine .= $sDelimiter."noNoteTags";
#      next;
    }
    
    if ($oFeature->has_tag('EC_number')) {
    	$sLine .= $sDelimiter.checkDelimiter(join (" ",
    								$oFeature->each_tag_value('EC_number')));
    }
    else
    {
    	$sLine .= $sDelimiter.'no EC';
    }
    
    my $sAssignmentLine;
    if ($oFeature->has_tag('note')) {
    ($sAssignmentLine) = grep ($_ =~ /^Assignment/,
				   $oFeature->each_tag_value('note'));
    }
    if (defined $sAssignmentLine) {
    	my ($sBestAssignment, $sBestScore) =
    		$sAssignmentLine =~ /^Assignments \(.*\): (\b.*?\b)\s\(([\d\.]+)\)/;
    	$sLine .= $sDelimiter.$sBestAssignment.$sDelimiter.sprintf("%.1f", $sBestScore);
    }
    else {
    	$sLine .= $sDelimiter."n.a.".$sDelimiter."n.a.";
    }
    
    if (($bPaperList) || ($bPaperList2)) {
      $sLine .= $sDelimiter.$oFeature->start."/".$oFeature->end;
    }
    else {
      $sLine .= $sDelimiter.$oFeature->start.$sDelimiter.$oFeature->end;
    }
    
    my $sNoAA = $oFeature->length/3;
    if (($bPaperList) || ($bPaperList2)) {
      $sLine .= $sDelimiter.$oFeature->length."/".$sNoAA;
    }
    else {
      $sLine .= $sDelimiter.$oFeature->length;
      $sLine .= $sDelimiter.$sNoAA;
    }
    if ($sPrintDNASeq) {
      $sLine .= $sDelimiter."Strand: ".$oFeature->strand;
      $sLine .= $sDelimiter.$oFeature->seq->seq;
    }
    if ($sPrintAASeq) {
      $sLine .= $sDelimiter.join ("", 
				  $oFeature->each_tag_value('translation'));
    }
    if ($sPrintAssTab) {
      $sAssignmentLine =~ s/^Assignments \(.*\): //;
      $sLine .= $sDelimiter.checkDelimiter($sAssignmentLine);
    }
    if ($sPrintDesc) {
      if (defined $oSeq->species) {
	$sLine .= $sDelimiter.$oSeq->species->binomial;
      }
      else {
	$sLine .= $sDelimiter."NoSpecies";
      }
      my $sDescLine = $oSeq->desc;
      if (defined $sDescLine) {
	$sLine .= $sDelimiter.checkDelimiter($sDescLine);
      }
      else {
	$sLine .=$sDelimiter;
	}
    }
    if ($sPrintBlastP) {
      # Test whether Blast-Report is available
      if ($oFeature->has_tag('blastp_file')) {
	(my $sBlastFile) = $oFeature->each_tag_value('blastp_file');

	# If file is compressed decompress it "on the fly" (Dirty hack, I know)
	if (-e "$sBlastFile.gz") {
	  open (FH, "gunzip -c $sBlastFile|") || 
                    die "Can't open compressed $sBlastFile!\n";
	}
	else {
	  open (FH, $sBlastFile) || die "Can't open $sBlastFile!\n";
	}

	# Now do the analysis with BioPerl...
	my $oBlastIO = Bio::SearchIO->new ('-fh'      =>  \*FH,
					   '-format'    =>  'blast');
	my $oBlastResult = $oBlastIO->next_result;
	my $oBlastHit = $oBlastResult->next_hit;
	# skip to next hit if own sequence is found in Blast-Search
	# CARE: If there is 100% sequence identity to another sequence this is also skipped
	

	if (defined $oBlastHit) {
		if ((! defined $bNoBlastSkip) && ($oBlastHit->frac_conserved == 1)) {
			$oBlastHit = $oBlastResult->next_hit;
			
			# skip result if no more hits are present
			next if (! defined $oBlastHit);
		}
	  my $oBlastHSP = $oBlastHit->next_hsp;
	  # OK, now we have the best scoring hit in $oBlastHit, 
	  # and can extract the data...
     
	  my $sHitDescCompl  = checkDelimiter($oBlastHit->description);

	  my $sHSPScore = $oBlastHSP->bits;
	  my $sHSPEval  = $oBlastHSP->evalue;
	 
	  # I don't know why but that crappy parser adds a "," after Evalue so let's strip it if it's there...
	  $sHSPEval =~ s/\,//;
	  
	  my $sHitLength = $oBlastHit->length;
	  my $sHSPLength = $oBlastHSP->length;
	  my $sHSPConserved = sprintf("%.1f", $oBlastHSP->frac_conserved*100);
	  my $sHSPIdentical = sprintf("%.1f", $oBlastHSP->frac_identical*100);
	  my $sHSPGaps = $oBlastHSP->gaps;
	  my ($sHitOrganism, $sHitDB, $sHitDBRef, $sHitDesc);

      # EBI changed the header format of the fasta uniprot database
      # so this is for the old format
	  if ($buniprotBlastOld) {
	    ($sHitOrganism) = $sHitDescCompl =~ /.*-\s(.*?)$/;
	    $sHitDB = "uniprot";
	    #($sHitDBRef) = $sHitDescCompl =~ /^(.*?)\|.*/;
	    ($sHitDBRef) = $oBlastHit->name =~ /^(.*?)\|.*/;
	    ($sHitDesc) = $sHitDescCompl =~ /(.*)\s-\s.*$/;
	    if ($bVerbose) {
	      print $sHitOrganism,"|",$sHitDBRef,"|",$sHitDescCompl,"\n";
	    }
	  }
	  
	  # and this is for the new format
	  if ($buniprotBlast) {
	    ($sHitOrganism) = $sHitDescCompl =~ /.*OS=(.*?)\s..=.*$/;
	    $sHitDB = "uniprot";
	    #($sHitDBRef) = $sHitDescCompl =~ /^(.*?)\|.*/;
	    ($sHitDBRef) = $oBlastHit->name =~ /^\w+\|(.*?)\|.*/;
#	    ($sHitDBRef) = $oBlastHit->name ;
	    (my $sGeneName) = $sHitDescCompl =~ /.*GN=(.*?)\s.*/;
	    ($sHitDesc) = $sHitDescCompl =~ /^(.*?)\s..=.*$/;
	    if ($sHitDesc !~ /$sGeneName/i ) {
	    	$sHitDesc .= " ".$sGeneName;
	    } 
	    if ($bVerbose) {
	      print $sHitOrganism,"|",$sHitDBRef,"|",$sHitDescCompl,"\n";
	    }
	  }
	  
	  
	  else {
	    ($sHitOrganism) = $sHitDescCompl =~ /.*\[(.*?)\].*/;
	    my $sHitName = $oBlastHit->name;
	    ($sHitDB, $sHitDBRef) = $sHitName =~ /^(\w+?)\|(.+?)\|.*/ ; 
	    #($sHitDB, $sHitDBRef) = $sHitDescCompl =~ /^(\w+?)\|(.+?)\|.*/ || ("noRef", "noRef");
	 
	    ($sHitDesc) = $sHitDescCompl =~ /^(.*?)\s\[/; 
	    if (! defined $sHitDesc) {
	    	$sHitDesc= $sHitDescCompl;
	    }
	    if ($bVerbose) {
	      print $sHitOrganism,"|",$sHitDBRef,"|",$sHitDescCompl,"|",$sHitDesc,"\n";
	    }

	  }

 
	  if ($bPaperList) {
	    $sLine .= $sDelimiter.join ($sDelimiter, ($sHitDescCompl,
						      $sHitDesc, $sHSPScore."/".$sHSPEval,
						      $sHitLength."/".$sHSPLength,
						      $sHSPConserved."/".$sHSPIdentical,
						      $sHSPGaps, $sHitOrganism,
						      $sHitDB, $sHitDBRef));
	    
	  }
	  elsif ($bPaperList2) {
	    my $sBlastLine = $sHitDesc."\n".$sHitOrganism." ".$sHitDB." ".$sHitDBRef."\n";
	    $sBlastLine   .= "Score: ".$sHSPScore." E-value: ".$sHSPEval;
	    $sBlastLine   .= " id: ".$sHSPIdentical."% cons: ".$sHSPConserved."%";

	    $sLine .= $sDelimiter.$sBlastLine;
	  }
	  else {

	    $sLine .= $sDelimiter.join ($sDelimiter, ($sHitDescCompl,
						      $sHitDesc, $sHSPScore,
						      $sHSPEval, $sHitLength,
						      $sHSPLength,
						      $sHSPConserved,
						      $sHSPIdentical,
						      $sHSPGaps, $sHitOrganism,
						      $sHitDB, $sHitDBRef));
	  }
	}
	else {
	  $sLine .= $sDelimiter.join ($sDelimiter, 
			     qw(noHitDescCompl noHitdesc
                                noScore noEval noHitLenght noHSPLength
                                noHitConserved noHitIdentical noHitGaps
                                noHitOrganism noHitDB noHitDBRef));
	}
	close (FH);
      }
      else {
	$sLine .= $sDelimiter.join ($sDelimiter, 
				    qw (noBlastHit noBlastHit
                                       noScore noEval noHitLenght
                                       noFracConserved no FracIdent
                                       noGaps noOrganism noHitRef noHitRef));
      }
    }
    if ($sExportExcel) {
      if ((! $bNoHeader) && ($sHeadPrint)) {
	my @aColumns=split(/\|/, $sHeading);
	$oExcelSpreadsheet->write_bold_row (\@aColumns);
	$sHeadPrint=undef;
      }
      my @aColumns=split(/\|/o, $sLine);
      $oExcelSpreadsheet->write_row(\@aColumns);
#      print Dumper(@aColumns);
    }
    else {
      if ((! $bNoHeader) && ($sHeadPrint)) {
	print $sHeading;
	$sHeadPrint=undef;
      }
      print $sLine,"\n";
    }
  }
  if ($sExportExcel) {
    $oExcelSpreadsheet->save($sExportExcel) || die "Error saving EXCEL file $sExportExcel\n";
  }
}

# We have to make sure, that the column-delimiter is not contained 
# in the entry.
# This subroutine makes the necessary changes to the content

sub checkDelimiter {
  my $sString = shift;
  if ($sDelimiter eq ",") {
    $sString =~ s/,/|/g;
  }
  elsif ($sDelimiter eq "|") {
    $sString =~ s/\|/;/g;
  }
  elsif ($sString =~ /$sDelimiter/) {
    $sString =~ s/$sDelimiter/|/g;
  }
  return $sString;
}




















