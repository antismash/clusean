#!/usr/bin/perl
#
# Author: Tilmann Weber
#
# This program runs analyses based on the extraction of specific amino acids
#
# InputFiles: EMBL-Format-File, processed with CLUSEAN pipeline (evaluateDomains.pl)
#
# Output: EMBL-File with blastp_file tags
#         HMMer-Reports of identified genes
#
#

=head1 NAME

BestProfileHit.pl

=head1 SYNOPSIS

BestProfileHit.pl  --input <input-EMBL-file> --output <output-EMBL-file> [--onlynew --csv <CSV-file>
--noCSVheader --verbose --help]

=head1 DESCRIPTION

	Returns best hit against HMM / Blast profile database for specified domain types
	Configuration: $CLUSEANROOT/etc/ProfileResources.xml

=head2 PARAMETERS

=head3
   --input <filename>               :       Input file (in EMBL-format)
   --output <filename>              :       Output filename (EMBL-format)
   --tool                           :       Analysis to perform (as defined in ProfileResources.xml
                                            Default: DomFinder
                                            
                                            Options: DomFinder: Identify enzymatic domains in modular PKS/NRPS
                                                     tATKSpred: Prediction of tAT-KS specificities
                                                     
   --onlynew                        :       Only process new CDS features

   --csv <filename>                 :       In addition to EMBL export, also export as CSV
   --noCSVheader                    :       Omit CSV headerline (useful for automated processing of
                                            the CSV line
                                             
   --verbose                        :       verbose output
   --help                           :       print help

=head3 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 Auf der Morgenstelle 28
 72076 Tuebingen
 Germany
 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

use strict;
use CLUSEAN::GetDomains;
use Bio::SeqIO;
use Bio::SearchIO;
use Bio::SeqFeature::Generic;
use Getopt::Long;
use XML::Simple;
use FindBin qw ($Bin);
use Config::General;
use File::Basename;
use File::Temp qw (tempdir);

my $sInputSeqFn;     # Filename for InputFile
my $sOutputSeqFn;    # Filename for Outputfile
my $sTool;
my $sCSVOutFn;       # Filename for annotation output file
my $bVerbose;        # Verbose flag
my $bHelp;           # Help flag
my $bNoCSVHead;      # Don't print CSV-Headerline of field descriptions
my $oSeqIOIn;        # Input SeqIO-Object
my $oSeqIOOut;       # Output SeqIO-Object
my $oSeq;            # Bio::Seq main object

#Import Configuration data
my $CLUSEANROOT;
if ( defined $ENV{CLUSEANROOT} ) {
	$CLUSEANROOT = $ENV{CLUSEANROOT};
}
else {
	$CLUSEANROOT = $Bin;
	$CLUSEANROOT =~ s/(.*)\/.*?$/$1/;
}

# Parse Config file
my $oConfig =
  Config::General->new( '-ConfigFile' => "$CLUSEANROOT/etc/CLUSEAN.cfg" );
my %hConfig    = $oConfig->getall;
my $sSeparator = $hConfig{Formatting}->{Separator}
  || "|";    # Character for joining entries;
my $sXMLFileName = $hConfig{ProfileResources}->{ProfileResourceFile};
if ( !-e "$CLUSEANROOT/$sXMLFileName" ) {
	die "Can't find Singnature XML file at $CLUSEANROOT/$sXMLFileName\n";
}

# Parse XML-File and assign hashref
my $xml = XML::Simple->new( 'SuppressEmpty' => 1 );
my $xmlHash = $xml->XMLin("$CLUSEANROOT/$sXMLFileName");

# Evaluate Cmdline and assign variables
GetOptions(
			'input|i=s'     => \$sInputSeqFn,
			'output|o=s'    => \$sOutputSeqFn,
			'tool|t=s'      => \$sTool,
			'csv|c=s'       => \$sCSVOutFn,
			'noCSVHeader|n' => \$bNoCSVHead,
			'verbose|v'     => \$bVerbose,
			'help|h'        => \$bHelp
);

# Check parameters
if ($bHelp) {
	print "\n\n";
	eval { require Pod::Text };
	if (@$) {
		print
		  "Error formatting help content! Perl-Module Pod::Text required!\n";
		print
"To view help, please install Pod::Text module or look directly into\n";
		print "the perl script $0\n\n";
	}
	else {
		my $oPodParser = Pod::Text->new();
		$oPodParser->parse_from_file($0);
	}
	exit 0;
}
if ( !-e $sInputSeqFn ) {
	die "EMBL Inputfile $sInputSeqFn not found";
}
my ( undef, $sInputDirName, undef ) = fileparse($sInputSeqFn);

# First we have to load the annotated EMBL-file
$oSeqIOIn = Bio::SeqIO->new( '-file'   => $sInputSeqFn,
							 '-format' => 'EMBL' );

# create output object
$oSeqIOOut = Bio::SeqIO->new( '-file'   => ">$sOutputSeqFn",
							  '-format' => 'EMBL' );
if ( defined $sCSVOutFn ) {

	# open Filehandle for CSV output
	open( CSVOUT, ">$sCSVOutFn" ) || die "Cannot open $sCSVOutFn for writing";
	if ( !defined $bNoCSVHead ) {
		my @aHeader = (
						"Accession number",     "Analysis name",
						"Analysis description", "GeneName",
						"CDSstart",             "CDSend",
						"Strand",               "FeatureLabel",
						"FeatureStart",         "FeatureEnd",
						"Hit-ID",               "Hit-Description",
						"Score",                "E-value",
						"Gaps",                 "Discriminated against"
		);
		print CSVOUT join( $sSeparator, @aHeader ), "\n";
	}
}

# For every Sequence in File...
while ( $oSeq = $oSeqIOIn->next_seq ) {

	# get sequence id
	my $sSeqID = $oSeq->id;

	# if ID is unknown check accession...
	if ( $sSeqID =~ /.*unknown.*/ ) {
		$sSeqID = $oSeq->accession;
	}
	$sSeqID =~ s/\s/_/g;
	$sSeqID =~ s/\;$//;
	my @aAnalyses = keys( %{$xmlHash} );
	if ($bVerbose) {
		print "Available analyses: @aAnalyses \n";
	}
	if ( ( defined $sTool ) && ( defined $xmlHash->{$sTool} ) ) {
		@aAnalyses = ();
		push( @aAnalyses, $sTool );
	}

	# For each analysis defined in SignatureResources.xml file
	foreach my $sAnalysis (@aAnalyses) {

		# Skip xmlns data...
		next if ( $sAnalysis =~ /xmlns.*/ );

		# only run on prediction type analyses
		next if ( $xmlHash->{$sAnalysis}->{type} !~ /Best.*Hit/ );
		if ($bVerbose) {
			print "---------\nAnalysis: $sAnalysis\n\n";
		}

		# Now cylce through CDS features
		foreach my $oCDSFeature (
				 grep ( ( $_->primary_tag eq 'CDS' ), $oSeq->top_SeqFeatures ) )
		{
			my @aFeatures;
			if ( $xmlHash->{$sAnalysis}->{'Prerequisite'}->{'type'} =~
				 /annotation/ )
			{
				my $oOverlappingFeatures =
				  CLUSEAN::GetDomains->new( '-featureObj' => \$oCDSFeature,
											'-seqObj'     => $oSeq );
				my @aOverlappingFeatures =
				  $oOverlappingFeatures->getSubfeatures;

				# skip CDS if there are no overlapping features for this CDS
				next if ( !defined $aOverlappingFeatures[0] );
				@aFeatures = grep ( (
						   (
							 $_->primary_tag eq
							   $xmlHash->{$sAnalysis}->{Prerequisite}
							   ->{primary_tag_type}
						   )
							 && (
							   $_->has_tag(
								   $xmlHash->{$sAnalysis}->{Prerequisite}->{tag}
							   )
							 )
						),
						@aOverlappingFeatures );
			}
			elsif (
					(
					  $xmlHash->{$sAnalysis}->{'Prerequisite'}->{'type'} =~
					  /profileHit/
					)
					|| ( $xmlHash->{$sAnalysis}->{'Prerequisite'}->{'type'} =~
						 /all/ )
			  )
			{
				####
				#####
				#BLAST/HMMer Execution + parsing
				my $Database = $xmlHash->{$sAnalysis}->{Execute}->{database};

				# to help debugging: Lets start the tempfile with date/time
				my ( $sec, $min, $hour, $day, $mon, $year, $wday, $yday,
					 $isdst ) = localtime;
				$year += 1900;
				$mon++;

				# Create temporary directory below $sTempDirBase
				my $sTempDirBase = $ENV{TEMP}
				  || '/tmp'
				  ;  # Temp dir, use /tmp if no TEMP environment variable is set
				my $sTempDir = tempdir(
										"BestProfileHit_" 
										  . $year 
										  . $mon 
										  . $day 
										  . $hour 
										  . $min
										  . $sec . "XXXX",
										DIR     => $sTempDirBase,
										CLEANUP => 1
				);
				open( FASTAOUT, ">$sTempDir/FeatTransl.fasta" )
				  || die(
						"Can't open $sTempDir/FeatTransl.fasta for writing!\n");

				# Get DNA-sequence of feature
				my $oOrfDNASeq = $oCDSFeature->seq;
				if ( $oCDSFeature->strand eq -1 ) {
					$oOrfDNASeq->revcom;
				}
				my $aaSeq =
				  $oOrfDNASeq->translate( undef, undef, undef, 11 )->seq;

				# OK, write Fastafile
				print FASTAOUT ">tempFasta\n";
				print FASTAOUT $aaSeq;
				close(FASTAOUT);
				my $sCmdLine =
				  $xmlHash->{$sAnalysis}->{'Prerequisite'}->{'program'};
				$sCmdLine .= " "
				  . $xmlHash->{$sAnalysis}->{'Prerequisite'}->{'parameters'};
				if (
					 defined $xmlHash->{$sAnalysis}->{'Prerequisite'}
					 ->{'dbprefix'} )
				{
					$sCmdLine .= " "
					  . $xmlHash->{$sAnalysis}->{'Prerequisite'}->{'dbprefix'};
				}
				$sCmdLine .=
				  " " . $xmlHash->{$sAnalysis}->{'Prerequisite'}->{'database'};
				if (
					 defined $xmlHash->{$sAnalysis}->{'Prerequisite'}
					 ->{'inputfileprefix'} )
				{
					$sCmdLine .= " "
					  . $xmlHash->{$sAnalysis}->{'Prerequisite'}
					  ->{'inputfileprefix'};
				}
				$sCmdLine .= " " . $sTempDir . "/FeatTransl.fasta";
				if (
					$xmlHash->{$sAnalysis}->{'Prerequisite'}->{'CaptureConsole'}
					eq "TRUE" )
				{
					open( OUTFILE, ">$sTempDir/Analysis.out" )
					  || die("Can't open file for tool output");
					my $Output = `$sCmdLine`
					  || die("Error executing:\n$sCmdLine\n");
					print OUTFILE $Output;
					close(OUTFILE);
				}
				else {
					if (
						 defined $xmlHash->{$sAnalysis}->{'Prerequisite'}
						 ->{'outputfileprefix'} )
					{
						$sCmdLine .= " "
						  . $xmlHash->{$sAnalysis}->{'Prerequisite'}
						  ->{'outputfileprefix'};
					}
					$sCmdLine .= " " . $sTempDir . "/Analysis.out";
					system($sCmdLine) || die("Error executing:\n$sCmdLine\n");
				}

# OK, now we have run the analysis (jut tested with HMMer, but Blast should also work
				my %ParserSelect = (
									 'hmmpfam'  => 'hmmer',
									 'blastall' => 'blast',
									 'hmmscan'  => 'hmmer3'
				);
				my $oSearchIO =
				  Bio::SearchIO->new(
							'-file' => $sTempDir . "/Analysis.out",
							'-format' =>
							  $ParserSelect{ $xmlHash->{$sAnalysis}->{'Execute'}
								  ->{'program'} }
				  );

# For this tool we only expect one Hit on the single HMM-profile in the analysis file, so we just need to look
# at the first result/hit
# The domains then are inside the HSPs
				while ( my $oResult = $oSearchIO->next_result() ) {

					while ( my $oHit = $oResult->next_hit() ) {

						while ( my $oHSP = $oHit->next_hsp ) {

							next
							  if (    ( $oHSP->evalue > 0.01 )
								   || ( $oHSP->score < 0 ) );

			 # ...calculate start/stop nucleotides out of protein coordinates...
							my ( $iFtStart, $iFtEnd );
							if ( $oCDSFeature->strand eq 1 ) {
								$iFtStart = $oCDSFeature->start +
								  ( 3 * $oHSP->start ) - 3;
								$iFtEnd =
								  $oCDSFeature->start + ( 3 * $oHSP->end ) - 1;
							}
							else {
								$iFtEnd =
								  $oCDSFeature->end - ( 3 * $oHSP->start ) + 3;
								$iFtStart =
								  $oCDSFeature->end - ( 3 * $oHSP->end ) + 1;
							}

							# Setup new sequence-feature
							my $oHMMFeature =
							  Bio::SeqFeature::Generic->new(
									  '-primary_tag' =>
										$xmlHash->{$sAnalysis}->{'Prerequisite'}
										->{'primary_tag_type'},
									  '-strand' => $oCDSFeature->strand,
									  '-start'  => $iFtStart,
									  '-end'    => $iFtEnd
							  );
							my $sHMMDesc = "Hit against profile: ";
							$sHMMDesc .= $oHit->name;
							$sHMMDesc .= '. ';
							$sHMMDesc .= 'Score: ' . $oHSP->score . '. ';
							$sHMMDesc .= 'E-value: ' . $oHSP->evalue . '. ';
							$sHMMDesc .=
							    'Domain range: '
							  . $oHSP->start . '..'
							  . $oHSP->end . ".";
							$oHMMFeature->add_tag_value( 'note', $sHMMDesc );
							$oHMMFeature->add_tag_value(
										$xmlHash->{$sAnalysis}->{'Prerequisite'}
										  ->{'tag'},
										$oHit->name
							);

							push( @aFeatures, $oHMMFeature );
						}
						#####
						#####
					}
				}
			}

	 #			elsif (
	 #				   $xmlHash->{$sAnalysis}->{'Prerequisite'}->{'type'} =~ /all/ )
	 #			{
	 #
	 #		   #				@aFeatures =
	 #		   #				  grep ( ( $_->primary_tag eq 'CDS' ), $oSeq->top_SeqFeatures );
	 #				push( @aFeatures, $oCDSFeature );
	 #			}
			else {
				die(
"Unknown method to obtain domains specified in $sXMLFileName!\n" );
			}

			foreach my $oFeature (@aFeatures) {
				if ($bVerbose) {
					print "\nAnalyzing CDS ", $oCDSFeature->start, "..",
					  $oCDSFeature->end, " ";
					if ( $oCDSFeature->has_tag('gene') ) {
						my ($sGene) = $oCDSFeature->get_tag_values('gene');
						print "Gene: $sGene\n";
					}
					print "Feature at ", $oFeature->start, "..",
					  $oFeature->end, "\n";
				}

# Now we should only have $oFeatures that match the prerequisites defined in xml file
# Attach sequence data
				$oFeature->attach_seq($oSeq);

				# Get gene name
				my $sGene;
				if ( $oCDSFeature->has_tag('gene') ) {
					($sGene) = $oCDSFeature->get_tag_values('gene');
				}
				else {
					$sGene = "noGeneName";
				}

				#BLAST/HMMer Execution + parsing
				my $Database = $xmlHash->{$sAnalysis}->{Execute}->{database};

				# to help debugging: Lets start the tempfile with date/time
				my ( $sec, $min, $hour, $day, $mon, $year, $wday, $yday,
					 $isdst ) = localtime;
				$year += 1900;
				$mon++;

				# Create temporary directory below $sTempDirBase
				my $sTempDirBase = $ENV{TEMP}
				  || '/tmp';

				# Temp dir, use /tmp if no TEMP environment variable is set
				my $sTempDir = tempdir(
										"BestProfileHit_" 
										  . $sGene 
										  . $year 
										  . $mon 
										  . $day 
										  . $hour
										  . $min
										  . $sec . "XXXX",
										DIR     => $sTempDirBase,
										CLEANUP => 1
				);
				open( FASTAOUT, ">$sTempDir/FeatTransl.fasta" )
				  || die(
						"Can't open $sTempDir/FeatTransl.fasta for writing!\n");

				# Get DNA-sequence of feature
				my $oOrfDNASeq = $oFeature->seq;
				if ( $oFeature->strand eq -1 ) {
					$oOrfDNASeq->revcom;
				}
				my $aaSeq =
				  $oOrfDNASeq->translate( undef, undef, undef, 11 )->seq;

				# OK, write Fastafile
				print FASTAOUT ">tempFasta\n";
				print FASTAOUT $aaSeq;
				close(FASTAOUT);
				my $sCmdLine = $xmlHash->{$sAnalysis}->{'Execute'}->{'program'};
				$sCmdLine .=
				  " " . $xmlHash->{$sAnalysis}->{'Execute'}->{'parameters'};
				if ( defined $xmlHash->{$sAnalysis}->{'Execute'}->{'dbprefix'} )
				{
					$sCmdLine .=
					  " " . $xmlHash->{$sAnalysis}->{'Execute'}->{'dbprefix'};
				}
				$sCmdLine .=
				  " " . $xmlHash->{$sAnalysis}->{'Execute'}->{'database'};
				if (
					 defined $xmlHash->{$sAnalysis}->{'Execute'}
					 ->{'inputfileprefix'} )
				{
					$sCmdLine .= " "
					  . $xmlHash->{$sAnalysis}->{'Execute'}
					  ->{'inputfileprefix'};
				}
				$sCmdLine .= " " . $sTempDir . "/FeatTransl.fasta";
				if ( $xmlHash->{$sAnalysis}->{'Execute'}->{'CaptureConsole'} eq
					 "TRUE" )
				{
					open( OUTFILE, ">$sTempDir/Analysis.out" )
					  || die("Can't open file for tool output");
					my $Output = `$sCmdLine`
					  || die("Error executing:\n$sCmdLine\n");
					print OUTFILE $Output;
					close(OUTFILE);
				}
				else {
					if (
						 defined $xmlHash->{$sAnalysis}->{'Execute'}
						 ->{'outputfileprefix'} )
					{
						$sCmdLine .= " "
						  . $xmlHash->{$sAnalysis}->{'Execute'}
						  ->{'outputfileprefix'};
					}
					$sCmdLine .= " " . $sTempDir . "/Analysis.out";
					system($sCmdLine) || die("Error executing:\n$sCmdLine\n");
				}

# OK, now we have run the analysis (jut tested with HMMer, but Blast should also work
				my %ParserSelect = (
									 'hmmpfam'  => 'hmmer',
									 'blastall' => 'blast',
									 'hmmscan'  => 'hmmer3'
				);
				my $oSearchIO =
				  Bio::SearchIO->new(
							'-file' => $sTempDir . "/Analysis.out",
							'-format' =>
							  $ParserSelect{ $xmlHash->{$sAnalysis}->{'Execute'}
								  ->{'program'} }
				  );

	   # Now cycle through results/hits/hsp; skip if no result/hit/hsp was found
				while ( my $oResult = $oSearchIO->next_result() ) {

	 # Sort hits with descencendig e-values, that first returned hit is best hit
					$oResult->sort_hits(
						sub {
							$Bio::Search::Result::ResultI::a->significance <=>
							  $Bio::Search::Result::ResultI::b->significance;
						}
					);

					# Best hit is first HSP of first Hit
					my $oHit = $oResult->next_hit() || next;
					my $oHSP = $oHit->next_hsp()    || next;

					# Hits with lower scores follow

					my @DiscHits;
					while ( my $oDiscHit = $oResult->next_hit() ) {

						while ( my $oDiscHSP = $oDiscHit->next_hsp ) {

							next
							  if (    ( $oDiscHSP->evalue > 0.01 )
								   || ( $oDiscHSP->score < 0 ) );
							push( @DiscHits,
								      $oDiscHit->name
									. " (score: "
									. $oDiscHSP->score
									. "; E-value: "
									. $oDiscHSP->evalue
									. ")" );
						}
					}

					#Generate tabular/EMBL output
					if ( defined $sCSVOutFn || defined $bVerbose ) {
						my @aLine = (
							$oSeq->id,
							$sAnalysis,
							$xmlHash->{$sAnalysis}->{'description'},
							$sGene,
							$oCDSFeature->start,
							$oCDSFeature->end,
							$oCDSFeature->strand,

							#								  $sLabel,
							$oFeature->start,
							$oFeature->end,
							$oHit->name,
							$oHit->description,
							$oHSP->score,
							$oHSP->evalue,
							$oHSP->gaps,
							join( "; ", @DiscHits )

						);
						print CSVOUT join( $sSeparator, @aLine ), "\n";
						if ($bVerbose) {
							print join( $sSeparator, @aLine ), "\n";
						}
					}

					# Attach data to oFeature object
					my $sNoteLine =
					    $sAnalysis 
					  . "-hit: "
					  . $oHit->name
					  . " with score: "
					  . $oHSP->score
					  . "; E-value: "
					  . $oHSP->evalue;
					$oFeature->add_tag_value( 'note', $sNoteLine );

					# print out discriminated hits
					if (@DiscHits) {
						$sNoteLine =
						  $sAnalysis . ":" . $oHit->name . " won against: ";
						$sNoteLine .= join( "; ", @DiscHits );
						$oFeature->add_tag_value( 'note', $sNoteLine );
					}

					# replace label tag with identified domain
					if ( $oFeature->has_tag('label') ) {
						$oFeature->remove_tag('label');
					}
					$oFeature->add_tag_value( 'label', $oHit->name );
					$oSeq->add_SeqFeature($oFeature);
				}
			}
		}
	}

	# Write results to output EMBL file
	$oSeqIOOut->write_seq($oSeq);
}
