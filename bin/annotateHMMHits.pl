#!/usr/bin/perl

=head1 NAME

annotateHMMHits.pl

=head1 SYNOPSIS

annotateHMMHits.pl --input <input-EMBL-file> --output <output-EMBL-file> [--hmmertag HMMer_file
                   --feattag CDS_motif --evalue 0.01 --score 1 --verbose --help]

=head1 DESCRIPTION

Enters data contained in  CDS associated HMMer result files into EMBL-Style annotation.

=head2 PARAMETERS

=head3
   --input <filename>                :       Input file (in EMBL-format)
   --output <filename>               :       Output filename (EMBL-format)
   --hmmertag <tag>                  :       Tag used for HMM-result-reference
                                             default: HMMer_file
   --feattag <tag>                   :       Tag used for feature annotation
                                             default: CDS_motif
   --evalue <number>                 :       E-Value treshold, default: 0.01
   --score  <number>                 :       minimal score, default 0
   --verbose                         :       verbose output
   --help                            :       print help

=head3 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 Auf der Morgenstelle 28
 72076 Tuebingen
 Germany
 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

# So let the code begin...

# as the EMBL format changed mid 2006 we need to have a post Bioperl 1.4 version# so we switch to bioperl live in my home directory

use strict;
use Bio::SeqIO;
use CLUSEAN::AnnotateHMMHits;
use Getopt::Long;
use File::Basename;
use Config::General;
use FindBin qw ($Bin);

# Global variables declaration

my $sInputSeqFn;     # Filename for InputFile
my $sOutputSeqFn;    # Filename for Outputfile

my $bVerbose;        # Verbose flag
my $bHelp;           # Help flag

my $oSeqIOIn;        # Input SeqIO-Object
my $oSeqIOOut;       # Output SeqIO-Object
my $oSeq;            # Bio::Seq main object

my $CLUSEANROOT;
if ( defined $ENV{CLUSEANROOT} ) {
    $CLUSEANROOT = $ENV{CLUSEANROOT};
}
else {
    $CLUSEANROOT = $Bin;
    $CLUSEANROOT =~ s/(.*)\/.*?$/$1/;
}

# Parse Config file
my $oConfig =
  Config::General->new( '-ConfigFile' => "$CLUSEANROOT/etc/CLUSEAN.cfg" );
my %hConfig = $oConfig->getall;

my $sHMMerTag    = undef;
my $sFeatureTag  = undef;
my $fScore       = undef;
my $fEvalue       = undef;

# Evaluate Cmdline and assign variables

GetOptions(
    'input|i=s'    => \$sInputSeqFn,
    'output|o=s'   => \$sOutputSeqFn,
    'hmmertag|h=s' => \$sHMMerTag,
    'feattag|t=s'  => \$sFeatureTag,
    'evalue|e=s'   => \$fEvalue,
    'score|s=s'    => \$fScore,
    'verbose|v'    => \$bVerbose,
    'help|h'       => \$bHelp
);

# Check parameters

my @extra_args = ();
push (@extra_args, (-evalue => $fEvalue)) if (defined($fEvalue));
push (@extra_args, (-score => $fScore)) if (defined($fScore));
push (@extra_args, (-feattag => $sFeatureTag)) if (defined($sFeatureTag));
push (@extra_args, (-hmmertag => $sHMMerTag)) if (defined($sHMMerTag));

if ($bHelp) {
    print "\n\n";
    eval { require Pod::Text };
    if (@$) {
        print
          "Error formatting help content! Perl-Module Pod::Text required!\n";
        print
          "To view help, please install Pod::Text module or look directly\n";
        print "into the perl script $0\n\n";
    }
    else {
        my $oPodParser = Pod::Text->new();
        $oPodParser->parse_from_file($0);
    }

    #  print `pod2text $0`;
    exit 0;
}

if ( !-e $sInputSeqFn ) {
    die "EMBL Inputfile $sInputSeqFn not found";
}

my ( undef, $sInputDirName, undef ) = fileparse($sInputSeqFn);
if ($bVerbose) {
    print "Inputfile basedir is $sInputDirName \n";
}

$oSeqIOIn = Bio::SeqIO->new(
    '-file'   => $sInputSeqFn,
    '-format' => 'EMBL'
);

# create output object

$oSeqIOOut = Bio::SeqIO->new(
    '-file'   => ">$sOutputSeqFn",
    '-format' => 'EMBL'
);

# For every Sequence in File (normally only one sequence per file...)
while ( $oSeq = $oSeqIOIn->next_seq ) {

    my $seq = annotateHMMHits(-sequence => $oSeq,
                              -config => \%hConfig,
                              -basedir => $sInputDirName,
                              -verbose => $bVerbose,
                              @extra_args);

    $oSeqIOOut->write_seq($seq);
}
