#!/usr/bin/perl -w
#
=head1 NAME

annotateClusterPredict.pl

=head1 SYNOPSIS

annotateClusterPredict.pl --input filename --table filename --output filename

=head1 DESCRIPTION

Annotate probabilities that a given CDS_motif is part of a secondary
metabolite producing cluster.

=head2 PARAMETERS

=head3
   --input <filename>                :       Input filename
   --table <filename>                :       Cluster prediction table
   --output <filename>               :       Output filename
   --verbose                         :       verbose output
   --help                            :       print help

=head3 AUTHOR

 Kai Blin
 Microbiology/Biotechnology
 Auf der Morgenstelle 28
 72076 Tuebingen
 Germany
 Email: kai.blin@biotech.uni-tuebingen.de

=cut

use strict;
use Getopt::Long;
use Bio::SeqIO;
use CLUSEAN::AnnotateClusterPredict;

my $in_file = undef;
my $tab_file = undef;
my $out_file = undef;
my $verbose = 0;
my $help = 0;

GetOptions(
    'input|i=s'    => \$in_file,
    'table|t=s'    => \$tab_file,
    'output|o=s'   => \$out_file,
    'verbose|v'    => \$verbose,
    'help|h'       => \$help
);

if ($help) {
    print "\n";
    eval { require Pod::Text };
    if (@$) {
        print
          "Error formatting help content! Perl-Module Pod::Text required!\n";
        print
          "To view help, please install Pod::Text module or look directly\n";
        print "into the perl script $0\n\n";
    }
    else {
        my $pod = Pod::Text->new();
        $pod->parse_from_file($0);
    }
    exit 0;
}

if (! -e $in_file ) {
    die "Input file $in_file does not exist";
}

if (! -e $tab_file ) {
    die "Cluster prediction table $tab_file does not exist";
}

my $in = Bio::SeqIO->new(
    '-file'   => $in_file,
    '-format' => 'EMBL'
);
my $seq = $in->next_seq;

open(TABFILE, '<', $tab_file) || die "Failed to open $tab_file";
my (@lines) = <TABFILE>;
close(TABFILE);

my $out = Bio::SeqIO->new(
    '-file'   => ">$out_file",
    '-format' => 'EMBL'
);

my $out_seq = annotateClusterPredict($seq, \@lines);

$out->write_seq($out_seq);

