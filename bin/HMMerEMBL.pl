#!/usr/bin/perl
#
# Author: Tilmann Weber
#
# This program runs HMMer analysis agains all CDS-Features found in EMBL-File
#
# InputFiles: EMBL-Format-File
#
# Output: EMBL-File with blastp_file tags
#         HMMer-Reports of identified genes
#
#


=head1 NAME

annotateHMMHits.pl

=head1 SYNOPSIS

HMMerEMBL.pl --input <input-EMBL-file> --output <output-EMBL-file>
[--hmmerdb <Database>  --hmmerdir <directory to store results>
--run --hmmerparms <additional hmmsearch parameters>
--hmmertag <tag used for reference> --onlynew --verbose --help

=head1 DESCRIPTION

Enters data of CDS associated HMMer result files into EMBL-Style file.

=head2 PARAMETERS

=head3
   --input <filename>                :       Input file (in EMBL-format)
   --output <filename>               :       Output filename (EMBL-format)
   --hmmerdb <filename>              :       Database to run hmmpfam on
                                             default: Pfam_fs
   --hmmerdir <dirname>              :       Directory to store outputfiles
                                             Default: HMMer
   --hmmertag <tag>                  :       Tag used for HMM-result-reference
                                             default: HMMer_file
   --run                             :       Start hmmpfam from within script
   --hmmerparms <"parameters">       :       additional hmmsearch parameters,
                                             enclose in ""
   --onlynew                         :       Only process CDS features with no
                                             previous HMMer-results
   --verbose                         :       verbose output
   --help                            :       print help

=head3 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 Auf der Morgenstelle 28
 72076 Tuebingen
 Germany
 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

# as the EMBL format changed mid 2006 we need to have a post Bioperl 1.4 version# so we switch to bioperl live in my home directory

# use lib "/home/weber/perllib";

use strict;
use Bio::SeqIO;
use Bio::SeqFeature::Generic;
use Getopt::Long;
use Config::General;
use FindBin qw ($Bin);
use File::Basename;



my $sInputSeqFn;                 # Filename for InputFile
my $sOutputSeqFn;                # Filename for Outputfile
my $bRunHMMer;                   # Run HMMer from script?
my $ssingleFASTA;                # singleFASTA file

my $bOnlyNew;                    # process only new CDS with no
                                 # previous results
my $bVerbose;                    # Verbose flag
my $bHelp;                       # Help flag

my $oSeqIOIn;                    # Input SeqIO-Object
my $oSeqIOOut;                   # Output SeqIO-Object
my $oSeqIOSingleFasta;           # Output SeqIO-Object for singleFASTA
my $oSeq;                        # Bio::Seq main object

my $sHMMerDir    = "HMMer";
my $sTag         = "HMMer_file";

# Evaluate Cmdline and assign variables

GetOptions ( 'input|i=s'               =>     \$sInputSeqFn,
	     'output|o=s'              =>     \$sOutputSeqFn,
	     'hmmerdir|d=s'            =>     \$sHMMerDir,
	     'singleFASTA=s'           =>     \$ssingleFASTA,
	     'run|r'                   =>     \$bRunHMMer,
	     'tag|t=s'                 =>     \$sTag,
	     'onlynew|n'               =>     \$bOnlyNew,
	     'verbose|v'               =>     \$bVerbose,
         'help|h'                  =>     \$bHelp);

# Check parameters


if ($bHelp) {
  print "\n\n";
  eval {require Pod::Text};
  if ( @$ ) {
    print "Error formatting help content! Perl-Module Pod::Text required!\n";
    print "To view help, please install Pod::Text module or look directly into\n";
    print "the perl script $0\n\n";
  }
  else {
    my $oPodParser = Pod::Text->new ();
    $oPodParser->parse_from_file($0);
  }
  exit 0;
}

if (! -e $sInputSeqFn) {
    die "EMBL Inputfile $sInputSeqFn not found";
}

my (undef, $sOutputDirName, undef) = fileparse($sOutputSeqFn);
if ($bVerbose) {
	print "Outputfile basedir is $sOutputDirName \n";
}

# If directory does not exist create it
if (! -e $sOutputDirName.$sHMMerDir) {

  mkdir $sOutputDirName.$sHMMerDir || die "Can't create $sHMMerDir\n";
}


# First we have to load the assembled Contigs
# ("Save all contigs" outputfile from consed)

$oSeqIOIn  =   Bio::SeqIO->new ('-file'    =>  $sInputSeqFn,
								'-format'  =>  'EMBL');

# create output object

$oSeqIOOut=Bio::SeqIO->new ('-file'   => ">$sOutputSeqFn",
			   				'-format' => 'EMBL');

# If requested generate single FASTA output file			    
if (defined $ssingleFASTA) {
	$oSeqIOSingleFasta = Bio::SeqIO->new ('-file'          => ">$ssingleFASTA",
										  '-format'        => "FASTA");
}			    

# For every Sequence in File...
while ($oSeq=$oSeqIOIn->next_seq) {

  # local variables
  my $iGeneCount=1;

  if ($bOnlyNew) {
    $iGeneCount = scalar (grep ( ( ($_->primary_tag eq 'CDS') &&
				  ($_->has_tag($sTag) ) ),
				 $oSeq->top_SeqFeatures));

    $iGeneCount++; # We have to do this, as array numbering starts with 0
                   # and our gene numbering starts with 1
  }

  # get sequence id
  my $sSeqID=$oSeq->id;
  
  # if ID is unknown check accession...
  
  if ($sSeqID=~/.*unknown.*/) {
  	$sSeqID = $oSeq->accession;
  }
  $sSeqID =~ s/\s/_/g;
  $sSeqID =~ s/\;$//;

  # now cycle though every identified orf..
  foreach my $oFeature (grep ((( $_->primary_tag eq 'CDS') &&
  								(! $_->has_tag("pseudo"))),
			       $oSeq->top_SeqFeatures)) {

      # to allow subsequence export we have to link our feature
      # with the sequence
      $oFeature->attach_seq($oSeq);

      # Set filnames
      my $sOrfId = "$sSeqID.orf$iGeneCount";
      my $sHMMerOutputFileName = "$sHMMerDir/$sOrfId";

      if ($oFeature->has_tag($sTag)) {

	# If only new files are to be processed goto next CDS-fetature,
	# as existing $sTag indicates that this feature has already
	# been processed
	if ($bOnlyNew) {
		if ($bVerbose) {
			print "Skipping CDS ",$oFeature->start,"..",$oFeature->end," as already processed \n";
		}
	  next;
	}
	else {

	  # If already a $sTag-tag is present remove it
	  $oFeature->remove_tag($sTag);
	}
      }

      # Set acutal file reference
      $oFeature->add_tag_value($sTag,$sHMMerOutputFileName.".hsr");

      # Get DNA-sequence of feature
      my $oOrfDNASeq=$oFeature->spliced_seq;
      if ($oFeature->strand eq -1) {
	$oOrfDNASeq->revcom;
      }

      # Translate to protein sequence and add translation tag
      if ($oFeature->has_tag('translation')) {
	$oFeature->remove_tag('translation');
      }
      my $oOrfProteinSeq=$oOrfDNASeq->translate(undef,undef,undef,11);
      $oFeature->add_tag_value('translation',$oOrfProteinSeq->seq);

      if (!-e "$sHMMerOutputFileName.fasta") {

		# Write ORF-Fasta protein sequence
		$oOrfProteinSeq->id($sOrfId);
		$oOrfProteinSeq->description(undef);
		my $oOrfProtIO=Bio::SeqIO->new('-file'    =>   ">$sOutputDirName$sHMMerOutputFileName.fasta",
				       '-format'  =>   'FASTA');
		$oOrfProtIO->write_seq($oOrfProteinSeq);
		if (defined $ssingleFASTA) {
      		$oSeqIOSingleFasta->write_seq($oOrfProteinSeq);
		}
      }	

      $iGeneCount++;
    }

  # Include all Features to output file
  $oSeqIOOut->write_seq($oSeq);
}
