#!/usr/bin/perl

=head1 NAME

evaluateDomains.pl

=head1 SYNOPSIS

evaluateDomains.pl --input <EMBL-Filename> --output <EMBL-Filename> [--annotate --feattag <TagName> --verbose --help]

=head1 DESCRIPTION

Certain specificity conferring catalytic grepdomains of secondary metabolite assembly line enzymes (PKSI/NRPS) can
be distinguished by specifically trained HMM-Profiles, each representing a single specificity. As the overall similarity
of the domains is very high, a HMMer search against the profile database does not result in a single hit (on
the model with the highest specificity) but on hits against all Profiles of this domain. Its specificty
can thus be determined by evaluating the HMMer score as the Profile denoting the correct specificity has the
highest scire.
So this script identifies overlapping/identical Hits to the same domain by the Profiles representig
different specificities and selects the one having the highest HNMer score. The other hits are discarded (or annotated
as a /note tag in the corresponding CDS_motif feature, if the command-line parameter --annotate is given.)

=head2 PARAMETERS
    --input <filename>         Name of EMBL-style file used for input
    --output <filename>        Name of (EMBL-style) file containing the evaluated annotation

=head2 OPTIONAL PARAMETERS
    --annotate	               Also annotate the scores/evalues of the other hits
    --feattag <tagname>        Specify tag that is used for motif annotation (default: CDS_motif)

    --verbose                  Print diagnostics information
    --help                     Show this documentation


=head3 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 Auf der Morgenstelle 28
 72076 Tuebingen
 Germany
 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

# So let the code begin...

# as the EMBL format changed mid 2006 we need to have a post Bioperl 1.4 version
# so we switch to bioperl live in my home directory

# use lib "/home/weber/perllib";

use strict;
use Bio::SeqIO;
use Getopt::Long;
use Data::Dumper;
use Sort::ArrayOfArrays;
use Config::General;
use FindBin qw ($Bin);

# Global variables declaration

my $sInputSeqFn  = '';    # Filename for InputFile
my $sOutputSeqFn = '';    # Filename for Outputfile

my $bVerbose;             # Verbose flag
my $bHelp;                # Help flag
my $bAnnotation;          # Flag whether to include additional annotation

my $oSeqIOIn;             # Input SeqIO-Object
my $oSeqIOOut;            # Output SeqIO-Object
my $oSeq;                 # Bio::Seq main object

# IMPORTANT: DATA STRUCTURE THAT CAN BE ANALYZED

# We look for these domains...

my @aPredAT = qw (PKSI-AT_malonylCoA AT_malonylCoA PKSI-AT_methylmalonylCoA);

# The specificity of NRPS-Condensation-LCL is not very high, so this profile also matches to some A-domains (albeit with)
# a comparably low score. This is why I temporarily added NRPS-A-domain here to get also a discrimination at A-domains)

my @aPredCond =
  qw (NRPS-Condensation-LCL NRPS-Condensation-DCL NRPS-Condensation-Starter NRPS-Condensation-Dual
  NRPS-HeteroCyc NRPS-epimerization NRPS-A-domain);
my @aPredCondATmot =
  qw (PKSI-AT-M_m1 PKSI-AT-M_m2 PKSI-AT-M_m3 PKSI-AT-M_m4 PKSI-AT-M_m5
  PKSI-AT-M_m6 PKSI-AT-M_m7 PKSI-AT-M_m8 PKSI-AT-M_m9
  PKSI-AT-mM_m1 PKSI-AT-mM_m2 PKSI-AT-mM_m3 PKSI-AT-mM_m4 PKSI-AT-mM_m5
  PKSI-AT-mM_m6 PKSI-AT-mM_m7 PKSI-AT-mM_m8 PKSI-AT-mM_m9);
my @aPredCondCmot =
  qw (C1_LCL_004-017 C2_LCL_024-062 C3_LCL_132-143 C4_LCL_164-176 C5_LCL_267-296 C67_LCL_14fromHMM
  C1_DCL_004-017 C2_DCL_024-062 C3_DCL_135-156 C4_DCL_171-183 C5_DCL_263-294 C67_DCL_14fromHMM
  C1_dual_004-017 C2_dual_024-063 C3_dual_135-156 C4_dual_169-182 C5_dual_260-291 C67_dual_14fromHMM
  C1_starter_004-017 C2_starter_024-063 C3_starter_134-155 C4_starter_171-183 C5_starter_265-294 C67_starter_14fromHMM
  Cy1 Cy2 Cy3 Cy4 Cy5 Cy6 Cy7
  NRPS-E1 NRPS-E2 NRPS-E3 NRPS-E4 NRPS-E5 NRPS-E6 NRPS-E7);

my @aPredACPPCP = qw (PKSI-ACP PCP_C PCP_E);
my @aPredDH     = qw (PKSI-DH PKSI-DH_ATless);
my @otherDomains = qw (PKSI-KS_C PKSI-KS_N PKSI-MT PKSI-KR PKSI-ER Thioesterase);

# These are general profiles, we can ignore these
# as we have more specific ones.
my @aIgnore     = qw (Acyl_transf_all NRPS-epimerization-e60);

# Now put all domains into @aAvailDomains
my @aAvailDomains;
push( @aAvailDomains, @aPredAT );
push( @aAvailDomains, @aPredCond );
push( @aAvailDomains, @aPredCondATmot );
push( @aAvailDomains, @aPredCondCmot );
push( @aAvailDomains, @aPredACPPCP );
push( @aAvailDomains, @aPredDH );
push ( @aAvailDomains, @otherDomains);
push( @aAvailDomains, @aIgnore );

my $CLUSEANROOT;
if ( defined $ENV{CLUSEANROOT} ) {
    $CLUSEANROOT = $ENV{CLUSEANROOT};
}
else {
    $CLUSEANROOT = $Bin;
    $CLUSEANROOT =~ s/(.*)\/.*?$/$1/;
}

# Parse Config file
my $oConfig =
  Config::General->new( '-ConfigFile' => "$CLUSEANROOT/etc/CLUSEAN.cfg" );
my %hConfig = $oConfig->getall;

my $sFeatureTag =
  $hConfig{EvaluateDomains}->{HMMerQual};    # Tag for domain analysis
my $fMinOverlap = $hConfig{EvaluateDomains}->{OverlapFrac}
  || 0.5
  ; # Fraction that features have to minimally overlap to be regarded as a group

# Evaluate Cmdline and assign variables

GetOptions(
    'input|i=s'   => \$sInputSeqFn,
    'output|o=s'  => \$sOutputSeqFn,
    'overlap=s'   => \$fMinOverlap,
    'feattag|t=s' => \$sFeatureTag,
    'annotate|a'  => \$bAnnotation,
    'verbose|v'   => \$bVerbose,
    'help|h'      => \$bHelp
);

# Check parameters

if ($bHelp) {
    print "\n\n";
    eval { require Pod::Text };
    if (@$) {
        print
          "Error formatting help content! Perl-Module Pod::Text required!\n";
        print
"To view help, please install Pod::Text module or look directly into\n";
        print "the perl script $0\n\n";
    }
    else {
        my $oPodParser = Pod::Text->new();
        $oPodParser->parse_from_file($0);
    }

    #  print `pod2text $0`;
    exit 0;
}

if ( !-e $sInputSeqFn ) {
    die "EMBL Inputfile $sInputSeqFn not found";
}

$oSeqIOIn = Bio::SeqIO->new(
    '-file'   => $sInputSeqFn,
    '-format' => 'EMBL'
);

# create output object

$oSeqIOOut = Bio::SeqIO->new(
    '-file'   => ">$sOutputSeqFn",
    '-format' => 'EMBL'
);

# For every Sequence in File (normally only one sequence per file...)
while ( $oSeq = $oSeqIOIn->next_seq ) {

    # Get all features and flush features in $oSeq
    my @oFeatures = $oSeq->remove_SeqFeatures;
    my @aDomains;
    my @AoDoms
      ; # Array of Domains, structure (i, [(start, end, type, score, eval, \$oFeature)])
    my @sortedAoDoms;    # dito, but sorted
    my @AoAoDoms;        # Array of Array of overlapping Domains (j, \@AoDoms)
    my @test;

    # Now filter out the features this script deals with, transfer all others
    # back to $oSeq

    my $iNoOfFeat = scalar(@oFeatures);
    if ($bVerbose) {
        print "We have to process $iNoOfFeat features\n";
    }

    my $j;
    for ( my $i = 0 ; $i < $iNoOfFeat ; $i++ ) {

        # Get Feature object form @oFeatures
        my $oFeature = shift(@oFeatures);
        if ($bVerbose) {
            print "processing feature $i: ", $oFeature->primary_tag,
              " starting at ", $oFeature->start, "\n";
        }

        # If it is not CDS_motif put it back to $oSeq and go to next feature
        if ( !( $oFeature->primary_tag eq $sFeatureTag ) ) {
            if ($bVerbose) {
                print "adding ", $oFeature->primary_tag, " starting at ",
                  $oFeature->start, " back\n";
            }
            $oSeq->add_SeqFeature($oFeature);
            $j++;
        }
        else {

            # If it is a CDS_motif other than we handle, ignore it
            ( my $sLabel ) = $oFeature->get_tag_values('label');
            if ( !grep ( $_ eq $sLabel, @aAvailDomains ) ) {
                if ($bVerbose) {
                    print "adding ", $oFeature->primary_tag, " starting at ",
                      $oFeature->start, " with tag ", $sLabel, "\n";
                }

                # as we want to ignore this feature we don't add it back
                $j++;
            }

            # If the feature is in the @aIgnore list ignore it
            elsif ( grep ( $_ eq $sLabel, @aIgnore ) ) {
                if ($bVerbose) {
                    print "removing ", $oFeature->primary_tag, " starting at ",
                      $oFeature->start, " with tag ", $sLabel, "\n";
                }

                # as we want to ignore this feature we don't add it back
                $j++;
            }
            else {

          # now this feature is one we have to analyze. So put it into @aDomains
                push( @aDomains, $oFeature );
                next;
            }

        }

    }
    if ($bVerbose) {
        print "OK, \@aDomains now has ", scalar @aDomains, " elements\n";
        print "$j elements were already transfered to output file\n";
    }

    # get number of Features we have to deal with, reuse $iNoOfFeat
    $iNoOfFeat = scalar(@aDomains);

    for ( my $i = 0 ; $i < $iNoOfFeat ; $i++ ) {
        my $oFeature = $aDomains[$i];
        ( my $sNoteTag ) =
          grep ( /^.*-\s?Hit:/, $oFeature->get_tag_values('note') );
          
        my ( $fScore, $fEvalue ) =
          $sNoteTag =~ /.*Score: (.*)\. E- *value: (.*)\. Domain.*/;
        # as the EMBL parser allows libne breaks after hyphens, which result in inserting a space when parsed again,
        # we have to remove this space to get a real number  
        $fEvalue =~ s/\s//g;  

#print $oFeature->primary_tag," ", $oFeature->start," ", $oFeature->get_tag_values('label'),"\n";
        push(
            @AoDoms,
            [
                (
                    $oFeature->start,                   $oFeature->end,
                    $oFeature->get_tag_values('label'), $fScore,
                    $fEvalue,                           \$oFeature
                )
            ]
        );
    }

# now we have to make sure, that the CDS-motifs are sorted (and that we have more than 1 element in our array)
    if ( $iNoOfFeat > 0 ) {
        my $oSorter = Sort::ArrayOfArrays->new(
            {
                'results'     => \@AoDoms,
                'sort_column' => '1,2'
            }
        );
        @sortedAoDoms = @{ $oSorter->sort_it };

    }
    else {
        @sortedAoDoms = @AoDoms;
    }

# OK, now we have all information of all CDS_motifs we can deal with in a sorted two dimensional Array
# @sortedAoDoms

# Next step is to find out, which annotations describe the same domain (which means) they overlap.
# So let's add another dimension to the array to store this information

    # delete @AoDoms so that we can reuse the variable
    @AoDoms = ();

# Strategy to group domains:
# If domains overlap store them in @AoDoms
# As our Domain collection @sortedAoDoms is sorted, non-overlap means that the domain does not
# belong to the same group for Score/Eval evaluation. So put all domains in @AoDoms into the
# 3 dimensional array @AoAoDoms. The fist index of @AoAoDoms indicates the group

    # To start we have to put the first daomain into @AoDoms
    push @AoDoms, $sortedAoDoms[0];
    if ($bVerbose) {
        print
"Analyzing \$i=0: $sortedAoDoms[0][0]..$sortedAoDoms[0][1] of type $sortedAoDoms[0][2]\n";
    }

    # Now for every domain n in @sortedAoDoms...
    for ( my $i = 0 ; $i < $iNoOfFeat - 1 ; $i++ ) {

        # ..get SeqFeature object of domain n
        my $oFeature0 = ${ $sortedAoDoms[$i][5] };

        # ..and of domain n+1
        my $oFeature1 = ${ $sortedAoDoms[ $i + 1 ][5] };

        # If there is overlap
        my $iFeature0Length = $oFeature0->length;
        ( undef, my $iFeatureOverlap, undef ) =
          $oFeature0->overlap_extent($oFeature1);
        if (   ( $oFeature0->overlaps($oFeature1) )
            && ( $iFeatureOverlap / $iFeature0Length > $fMinOverlap ) )
        {

            # store domain n+1 in @AoADoms (domain n already is in @AoADoms)
            push @AoDoms, $sortedAoDoms[ $i + 1 ];
            if ($bVerbose) {
                print
"  found overlapping domain $sortedAoDoms[$i+1][0]..$sortedAoDoms[$i+1][1] of type $sortedAoDoms[$i+1][2]\n";
            }
        }

        # If there is no overlap
        else {

            # put @AoDoms into @AoAoDoms
            push @AoAoDoms, [@AoDoms];

            # reset @AoDoms
            @AoDoms = ();

            # and put domain n+1 into empty @AoDoms
            push @AoDoms, $sortedAoDoms[ $i + 1 ];
            if ($bVerbose) {
                print "Analyzing \$i+1=", $i + 1,
                  ": $sortedAoDoms[$i+1]->[0]..$sortedAoDoms[$i+1]->[1]\n";
            }
        }

    }

    # Put last domain into @AoAoDoms
    push @AoAoDoms, [@AoDoms];

    my $iNoOfGroups = scalar(@AoAoDoms);
    if ($bVerbose) {
        print "\nFound $iNoOfGroups groups of domains!\n";
    }

    # Now analyze each group
    for ( my $i = 0 ; $i < $iNoOfGroups ; $i++ ) {
        @AoDoms = ();
        @AoDoms = @{ $AoAoDoms[$i] };

        # Lets sort to get the hit with the best score...

        my $oSorter = Sort::ArrayOfArrays->new(
            {
                'results'     => \@AoDoms,
                'sort_column' => '3, 4',
                'sort_code'   => {
                    '3' => 'nd',
                    '4' => 'nd'
                }
            }
        );
        my @sAoDoms = @{ $oSorter->sort_it };

# Now get winning Feature out of @AoDoms => as the array is sorted it is $sAoDoms[0][5]
        my $oFeature = ${ $sAoDoms[0][5] };

        # and add it to our sequence...
        $oSeq->add_SeqFeature($oFeature);
        if ($bAnnotation) {
            next if ( scalar(@sAoDoms) < 2 );
            my $sNoteTag = "Discriminated HMM-Hits:";

            for ( my $j = 1 ; $j < scalar(@sAoDoms) ; $j++ ) {

                $sNoteTag .=
" $sAoDoms[$j][2] (Score: $sAoDoms[$j][3], E-value: $sAoDoms[$j][4])";
            }
            $oFeature->add_tag_value( 'note', $sNoteTag );
        }

        if ($bVerbose) {
            print "Group $i\n";
            for ( my $j = 0 ; $j < scalar(@sAoDoms) ; $j++ ) {
                print
"$sAoDoms[$j][0]..$sAoDoms[$j][1] of type $sAoDoms[$j][2] with score $sAoDoms[$j][3] and Evalue $sAoDoms[$j][4] \n";
            }

        }
    }

    # finally write sequence
    $oSeqIOOut->write_seq($oSeq);
}
