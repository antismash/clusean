#!/usr/bin/perl
#
# This script tests the prerequisites of CLUSEAN
#
#

use strict;

my $configfile = $ARGV[0] ? $ARGV[0] : undef;

my @LibrariesToCheck  = qw (Getopt::Long
                            File::Copy
                            File::Temp
                            File::Basename
                            Config::General
                            FindBin
                            Data::Dumper
                            Sort::ArrayOfArrays
                            Bio::Annotation::Comment
                            Bio::SearchIO
                            Bio::SearchIO::hmmer3
                            Bio::Seq
                            Bio::SeqIO
                            Bio::SeqFeature::Generic
                           );

my @MissingLib;

my @CLUSEANLibsToCheck = qw (CLUSEAN::NRPSPredIO
                             CLUSEAN::NRPSPred
                             CLUSEAN::HMMParse
                             CLUSEAN::AssignFunction
                             CLUSEAN::GetDomains
                            );
my @CLUSEANMissingLib;

my @BinariesToCheck = qw ( hmmscan
                         );
my @MissingBin;

my $ErrorFlag;

print "Checking installed libraries \n";
print "#############################################\n";

foreach my $Library (@LibrariesToCheck) {
    print "Checking $Library...\t";
    eval ("require $Library");
    if ( $@ ) {
        print "missing! \n";
        push (@MissingLib, $Library);
    }
    else {
        print "present. Version is ", $Library->VERSION,"\n";
    }
}

# Special case for blastp/blastall
print "Checking whether a BLAST binary is accessible...\t";
system ("blastp > /dev/null 2>&1");
my $exit_code = $? >> 8;
my $blast_executable = "";
if ($exit_code == 127) {
    # Blast+ not found, let's try blastall from the old Blast distribution
    system ("blastall > /dev/null 2>&1");
    $exit_code = $? >> 8;
    if ($exit_code == 127) {
        print "ERROR: not found\n";
        push (@MissingBin, "blastp or blastall");
    } else {
        print "blastall\n";
        $blast_executable = "blastall";
    }
} else {
    print "blastp\n";
    $blast_executable = "blastp";
}

# Special case for dialog
print "Checking whether the dialog binary is accessible...\t";
system ("dialog > /dev/null 2>&1");
$exit_code = $? >> 8;
# Provide a fallback using the unix "true" command
my $dialog = "true";
if ($exit_code == 127) {
    print "No. Consider installing dialog for nicer output.\n";
} else {
    print "found, exit code $exit_code.\n";
    $dialog="dialog"
}

foreach my $Binary (@BinariesToCheck) {
    print "Checking whether executable $Binary is accessible...\t";
    system ("$Binary > /dev/null 2>&1");
    # When executing a shell, the return value of the command is in the upper word
    my $exit_code = $? >> 8;
    if ($exit_code == 127) {
        print "ERROR: not found\n";
        push (@MissingBin, $Binary)
    }
    else {
        print "found, exit code $exit_code\n";
    }
}

foreach my $Library (@CLUSEANLibsToCheck) {
    print "Checking CLUSEAN $Library...\t";
    eval ("require $Library");
    if ( $@ ) {
        print "missing! \n";
        push (@CLUSEANMissingLib, $Library);
    }
    else {
        print "present.\n";
    }
}

print "\n";
print "########################################\n";
print "SUMMARY:\n";

if (scalar @MissingLib > 0) {

    print "The following libraries have to be installed before you can use the CLUSEAN scripts!\n";
    print "Please use CPAN (LINUX) or PPM (Windows) to install these libraries\n\n";
    foreach my $Library (@MissingLib) {
        print $Library,"\n\n\n";
        $ErrorFlag = 1;
    }
}

if (scalar @CLUSEANMissingLib > 0) {

    print "The following CLUSEAN libraries were not accessible by perl\n";
    print "Please add the CLUSEAN root directory to your PERLLIB environment variable e.g. by \n";
    print "export PERLLIB=/home/username/CLUSEAN on the BASH command line prompt\n\n";
    foreach my $Library (@CLUSEANMissingLib) {
        print $Library,"\n\n\n";
        $ErrorFlag = 1
    }
}

if (scalar @MissingBin > 0) {
    print "\n";
    print "########################################\n";
    print "The following binaries could not be executed within the perl scripts\n";
    foreach my $Binary (@MissingBin) {
        print $Binary,"\n";
        $ErrorFlag = 1
    }
}

if (! defined $ErrorFlag) {
    print "All libraries are installed, no errors found\n";
    exit 0;
}
exit 1;
