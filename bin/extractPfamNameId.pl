#!/usr/bin/perl -w
#
=head1 NAME

extractPfamNameId.pl

=head1 SYNOPSIS

extractPfamNameId.pl --input pfam-db --output result.pm [--name Perl::Module::Name]

=head1 DESCRIPTION

Extract PFAM accession IDs from a hmm database file and store it in
an array. This is put into a perl module for easier loading.

=head2 PARAMETERS

=head3
   --input <filename>                :       Input file (Hmmer database file)
   --output <filename>               :       Output filename
   --name Name                       :       Optional name for the module
   --verbose                         :       verbose output
   --help                            :       print help

=head3 AUTHOR

 Kai Blin
 Microbiology/Biotechnology
 Auf der Morgenstelle 28
 72076 Tuebingen
 Germany
 Email: kai.blin@biotech.uni-tuebingen.de

=cut

use strict;
use Getopt::Long;

my $in_file = "Pfam-A.hmm";
my $out_file = "NameToPfam.pm";
my $mod_name = "CLUSEAN::NameToPfam";
my $verbose = 0;
my $help = 0;

GetOptions(
    'input|i=s'    => \$in_file,
    'output|o=s'   => \$out_file,
    'name|n=s'  => \$mod_name,
    'verbose|v'    => \$verbose,
    'help|h'       => \$help
);

if ($help) {
    print "\n";
    eval { require Pod::Text };
    if (@$) {
        print
          "Error formatting help content! Perl-Module Pod::Text required!\n";
        print
          "To view help, please install Pod::Text module or look directly\n";
        print "into the perl script $0\n\n";
    }
    else {
        my $oPodParser = Pod::Text->new();
        $oPodParser->parse_from_file($0);
    }
    exit 0;
}

if (! -e $in_file ) {
    die "Input file $in_file does not exist";
}

open(my $in, $in_file) || die "Failed to open infile $in_file";
open(OUT, ">", $out_file) ||
    die "Failed to open outfile $out_file for writing";

my %name_to_id = ();

while(defined($_ = <$in>)) {
    next unless (m/^NAME\s+(\S+)\n/);
    my $name = $1;
    my $id;
    defined( $_ = <$in>) || die "Failed to read HMMer db";
    next unless (m/^ACC\s+(\S+)\.\S+\n/);
    $id = $1;
    $name_to_id{$name} = $id;
}

close($in);

print OUT <<EOF;
package $mod_name;
require Exporter;
our \@ISA      = qw(Exporter);
our \@EXPORT   = qw(name_to_pfam);
our \$VERSION  = 1.001;

use strict;
my %name_to_id = (
EOF

foreach my $key (keys %name_to_id) {
    print OUT "    \"" . $key . "\" => \"" . $name_to_id{$key} . "\",\n";
}

print OUT <<EOF;
    );

sub name_to_pfam {
    my \$key = shift;
    return \$name_to_id{\$key};
}

1;
EOF

close OUT;
