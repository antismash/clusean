#!/usr/bin/perl -w
#
=head1 NAME

clusterPfam.pl

=head1 SYNOPSIS

clusterPfam.pl --input filename --output filename.py

=head1 DESCRIPTION

Get a list of PFAM domains in the order they are occuring on the genome.
This needs to be read by a python script, so generate a python module file
we can import immediately.

=head2 PARAMETERS

=head3
   --input <filename>                :       Input filename
   --output <filename>               :       Output filename
   --verbose                         :       verbose output
   --help                            :       print help

=head3 AUTHOR

 Kai Blin
 Microbiology/Biotechnology
 Auf der Morgenstelle 28
 72076 Tuebingen
 Germany
 Email: kai.blin@biotech.uni-tuebingen.de

=cut

use strict;
use Getopt::Long;
use Bio::SeqIO;

my $in_file = undef;
my $out_file = undef;
my $verbose = 0;
my $help = 0;

GetOptions(
    'input|i=s'    => \$in_file,
    'output|o=s'   => \$out_file,
    'verbose|v'    => \$verbose,
    'help|h'       => \$help
);

if ($help) {
    print "\n";
    eval { require Pod::Text };
    if (@$) {
        print
          "Error formatting help content! Perl-Module Pod::Text required!\n";
        print
          "To view help, please install Pod::Text module or look directly\n";
        print "into the perl script $0\n\n";
    }
    else {
        my $pod = Pod::Text->new();
        $pod->parse_from_file($0);
    }
    exit 0;
}

if (! -e $in_file ) {
    die "Input file $in_file does not exist";
}

my $in = Bio::SeqIO->new(
    '-file'   => $in_file,
    '-format' => 'EMBL'
);


#FIXME: Only runs for the first sequence (should make no difference usually)
my $seq = $in->next_seq;
my @cds_motives = grep ($_->primary_tag eq 'CDS_motif', $seq->top_SeqFeatures);
#@cds_motives = sort { ($a->start != $b->start)? $a->start <=> $b->start : $a->end <=> $b->end } @cds_motives;
my %pfam_by_orf;
my @pfam_matches = ();
foreach my $motif (@cds_motives) {
    next unless ($motif->has_tag('note'));
    my @tag_values = $motif->get_tag_values('note');
    (my $pfam) = grep(/^PFAM-Id:/, @tag_values);
    (my $score) = grep(/-Hit:/, @tag_values);
    $score =~ s/^.*Score:\s+(\S+)\.\s+.*$/$1/;
    next unless (defined $pfam);
    next unless ($score > 7);
    $pfam =~ s/^PFAM-Id:\s//;
    (my $name) = $motif->get_tag_values('locus_tag');
    my $start = $motif->start;
    my $end = $motif->end;
    my @match = ($pfam, $name, $start, $end, $score);

    if(!defined($pfam_by_orf{$name})) {
        $pfam_by_orf{$name} = [\@match];
        next;
    }

    my $needs_add = 1;
    foreach my $old_match (@{$pfam_by_orf{$name}}) {
        my ($old_pfam, $old_name, $old_start, $old_end, $old_score) = @{$old_match};

        if (!( $end < $old_start || $old_end < $start)) {
            $needs_add = 0;
            if ($old_score < $score) {
                $old_match = \@match;
                last;
            }
        }
    }
    if ($needs_add) {
        push @{$pfam_by_orf{$name}}, \@match;
    }
}

foreach my $match (values %pfam_by_orf) {
    push @pfam_matches, @{$match};
}

@pfam_matches = sort { $$a[2] <=> $$b[2] } @pfam_matches;

open(OUT, ">", $out_file) ||
    die "Failed to open outfile $out_file for writing";

print OUT <<EOF;
#!/usr/bin/env python
#
# AUTOMATICALLY GENERATED FILE. DO NOT EDIT.

pfam_ids = [
EOF

foreach my $match (@pfam_matches) {
    my ($pfam, $name, $start, $end, $score) = @{$match};
    print OUT "        ('$pfam', '$name', $start, $end), #$score\n";
}
print OUT "    ]\n";

close OUT;
