#!/usr/bin/perl
#
# Author: Tilmann Weber
#
# This program runs analyses based on the extraction of specific amino acids
#
# InputFiles: Fasta-Formatted-protein sequence file
#
# Output: Report
#
#

=head1 NAME

SpecSitePredict.pl

=head1 SYNOPSIS

SpecSitePredict.pl  --input <format-EMBL-file> [--csv <CSV-file>
--noCSVheader --verbose --help]

=head1 DESCRIPTION

	Identifies and annotates conserved amino acids within specific protein domains
	Configuration: $CLUSEANROOT/etc/SignatureResources.xml

=head2 PARAMETERS

=head3
   --input <filename>                :       Input file (in EMBL-format)
   --onlynew                         :       Only process CDS features with no
                                             previous results
   --csv <filename>					  :		   In addition to EMBL export, also export as CSV
   --noCSVheader					     :		   Omit CSV headerline (useful for automated processing of
   											 the CSV line	                                             
                                             
   --verbose                         :       verbose output
   --help                            :       print help

=head3 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 Auf der Morgenstelle 28
 72076 Tuebingen
 Germany
 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

use strict;
use CLUSEAN::AligAnal;
use CLUSEAN::Generic;
use CLUSEAN::GetDomains;
use Bio::SeqIO;
use Carp;
use Getopt::Long;
use XML::Simple;
use FindBin qw ($Bin);
use Config::General;
use File::Basename;
use Data::Dumper;
use File::Temp qw(tempdir);

my $sInputSeqFn;    # Filename for InputFile
my $sCSVOutFn;      # Filename for annotation output file
my $bVerbose;       # Verbose flag
my $bHelp;          # Help flag
my $bNoCSVHead;     # Don't print CSV-Headerline of field descriptions
my $oSeqIOIn;       # Input SeqIO-Object
my $oSeqIOOut;      # Output SeqIO-Object
my $oSeq;           # Bio::Seq main object

#Import Configuration data
my $CLUSEANROOT;
if ( defined $ENV{CLUSEANROOT} ) {
	$CLUSEANROOT = $ENV{CLUSEANROOT};
}
else {
	$CLUSEANROOT = $Bin;
	$CLUSEANROOT =~ s/(.*)\/.*?$/$1/;
}

# Parse Config file
my $oConfig =
  Config::General->new( '-ConfigFile' => "$CLUSEANROOT/etc/CLUSEAN.cfg" );
my %hConfig = $oConfig->getall;

my $sSeparator = $hConfig{Formatting}->{Separator}
  || "|";    # Character for joining entries;

my $sXMLFileName = $hConfig{SignatureResources}->{SignatureResourceFile};

if ( !-e "$CLUSEANROOT/$sXMLFileName" ) {
	die "Can't find Singnature XML file at $CLUSEANROOT/$sXMLFileName\n";
}

# Parse XML-File and assign hashref
my $xml = XML::Simple->new(
	'ForceArray' => [ ( 'choice', 'scaffold' ) ],
	'SuppressEmpty' => 1 );
my $xmlHash = $xml->XMLin("$CLUSEANROOT/$sXMLFileName");

# Evaluate Cmdline and assign variables
GetOptions(
			'input|i=s'     => \$sInputSeqFn,
			'csv|c=s'       => \$sCSVOutFn,
			'noCSVHeader|n' => \$bNoCSVHead,
			'verbose|v'     => \$bVerbose,
			'help|h'        => \$bHelp
);

# Check parameters
if ($bHelp) {
	print "\n\n";
	eval { require Pod::Text };
	if (@$) {
		print
		  "Error formatting help content! Perl-Module Pod::Text required!\n";
		print
"To view help, please install Pod::Text module or look directly into\n";
		print "the perl script $0\n\n";
	}
	else {
		my $oPodParser = Pod::Text->new();
		$oPodParser->parse_from_file($0);
	}
	exit 0;
}
if ( !-e $sInputSeqFn ) {
	die "EMBL Inputfile $sInputSeqFn not found";
}

my ( undef, $sInputDirName, undef ) = fileparse($sInputSeqFn);

# First we have to load fasta-file
$oSeqIOIn = Bio::SeqIO->new( '-file'   => $sInputSeqFn,
							 '-format' => 'fasta' );

if ( defined $sCSVOutFn ) {

	# open Filehandle for CSV output
	open( CSVOUT, ">$sCSVOutFn" ) || die "Cannot open $sCSVOutFn for writing";

	if ( !defined $bNoCSVHead ) {
		my @aHeader = (
						"Sequence ID",
						"Analysis name",
						"Analysis description",
						"Domain start",
						"Domain End",
						"QueryScaffold",
						"HitScaffold",
						"Scaff. binary comp.",
						"Extracted residues signature",
						"Corresponding signature of hit",
						"Reference signature to compare with",
						"Binary comparison",
						"Result Name",
						"Description of Result"
		);
		print CSVOUT join( $sSeparator, @aHeader ), "\n";
	}
}

# For every Sequence in File...
while ( $oSeq = $oSeqIOIn->next_seq ) {

	# get sequence id
	my $sSeqID = $oSeq->id;

	# if ID is unknown check accession...
	if ( $sSeqID =~ /.*unknown.*/ ) {
		$sSeqID = $oSeq->accession;
	}
	$sSeqID =~ s/\s/_/g;
	$sSeqID =~ s/\;$//;
	my @aAnalyses = keys( %{$xmlHash} );

	print "Checking... \n";

	# For each analysis defined in SignatureResources.xml file
	foreach my $sAnalysis (@aAnalyses) {

		# Skip xmlns data...
		next if ( $sAnalysis =~ /xmlns.*/ );

		# only run on prediction type analyses

		next if ( $xmlHash->{$sAnalysis}->{type} !~ /prediction/ );

		print "---------\n";
		print "Analysis: $sAnalysis\n";
		print "---------\n";

		# First, we have to execute hmmer to identify domains
		# to help debugging: Lets start the tempfile with date/time
		my ( $sec, $min, $hour, $day, $mon, $year, $wday, $yday, $isdst ) =
		  localtime;
		$year += 1900;
		$mon++;

		# Create temporary directory below $sTempDirBase
		my $sTempDirBase = $ENV{TEMP}
		  || '/tmp'; # Temp dir, use /tmp if no TEMP environment variable is set
		my $sTempDir = tempdir(
			   "AligAnal_" . $year . $mon . $day . $hour . $min . $sec . "XXXX",
			   DIR     => $sTempDirBase,
			   CLEANUP => 1
		);
		open( FASTAOUT, ">$sTempDir/FeatTransl.fasta" )
		  || throw("Can't open $sTempDir/FeatTransl.fasta for writing!\n");

		# OK, write Fastafile
		print FASTAOUT ">tempFasta\n";
		print FASTAOUT $oSeq->seq;
		close(FASTAOUT);

		# Get run parameters from XML config file and assemble commandline
		my $sCmdLine = $xmlHash->{$sAnalysis}->{'Execute'}->{'program'};
		$sCmdLine .= " " . $xmlHash->{$sAnalysis}->{'Execute'}->{'parameters'};
		if ( defined $xmlHash->{$sAnalysis}->{'Execute'}->{'dbprefix'} ) {
			$sCmdLine .=
			  " " . $xmlHash->{$sAnalysis}->{'Execute'}->{'dbprefix'};
		}
		$sCmdLine .= " " . $CLUSEANROOT . "/lib/";
		$sCmdLine .= $xmlHash->{$sAnalysis}->{'Execute'}->{'database'};
		if ( defined $xmlHash->{$sAnalysis}->{'Execute'}->{'inputfileprefix'} )
		{
			$sCmdLine .=
			  " " . $xmlHash->{$$sAnalysis}->{'Execute'}->{'inputfileprefix'};
		}
		$sCmdLine .= " " . $sTempDir . "/FeatTransl.fasta";
		if ( $xmlHash->{$sAnalysis}->{'Execute'}->{'CaptureConsole'} eq "TRUE" )
		{
			open( OUTFILE, ">$sTempDir/Analysis.out" )
			  || throw("Can't open file for tool output");
			my $Output = `$sCmdLine`
			  || throw("Error executing:\n$sCmdLine\n");
			print OUTFILE $Output;
			close(OUTFILE);
		}
		else {
			if (
				 defined $xmlHash->{$sAnalysis}->{'Execute'}
				 ->{'outputfileprefix'} )
			{
				$sCmdLine .= " "
				  . $xmlHash->{$sAnalysis}->{'Execute'}->{'outputfileprefix'};
			}
			$sCmdLine .= " " . $sTempDir . "/Analysis.out";
			system($sCmdLine) || throw("Error executing:\n$sCmdLine\n");
		}

# OK, now we have run the analysis (jut tested with HMMer, but Blast should also work

		my %ParserSelect = (
							 'hmmpfam'  => 'hmmer',
							 'hmmscan'  => 'hmmer3',
							 'blastall' => 'blast'
		);

		# Set up Bio::SearchIO facility

		my $oSearchIO =
		  Bio::SearchIO->new(
			 '-file' => $sTempDir . "/Analysis.out",
			 '-format' =>
			   $ParserSelect{ $xmlHash->{$sAnalysis}->{'Execute'}->{'program'} }
		  );

# As the databases only contain a single result we don't have to cycle through results...
		
		my $oResult = $oSearchIO->next_result;

		# OK, here also only 1 hit is expected, but to be sure we'll take a loop
		while ( my $oHit = $oResult->next_hit ) {

			while ( my $oHSP = $oHit->next_hsp ) {

# skip analysis if HSP does not fulfill evalue/score threshold (defined in XML file)
				if (
					 (
					   $oHSP->evalue >
					   $xmlHash->{$sAnalysis}->{'Execute'}->{'evalue'}
					 )
					 || ( $oHSP->score <
						  $xmlHash->{$sAnalysis}->{'Execute'}->{'score'} )
				  )
				{
					print "Skipping hit against ",
					  $xmlHash->{$sAnalysis}->{'Execute'}->{'database'}, ".\n";
					print "HSP: ", $oHSP->start, "..", $oHSP->end, " score: ",
					  $oHSP->score, " Evalue ", $oHSP->evalue, "\n";
					next;
				}

				print "Hit found for ",
				  $xmlHash->{$sAnalysis}->{'Execute'}->{'database'}, ".\n";
				print "HSP: ", $oHSP->start, "..", $oHSP->end, " score: ",
				  $oHSP->score, " Evalue ", $oHSP->evalue, "\n";

				# Generate AligAnal object
				my $oAligAnal =
				  CLUSEAN::AligAnal->new(
										  '-xml_Hash' => $xmlHash,
										  '-aaSeq'    => $oSeq->seq,
										  '-oHSP'     => $oHSP,
										  '-program'  => $sAnalysis
				  );

				print "************\n";
				print "Running analysis: $sAnalysis \n";

				my @aChoices = @{ $oAligAnal->get_choiceArray };

				print "Available Choices: ";
				if ( scalar @aChoices == 0 ) {
					print "none \n";
				}
				else {
					print join( ", ", @aChoices ), "\n";
				}
				print "************\n";

				# Retrieve Scaffold information
				my @aScaffoldHit = @{
					$oAligAnal->ExtractResidues(
						   @{ $oAligAnal->get_aScaffoldOffset }
					  )->get_aHit
				  };
				my @aScaffoldQuery = @{
					$oAligAnal->ExtractResidues(
											@{ $oAligAnal->get_aScaffoldOffset }
					  )->get_aQuery
				  };
				my @aScaffoldEmission = @{ $oAligAnal->get_aScaffoldEmission };

				# Check whether scaffolds match
				my $oScaffCompArray = CLUSEAN::Generic->new;

				my @ScaffCompArray =
				  $oScaffCompArray->compareArray( '-Array1' => \@aScaffoldHit,
												  '-Array2' => \@aScaffoldQuery
				  );

				my $sScaffCompString = join( "", @ScaffCompArray )
				  ; # @CompArray does contain 1 for every match and 0 for mismatch

				# Output Scaffold information

				print "###########################\n";
				print "QueryCoordinates       : ",
				  $oAligAnal->get_HSP_startQ, "..",
				  $oAligAnal->get_HSP_endQ,   "\n";

				print "H-Scaffold             : ", @aScaffoldHit,   "\n";
				print "Q-Scaffold             : ", @aScaffoldQuery, "\n";
				print "MatchArray             : $sScaffCompString\n";
				print "Offsets to HMM profile : ",
				  join( ", ", @{ $oAligAnal->get_aScaffoldOffset } ), "\n";
				print "ScaffoldEmission       : ",
				  join( ", ", @aScaffoldEmission ),
				  "\n";

				if ( $sScaffCompString =~ /0/ ) {
					print "CAUTION: Scaffold sequence does not match 100% \n";
				}
				else {
					print "Scaffold sequences match :-)\n";
				}
				print "###########################\n";

				# Now cycle through available comparisonsi
				while ( my $oComp = $oAligAnal->next_comparisonData ) {

					{
						my $ExtrResidues = $oAligAnal->ExtractResidues(
													 @{ $oComp->get_aOffset } );

						my $oCompArray = CLUSEAN::Generic->new;
						my @CompArray =
						  $oCompArray->compareArray(
										 '-Array1' => $ExtrResidues->get_aQuery,
										 '-Array2' => $oComp->get_aValue );

						print "\n\n---------\n";
						print "Testing choice   : ", $oComp->get_result, "\n";
						print "\n";

						print "H-ExtrRes        : ",
						  @{ $ExtrResidues->get_aHit },
						  "\n";
						print "Q-ExtrRes        : ",
						  @{ $ExtrResidues->get_aQuery },
						  "\n";
						print "Offset to HMMprof: ", $oComp->get_sOffset, "\n";

						print "Compared against : ", @{ $oComp->get_aValue },
						  "\n";
						print "ResultA          : ", @CompArray, "\n";
						print "Emission         : ",
						  join( ", ", @{ $oComp->get_aValueEmission } ), "\n";

						if ( join( "", @CompArray ) =~ /0/ ) {
							print "NO MATCH AGAINST ", $oComp->get_result, "\n";
						}
						else {
							print "Perfect match for ", $oComp->get_result,
							  "\n";
						}

						print "------------\n\n";
						if ($sCSVOutFn) {

							my $sSeqID = $oSeq->id;
							$sSeqID =~ s/;$//;

							# Assemble output line
							# Accession number |
							my $sLine = $sSeqID . $sSeparator;

							# AnalysisName |
							$sLine .=
							    $sAnalysis
							  . $sSeparator
							  . $xmlHash->{$sAnalysis}->{'description'}
							  . $sSeparator;

							# DomainStart | DomainEnd
							$sLine .= $oAligAnal->get_HSP_startQ . $sSeparator;
							$sLine .= $oAligAnal->get_HSP_endQ . $sSeparator;

							#Scaffold information
							$sLine .=
							  join(
									"",
									@{
										$oAligAnal->ExtractResidues(
											 @{
												 $oAligAnal->get_aScaffoldOffset
											   }
										  )->get_aQuery
									  }
							  ) . $sSeparator;
							$sLine .=
							  join(
									"",
									@{
										$oAligAnal->ExtractResidues(
											 @{
												 $oAligAnal->get_aScaffoldOffset
											   }
										  )->get_aHit
									  }
							  ) . $sSeparator;

							$sLine .= $sScaffCompString . $sSeparator;

				 # Extracted Residues Signature | Corresponding signature of hit
							$sLine .=
							  join( "", @{ $ExtrResidues->get_aQuery } )
							  . $sSeparator;
							$sLine .=
							  join( "", @{ $ExtrResidues->get_aHit } )
							  . $sSeparator;

							# Reference signature to compare with
							$sLine .=
							  join( "", @{ $oComp->get_aValue } ) . $sSeparator;

			# Binary comparison; 1 means identity at this position 0 means other
							$sLine .= join( "", @CompArray ) . $sSeparator;

							# Result Name | description of Result
							$sLine .=
							    $oComp->get_result
							  . $sSeparator
							  . $oComp->get_comment;

							# And print...
							print CSVOUT $sLine, "\n";
						}
					}
				}
			}
		}
	}
}
