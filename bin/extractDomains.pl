#!/usr/bin/perl
#
# This script reads an hmmscan report and extracts the identified domains into a new fasta file
#
#

=head1 NAME

extractDomains.pl

=head1 SYNOPSIS

extractDomains.pl --input <input FASTA file> --hmm <hmmscan report for fasta file> --output <output fasta file>
--prefix <string to prepend to domain numbering> --verbose --help

=head1 DESCRIPTION

Extracts identified domains by hmmscan from query fasta file

=head2 PARAMETERS

=head3
   --input <filename>                  :    Input (multi-)Fasta file
   --hmm <filename>                    :    Input hmmscan report
   --output <filename>                 :    Output filename
   --minHSPLength <number>             :    Mininmal length of HSP to be included in export
   --prefix <string>                   :    string to prepend to domain number
                                            default: dom
   --verbose                           :    Verbose output
   --help                              :    Print this help screen

=head3 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 Auf der Morgenstelle 28
 72076 Tuebingen
 Germany
 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut
use strict;
use Bio::SearchIO;
use Bio::SeqIO;
use Bio::Seq;
use Getopt::Long;

my $sInputSeqFn;
my $sOutputSeqFn;
my $bVerbose;
my $bHelp;
my $sHMMFn;
my $iMinHSPLength;
my $sDomPrefix ="dom";

# Evaluate Cmdline and assign variables
GetOptions(
			'input|i=s'        => \$sInputSeqFn,
			'hmm|j=s'		   => \$sHMMFn,
			'output|o=s'       => \$sOutputSeqFn,
			'minHSPlength|m=s' => \$iMinHSPLength,
			'prefix|p=s'       => \$sDomPrefix,
			'verbose|v'        => \$bVerbose,
			'help|h'           => \$bHelp
);

# Check parameters
if ($bHelp)
{
	print "\n\n";
	eval { require Pod::Text };
	if (@$)
	{
		print
		  	"Error formatting help content! Perl-Module Pod::Text required!\n";
		print
			"To view help, please install Pod::Text module or look directly into\n";
		print "the perl script $0\n\n";
	}
	else
	{
		my $oPodParser = Pod::Text->new();
		$oPodParser->parse_from_file($0);
	}
	exit 0;
}
if ( !-e $sInputSeqFn )
{
	die "Inputfile $sInputSeqFn not found \n\n";
}

if ( !-e $sHMMFn )
{
	die "hmmscan output $sHMMFn not found\n\n";
}


# First we have to load the annotated EMBL-file
my $oSeqIOIn = Bio::SeqIO->new( '-file'   => $sInputSeqFn);

# create output object
my $oSeqIOOut = Bio::SeqIO->new( '-file'   => ">$sOutputSeqFn",
							  '-format' => 'FASTA' );
							  
							  
my $oSearchIO = Bio::SearchIO->new ('-file'   => $sHMMFn,
									'-format' => 'hmmer3');
									
while (my $oSeq=$oSeqIOIn->next_seq) {
	my $oSearchResult=$oSearchIO->next_result;
	
	# Skip to next query sequence, if no result was found (this shouldn't happen, toutgh...)
	
	next if (!defined $oSearchResult);
	
	# Check, whether Query and Hit are identical
	while ($oSeq->id ne $oSearchResult->query_name) {
		warn "Error; Looking for HMMresults for ".$oSeq->id." but found results for".$oSearchResult->query_name."\n!";
		$oSeq=$oSeqIOIn->next_seq() || die "Error: no more sequences available!\n";
	}
	while (my $oHit=$oSearchResult->next_hit) {
		my $i=1;
		if ($bVerbose) {
			print "Found HMM-Hit for ",$oSeq->id," matching ",$oHit->name,"\n";
		}
		while (my $oHSP=$oHit->next_hsp) {
			
			if ($bVerbose) {
				print "Found HSP at ",$oHSP->start("subject"),"..",$oHSP->end("subject")," with length ",$oHSP->end("subject")-$oHSP->start("subject"),"\n";
			}
			
			if (($oHSP->end("subject")-$oHSP->start("subject")) < $iMinHSPLength,) {
				if ($bVerbose) {
					print "...skipping hit as HSP length is below threshold (HSP-length: ";
					print $oHSP->end("subject")-$oHSP->start("subject"), "\n";
				}
				next;
			}
			my $oExportSeq=$oSeq->trunc($oHSP->start("subject"), $oHSP->end("subject"));
			$oExportSeq->id($oSeq->id."_".$sDomPrefix.sprintf ("%02d",$i));
			my $desc=$oSeq->description;
			$oExportSeq->description($oHSP->start("subject")."..".$oHSP->end("subject")." ".$desc);
			$oSeqIOOut->write_seq($oExportSeq);
			$i++
		}
	}
}
