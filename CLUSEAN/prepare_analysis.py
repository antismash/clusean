#!/usr/bin/env python
#
# Prepare files for the later analysis steps
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>

import os
import re
from os import path
from utils import dispatch, filter_files, tmp_file_pattern, CluseanError

def prepare_analysis(step, config):
    """Prepare files for the later analysis steps"""
    skip_blast  = (config['Environment'].has_key('RUN_BLAST') and
                   config['Environment']['RUN_BLAST'] == "false")

    skip_hmmer  = (config['Environment'].has_key('RUN_HMMER') and
                   config['Environment']['RUN_HMMER'] == "false")

    skip_kegg  = (config['Environment'].has_key('RUN_BLAST_KEGG') and
                  config['Environment']['RUN_BLAST_KEGG'] == "false")

    keggdir = config['ECpred']['Dir']
    keggtag = config['ECpred']['Tag']

    for gb_file in filter_files(os.listdir('.'), ".gbk"):
        if (convert_genbank_to_embl(gb_file) > 0):
            return step
    for gb_file in filter_files(os.listdir('.'), ".gb"):
        if (convert_genbank_to_embl(gb_file, ext=".gb") > 0):
            return step
    for gb_file in filter_files(os.listdir('.'), ".genbank"):
        if (convert_genbank_to_embl(gb_file, ext=".genbank") > 0):
            return step

    for embl_file in filter_files(os.listdir('.'), ".embl"):
        if re.search(r'step..\.embl', embl_file) is not None:
            continue

        embl_id = embl_file.replace(".embl", "")
        tmp_file = tmp_file_pattern(embl_id, step)

        substep = 1
        if add_locus_tags(embl_file, tmp_file % (substep)) > 0:
            return step

        if not skip_blast:
            substep += 1
            if blast_embl(tmp_file % (substep-1), tmp_file % (substep)) > 0:
                return step

        if not skip_hmmer:
            substep += 1
            if hmmer_embl(tmp_file % (substep-1), tmp_file % (substep),
                          embl_id=embl_id) > 0:
                return step

        if not skip_kegg:
            substep += 1
            if blast_embl(tmp_file % (substep-1), tmp_file % (substep),
                          keggdir, keggtag) > 0:
                return step

        os.rename(tmp_file % substep, "%s.step%02d.embl" % (embl_id, step))
        for i in range(1, substep):
            os.remove(tmp_file % (i))

    return 0

def prepare_ab_specific_analysis(step, config):
    """Prepare files for antibiotics-specific analysis steps"""

    for embl_file in filter_files(os.listdir('.'), ".step04.embl"):
        if re.search(r'substep..\.step04\.embl', embl_file) is not None:
            continue

        embl_id = embl_file.replace(".step04.embl", "")
        tmp_file = tmp_file_pattern(embl_id, step)

        substep=1
        if hmmer_embl(embl_file, tmp_file % substep, 'ABDomains',
                      'AB_domain_file') > 0:
            return step

        substep += 1
        if hmmer_embl(tmp_file % (substep-1), tmp_file % substep, 'ABmotifs',
                      'AB_motif_file') > 0:
            return step

        substep += 1
        if nrpspred_embl(tmp_file % (substep-1), tmp_file % substep) > 0:
            return step

        os.rename(tmp_file % substep, "%s.step%02d.embl" % (embl_id, step))
        for i in range(1, substep):
            os.remove(tmp_file % (i))

    return 0

def convert_genbank_to_embl(gb_file, ext=".gbk"):
    """Convert a GenBank file to EMBL format"""
    embl_file = gb_file.replace(ext, ".embl")
    args = ['genbank2embl.pl', '--input', gb_file, '--output', embl_file]
    ret = dispatch(args)
    if ret != 0:
        raise CluseanError(
            "Error converting GenBank input file '%s' to EMBL format." % \
            embl_file)
    try:
        size = path.getsize(embl_file)
    except OSError:
        raise CluseanError(
            "Could not convert GenBank input file '%s' to EMBL format." % \
            embl_file)
    if size == 0:
        raise CluseanError(
            "Could not convert GenBank input file '%s' to EMBL format." % \
            embl_file)

    return ret

def add_locus_tags(infile, outfile):
    """Make sure all CDS features in the EMBL file have locus tags"""
    args = ['addLocusTags.pl', '--input', infile, '--output', outfile]
    return dispatch(args)

def blast_embl(infile, outfile, directory=None, tag=None):
    """Prepare FASTA files for BLAST"""
    args = ['BlastEMBL.pl', '--input', infile, '--output', outfile]
    if directory is not None:
        args += ['--blastpdir', directory]
    if tag is not None:
        args += ['--blastptag', tag]

    return dispatch(args)

def hmmer_embl(infile, outfile, directory=None, tag=None, embl_id=None):
    """Prepare FASTA files for HMMer"""
    args = ['HMMerEMBL.pl', '--input', infile, '--output', outfile]
    if directory is not None:
        args += ['--hmmerdir', directory]
    if tag is not None:
        args += ['--tag', tag]
    if embl_id is not None:
        args += ['--singleFasta', "%s.toBlast.fasta" % embl_id]
    return dispatch(args)

def nrpspred_embl(infile, outfile):
    """Prepare FASTA files for NRPSPredictor"""
    args = ['NRPSPredEMBL.pl', '--input', infile, '--output', outfile]
    return dispatch(args)

