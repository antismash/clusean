#!/usr/bin/env python
#
# Do the general annotation
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>
import os
import re
from glob import glob
from utils import dispatch, dispatch_to_file, filter_files, tmp_file_pattern

def annotate(step, config):
    """Annotate the ORFs using the BLAST/HMMer results"""

    skip_blast  = (config['Environment'].has_key('RUN_BLAST') and
                   config['Environment']['RUN_BLAST'] == "false")

    skip_hmmer  = (config['Environment'].has_key('RUN_HMMER') and
                   config['Environment']['RUN_HMMER'] == "false")

    skip_kegg  = (config['Environment'].has_key('RUN_BLAST_KEGG') and
                  config['Environment']['RUN_BLAST_KEGG'] == "false")

    for embl_file in filter_files(os.listdir('.'), ".step02.embl"):
        if re.search(r'substep..\.step02\.embl', embl_file) is not None:
            continue

        embl_id = embl_file.replace(".step02.embl", "")
        tmp_file = tmp_file_pattern(embl_id, step)

        substep = 1
        if assign_function(embl_file, tmp_file % substep, skip_blast,
                           skip_hmmer, skip_kegg) > 0:
            return step

        substep += 1
        if annotate_hmmer_hits(tmp_file % (substep-1), tmp_file % substep) > 0:
            return step

        os.rename(tmp_file % substep, "%s.step%02d.embl" % (embl_id, step))
        for i in range(1, substep):
            os.remove(tmp_file % (i))

    return 0

def annotate_antibiotics(step, config):
    """Annotate the ORFs related to antibiotics production"""
    for embl_file in filter_files(os.listdir('.'), ".step05.embl"):
        if re.search(r'substep..\.step05\.embl', embl_file) is not None:
            continue

        embl_id = embl_file.replace(".step05.embl", "")
        tmp_file = tmp_file_pattern(embl_id, step)
        final_domains = "%s.final_domains.embl" % embl_id

        substep = 1
        if annotate_hmmer_hits(embl_file, tmp_file % (substep),
                               'AB_domain_file') > 0:
            return step

        substep += 1
        if evaluate_domains(tmp_file % (substep-1), final_domains) > 0:
            return step

        # Don't increment substep yet

        if annotate_hmmer_hits(tmp_file % (substep-1), tmp_file % substep,
                               'AB_motif_file') > 0:
            return step

        substep += 1
        if annotate_nrps_pred(tmp_file % (substep-1), tmp_file % substep,
                              "%s.NRPSP.csv" % embl_id) > 0:
            return step

        if get_domain_order(final_domains, "%s.DomOrder.txt" % embl_id) > 0:
            return step

        if get_domain_order(final_domains, "%s.DomOrder.tab" % embl_id,
                            table=True) > 0:
            return step

        os.rename(tmp_file % substep, "%s.step%02d.embl" % (embl_id, step))
        for i in range(1, substep):
            os.remove(tmp_file % (i))

    return 0

def annotate_antismash(step, config):
    """Annotate antiSMASH results"""
    for embl_file in glob("*.step05.embl"):
        if re.search(r'substep..\.step05\.embl', embl_file) is not None:
            continue

        embl_id = embl_file.replace(".step05.embl", "")
        tmp_file = tmp_file_pattern(embl_id, step)
        final_domains = "%s.final_domains.embl" % embl_id

        substep = 1
        if do_antismash_annotation(embl_file, tmp_file % (substep)) > 0:
            return step

        os.rename(tmp_file % substep, "%s.step%02d.embl" % (embl_id, step))
        for i in range(1, substep):
            os.remove(tmp_file % (i))

    return 0

def assign_function(infile, outfile, skip_blast, skip_hmmer, skip_kegg):
    """Assign functions to ORFs based on BLAST results"""
    args = ['assignFunction.pl', '--input', infile, '--output', outfile]
    if skip_blast:
        args.append('--skipblast')
    if skip_hmmer:
        args.append('--skiphmmer')
    if not skip_kegg:
        args.append('--ECpred')
    return dispatch(args)

def annotate_hmmer_hits(infile, outfile, tag=None):
    """Annotate HMMer hits"""
    args = ['annotateHMMHits.pl', '--input', infile, '--output', outfile]
    if tag is not None:
        args += ['--hmmertag', tag]
    return dispatch(args)

def evaluate_domains(infile, outfile):
    """Evaluate which NRPS/PKS domains are present"""
    args = ['evaluateDomains.pl', '--input', infile, '--output', outfile,
            '--annotate']
    return dispatch(args)

def annotate_nrps_pred(infile, outfile, cvsoutfile):
    """Annotate ORFs with NRPSPredictor output"""
    args = ['annotateNRPSPred.pl', '--input', infile, '--output', outfile,
            '--csvout', cvsoutfile]
    return dispatch(args)

def get_domain_order(infile, outfile, table=False):
    """Write the NRPS/PKS domain order output"""
    args = ['getDomainOrder.pl', '--input', infile]
    if table:
        args.append('--printTable')
    return dispatch_to_file(args, outfile)

def do_antismash_annotation(infile, outfile):
    """Add the antiSMASH annotations to the embl file"""
    args = ['annotateAntiSMASH.pl', '--input', infile,
            '--table', 'embl_lines.txt',
            '--output', outfile]
    return dispatch(args)
