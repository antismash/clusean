#!/usr/bin/env python
#
# Run blast processes
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>
import os
import multiprocessing
from utils import dispatch, filter_files

def parallel_blast(directory, database, parallel_jobs=None):
    """Run blast scripts in parallel on all CPUs"""
    pool = multiprocessing.Pool(processes=parallel_jobs)

    blast_files = filter_files(os.listdir(directory), ".fasta")

    dbname = database[database.rfind(os.sep)+1:]


    for f in blast_files:
        f = directory + os.sep + f
        args = ['nice', '-n', '20', 'blastp', '-db', database, '-query', f, '-out',
                f.replace(".fasta", ".blastp"), '-num_threads', '1',
                '-num_descriptions', '250', '-num_alignments', '250']
        pool.apply_async(dispatch, (args, ))

    pool.close()
    pool.join()

    result_files = filter_files(os.listdir(directory), ".blastp")
    for f in result_files:
        f = directory + os.path.sep + f
        dispatch(['gzip', f])

