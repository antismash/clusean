#!/usr/bin/env python
#
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>

"""Parse the clusean Apache-style config file"""

import re

class ConfigParserError(Exception):
    """Report an error while parsing the config file"""
    pass

class ParseConfig(object):
    """Apache-style config file parser"""
    def __init__(self, interpolate=False, defaults=None):
        self.config = defaults or {}
        if not self.config.has_key('Global'):
            self.config['Global'] = {}

        self.sections = []
        for key in self.config.keys():
            self.sections.append(key)

        self.in_section = False
        self.curr_section = 'Global'
        self.interpolate = interpolate
        self.regex = re.compile(r'\$[A-Za-z0-9_]+')

    def expand_vars(self, depth=5):
        """Interpolate and extend variable declarations

        This allows referring to variables defined elsewhere in the config
        file by using $varname. Interpolation only works for variables from
        the global section or the current section.
        To avoid looping endlessly, the interpolation depth is limited to
        a default of 5. The depth can be set to a different value.

        """
        while depth > 0:
            for section in self.sections:
                curr = self.config[section]
                for (key, value) in curr.items():
                    for rep in self.regex.findall(value):
                        lookup = rep.replace("$", "")
                        ins_value = curr.get(lookup) or \
                                self.config['Global'].get(lookup)
                        if ins_value is None:
                            continue
                        new_value = re.sub(r'\%s' % rep, ins_value, value)
                        curr[key] = new_value
            depth -= 1

    def parse(self, lines):
        """Parse Apache-style config returning a dictionary

        The keys of the dictionary are either sections or keys found in the config
        file, the values are dictionaries for sections and plain values for normal
        keys.

        """
        line_no = 0
        for line in lines:
            line_no += 1
            line = line.strip()
            if line == '':
                continue
            elif line.startswith("#"):
                continue
            elif line.startswith("</"):
                if self.in_section and self.curr_section == line[2:-1]:
                    self.in_section = False
                    self.curr_section = 'Global'
                else:
                    raise ParserError("Invalid end of section in line %s" %
                                      line_no)
            elif line.startswith("<"):
                if not self.in_section:
                    self.in_section = True
                    sec_name = line[1:-1]
                    self.curr_section = sec_name
                    if not self.config.has_key(sec_name):
                        self.sections.append(sec_name)
                        self[sec_name] = {}
                else:
                    raise ParserError("Invalid start of section in line %s" %
                                      line_no)
            elif line.find("=") != -1:
                if self.in_section:
                    config = self[self.curr_section]
                else:
                    config = self['Global']

                (key, value) = line.split("=")
                value = value.strip().replace('"', '')
                comment_idx = value.find("#")
                if comment_idx != -1:
                    value = value[:comment_idx].strip()

                key = key.strip()
                if not config.has_key(key):
                    config[key] = value
                else:
                    config[key] += ":%s" % value

            else:
                raise ParserError("Invalid line %s" % line_no)

        if self.interpolate:
            self.expand_vars()

    def parse_file(self, filename):
        """Parse Apache-style config from a filename returning a dictionary
        """
        close_handle = False
        try:
            handle = open(filename, 'r')
            close_handle = True
            lines = handle.readlines()
        except IOError, e:
            raise ConfigParserError("Failed to open/read file %s" % filename)
        finally:
            if close_handle:
                handle.close()
        self.parse(lines)

    def __getitem__(self, item):
        return self.config[item]

    def __setitem__(self, item, value):
        self.config[item] = value
