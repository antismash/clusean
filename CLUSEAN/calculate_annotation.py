#!/usr/bin/env python
#
# Run the general annotation calculation
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>

import os
from blast import parallel_blast as blast
from hmmer import hmmscan, hmmpfam
from nrpspredictor import nrpspredictor

def calculate_annotation(step, config):
    """Calculate the annotation data using BLAST / HMMer"""

    cpus = config['Environment'].get('CPUS', None)
    if cpus is not None:
        cpus = int(cpus)

    skip_blast = (config['Environment'].has_key('RUN_BLAST') and
                  config['Environment']['RUN_BLAST'] == "false")

    skip_hmmer = (config['Environment'].has_key('RUN_HMMER') and
                  config['Environment']['RUN_HMMER'] == "false")

    skip_kegg  = (config['Environment'].has_key('RUN_BLAST_KEGG') and
                  config['Environment']['RUN_BLAST_KEGG'] == "false")

    if not skip_blast:
        blastdb = config['Environment']['BLASTDB'] + os.sep + config['Blast']['DB']
        blastdir = config['Blast']['Dir']
        blast(blastdir, blastdb, cpus)

    if not skip_hmmer:
        hmmerdb = config['Environment']['HMMERDB'] + os.sep + config['HMMer']['DB']
        hmmerdir = config['HMMer']['Dir']
        hmmscan(hmmerdir, hmmerdb, cpus)

    if not skip_kegg:
        keggdb = config['Environment']['BLASTDB'] + os.sep + config['ECpred']['DB']
        keggdir = config['ECpred']['Dir']
        blast(keggdir, keggdb, cpus)

    return 0

def calculate_ab_specific_annotation(step, config):
    """Calculate the antibiotics-specific annotation using HMMer and NRPSPredictor"""
    root = config['Global']['CLUSEANROOT']
    cpus = config['Environment'].get('CPUS', None)
    if cpus is not None:
        cpus = int(cpus)

    abdomdir = 'ABDomains'
    abdomdb = root + os.sep + 'lib' + os.sep + 'AB_domains.hmm2'

    abmotifdir = 'ABmotifs'
    abmotifdb = root + os.sep + 'lib' + os.sep + 'AB_motifs.hmm2'

    nrpsdir= 'NRPSPred'

    hmmpfam(abdomdir, abdomdb, cpus)
    hmmpfam(abmotifdir, abmotifdb, cpus)
    nrpspredictor(nrpsdir)
    return 0
