#!/usr/bin/env python
#
# Clean up after the clusean run
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>
from glob import glob
from shutil import copy2

def cleanup(step, config):
    """Cleaning up and writing result files"""
    steps = range(1,step)
    steps.reverse()
    found_files = False
    for i in steps:
        files = glob("*.step%02d.embl" % i)
        if files != []:
            found_files = True
        for embl_file in files:
            embl_id = embl_file.replace(".step%02d.embl" % i, "")
            copy2(embl_file, "%s.final.embl" % embl_id)
        if found_files:
            break

    return 0
