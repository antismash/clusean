#!/usr/bin/env python
#
# Run HMMer
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>
import os
from utils import dispatch, dispatch_to_file, filter_files, CluseanError

def hmmscan(directory, database, parallel_jobs=None):
    """Run hmmscan on a database"""

    hmmer_files = filter_files(os.listdir(directory), ".fasta")
    if len(hmmer_files) == 0:
        raise CluseanError("hmmscan: No input files!")

    dbname = database[database.rfind(os.sep)+1:]

    i = 0
    for f in hmmer_files:
        f = directory + os.sep + f
        args = ['nice', '-n', '20', 'hmmscan', '-o', f.replace(".fasta", ".hsr")]
        if parallel_jobs is not None:
            args += ['--cpu', '%s' % parallel_jobs]
        args += [database, f]
        dispatch(args)
        i += 1


def hmmpfam(directory, database, parallel_jobs=None):
    """Run hmmpfam on a database"""
    hmmer_files = filter_files(os.listdir(directory), ".fasta")
    if len(hmmer_files) == 0:
        raise CluseanError("hmmpfam: No input files!")

    dbname = database[database.rfind(os.sep)+1:]

    i = 0
    for f in hmmer_files:
        f = directory + os.sep + f
        args = ['nice', '-n', '20', 'hmmpfam2']
        if parallel_jobs is not None:
            args += ['--cpu', '%s' % parallel_jobs]
        args += [database, f]
        dispatch_to_file(args, f.replace(".fasta", ".hsr"))
        i += 1

