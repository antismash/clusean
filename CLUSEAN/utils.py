#!/usr/bin/env python
#
# Pipeline helpers for CLUSEAN
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>

import os
import sys
import subprocess

class CluseanError(Exception):
    pass

def dispatch(args):
    """Call the external command"""
    return subprocess.call(args)

if os.name == "nt":
    def dispatch(args):
        """Call the external command, with windows magic added"""
        # windows doesn't have "nice", so remove it and it's args from
        # the argument list
        if args[0] == "nice":
            args.pop(0)
            args.pop(0)
            args.pop(0)

        if args[0].endswith(".pl"):
            args[0] = "%s\\bin\\%s" % (os.environ["CLUSEANROOT"], args[0])
            args.insert(0, "perl")

        args.extend(["1>", "nul", "2>", "nul"])
        return subprocess.call(args, shell=True)

def dispatch_to_file(args, out_filename):
    """Call the external command writing stdout to a filename"""
    ret = 127
    try:
        fh = open(out_filename, 'w')
        ret = subprocess.call(args, stdout=fh)
    except IOError:
        fh = None
    finally:
        if fh is not None:
            fh.close()
    return ret

def filter_files(file_list, extension):
    """Filter a file list for files with a certain extension"""
    return [entry for entry in file_list if entry.endswith(extension)]

def tmp_file_pattern(embl_id, step):
    return "%s.substep%s.step%02d.embl" % (embl_id, "%02d", step)

def status_to_file(options, msg, status="running"):
    """Write status to status file for web frontend"""
    if options.statusfile is None:
        return

    fh = None
    try:
        fh = open(options.statusfile, "w")
        fh.write("%s: %s\n" % (status, msg))
    except:
        print >>sys.stderr, "Failed to open %s" % options.statusfile
    finally:
        if fh is not None:
            fh.close()

error_page_template = '''<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
 "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>antiSMASH: Error running the pipeline</title>
    <link rel="stylesheet" type="text/css" href="/help.css">
</head>
<body>
    <div class="page">
    <div class="header">
            <a href="/"><img src="/images/antismash.gif" alt="antiSMASH"></a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="/"><img src="/images/antismash2.png" alt="antibiotics &amp; Secondary Metabolite Analysis Shell"></a>
        <span id="menu">
            <a href="/">HOME</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/download.html">DOWNLOAD</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/help.html">HELP</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/about.html">ABOUT</a>
        </span>
    </div>
    <div class="content">

      <h1>Error at step %(step)s (%(description)s)</h1>
      <div class="error">The following error occured while running antiSMASH:<br>
      %(message)s<br>
      </div>
    </div>
    <div class="footer"> <div class="logos"> <img src="/images/ruglogo.gif">
            <img src="/images/gbblogo.gif"> <img src="/images/tueblogo.gif">
            <img src="/images/ucsflogo.gif"> </div>

        <div class="copyright">antiSMASH: Rapid identification, annotation and
            analysis of secondary metabolite biosynthesis gene clusters.
            <br>Marnix H. Medema, Kai Blin, Peter Cimermancic, Victor de Jager,
            Piotr Zakrzewski, Michael A. Fischbach, Tilmann Weber, Rainer
            Breitling &amp; Eriko Takano <br><i>Nucleic Acids Research</i>
            (2011), <a href="http://dx.doi.org/10.1093/nar/gkr466">doi: 10.1093/nar/gkr466</a>.
        </div>
    </div>
</div>
</body>
</html>
'''
