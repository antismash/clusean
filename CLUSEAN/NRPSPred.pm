=head1 NAME

CLUSEAN::NRPSPred - Generic object to store and process NRPSPredictor results

=head1 SYNOPSIS

(recommended variable names)

$NRPSPredRes = CLUSEAN::NRPSPred->new ('-name'       =>  'ABCD-module1')
									'-start'      =>  100,
									'-end'        =>  200
									'-large_clustArray' => [('XX=YY=ZZ-specificity','Z1-specificity')],
									'-lc_scoreArray     => [(1.2,1.24)],
									'-small_clustArray' => [('XX=YY-specificity, Z2-specificity')],
									'-sc_scoreArray'    => [(0.2,0.3)],
									'-StachPred'   => 'Gly',
									'-StachScore'  => 50,
									'-StachIdent'  =>100);
=head1 DESCRIPTION

This module codes for a dumb Object to store NRPSPredictor results
See also NRPSPredIO.pm for handling the result files

=head1 INCLUSIONS

NRPSPred.pm includes following 3rd Party modules:

CLUSEAN::NRPSPredIO

=head1 EXAMPLES

use CLUSEAN::NRPSPredIO;

my $oNRPSIO = CLUSEAN::NRPSPredIO->new('-file'   =>   '/home/weber/test/NRPSPred/AM746336.nrps1.out');

while (my $oNRPS = $oNRPSIO->next_result ) {
	my $ResultName = $oNRPS->get_name;
	my $LargeClusters =join ("##", @{$oNRPS->get_large_clustArray});
	my $SmallClusters = join ("##", @{$oNRPS->get_small_clustArray});
	my ($SmallClustScore) = $oNRPS->get_sc_scoreArray;
	print "$ResultName has a specificity for the follwing small cluster: $SmallClusters with Score $SmallClustScore \n";
}

=cut
package CLUSEAN::NRPSPred;

use strict;
use vars '$AUTOLOAD';
use Carp;

=head1 Methods

=head2

Title:      Autoload

Usage:      No direct execution

Function    This method is executed for every set_XXX or get_XXX method call that has no
            explicit method definition.
            
            You can store almost everything in via these methods but you have to
            care for yourself that you actually retrieve what you have stored...
            
Returns:    get_XXX : Value of XXX
            set_XXX : Defines XXX with value given as parameter            
            
            
=cut

# OK, let's start some perl magic with autoloading...
sub AUTOLOAD {
	
	# For AUTOLOAD to work, we must not be too strict ;-)
	no strict "refs";
	
	my ($self, $newval) = @_;
	
	# Do not handle DESTROY method!!!
	return if $AUTOLOAD =~ /::DESTROY$/;
	
	# If it's a get_XXX  method return value
	if (($AUTOLOAD =~ /.*::get([_-]\w+)/) &&
	     ($self->_accessible ($1)) ) {
			my $attr_name = $1;
			# Define method get_XXX to save time if the same method is called again
			*{$AUTOLOAD} = sub {return $_[0]->{$attr_name} };
			return $self->{$1};
		}
	# if it's a set_XXX  method set value	
	if (($AUTOLOAD =~ /.*::set(_\w+)/)) {
		my $attr_name = $1;
		*{$AUTOLOAD} = sub {$_[0]->{$attr_name}=$_[1]; return};
		$self->{$attr_name} = $newval;
		return
	}
	# If we arrive here, it must have been an erroneous method call so bail out...
	croak "Method $AUTOLOAD is not defined\n ";
}

=head2

Title:             _accessible

Usage:             $self->accessible('xyz')

Function:          Tests whether xyz is defined

Returns:           DEFINED/TRUE if xyz is defined

=cut

# To check whether method exists
sub _accessible {
	my ($self, $var) = @_;
	exists $self->{$var};
	}	


=head2

Title:             new

Usage:             $oNRPS=CLUSEAN::NRPSPred->new ('-value1' =>   1);

Function:          Instantiates a new NRPSPred object and defines variables submitted in parameter hash

				    You can store almost everything in via these methods but you have to
                    care for yourself that you actually retrieve what you have stored...

Returns:           a CLUSEAN::NRPSPred object

=cut

sub new {
	# Set up Method
    my $invocant  = shift;
    my $class     = ref($invocant)  ||  $invocant;
    my $self      = {};
	bless ($self, $class);
	
	# Get parameters
    my %aParams  =  @_;
    
    # Store values given in parameter hash
    # By AUTOLOAD we instantly generate get_XXX Methods for each parameter without writing a single 
    # line of code :-)
    
    foreach my $HashKey (keys %aParams) {
  		my $sObjData = $HashKey;
  		
  		# Lets start the variables with an underscore '_' instead of '-'
  		$sObjData =~ s/^-/_/;
  		
  		# $sMethodName = "set".$sMethodName;
  		# print "$sObjData";
  		$self->{$sObjData}=$aParams{$HashKey};  	
    }
   
    
    return $self;
}

=head2 

Title:     has_Hit

Usage:     if ($oNRPS->hasHit) {doSomethingUseful}

Function:  Checks whether the _NoHit flag is set

Returns:   1 or undef 

=cut

sub has_Hit {
	my $self = shift;
	if (! $self->_accessible('_NoHit')) {
		return 1;
	} 
}
1;	