#!/usr/bin/env python
#
# Run NRPSPredictor
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>
import os
from utils import dispatch_to_file, filter_files

def nrpspredictor(directory):
    """Run NRPSPredictor"""

    nrps_files = filter_files(os.listdir(directory), ".fasta")

    i = 0
    for f in nrps_files:
        f = directory + os.sep + f
        args = ['NRPSPredWrap.pl', '--input', f]
        dispatch_to_file(args, f.replace(".fasta", ".out"))
        i += 1
