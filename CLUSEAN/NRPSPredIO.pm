=head1 NAME

CLUSEAN::NRPSPredIO - Generic object to process NRPSPredictor output files

=head1 SYNOPSIS

$NRPSPredRes = CLUSEAN::NRPSPredIO->new ('-file'  =>  'test.out);
$oNRPS=CLUSEAN::NRPSPredIO->next_result;

=head1 DESCRIPTION

This module processes NRPSPredictor output files in a streamlike manner
that is comparable to the Bio::Seq / Bio::SearchIO fashion.

NRPSPredIO reads the complete outputfile in a buffer. Each call of 
"next_result" processes one domain and returns a NRPSPred compliant object



=head1 INCLUSIONS

NRPSPred.pm includes following 3rd Party modules:

CLUSEAN::NRPSPredIO

=head1 EXAMPLES

use CLUSEAN::NRPSPredIO;

my $oNRPSIO = CLUSEAN::NRPSPredIO->new('-file'   =>   '/home/weber/test/NRPSPred/AM746336.nrps1.out');

while (my $oNRPS = $oNRPSIO->next_result ) {
	my $ResultName = $oNRPS->get_name;
	my $LargeClusters =join ("##", @{$oNRPS->get_large_clustArray});
	my $SmallClusters = join ("##", @{$oNRPS->get_small_clustArray});
	my ($SmallClustScore) = $oNRPS->get_sc_scoreArray;
	print "$ResultName has a specificity for the follwing small cluster: $SmallClusters with Score $SmallClustScore \n";
}

=cut


package CLUSEAN::NRPSPredIO;

use strict;
#use Symbol;
use CLUSEAN::NRPSPred;



=head2

Title           :          new

Usage           :          my $oNRPSIO=CLUSEAN::NRPSPredIO->new('-file'    =>   'myNRPSPredOut.out');

Function        :          Sets up an CLUSEAN::NRPSPredIO object

                           Input parameters:
                           
                           -file    :   Filename of NRPSPredictor (text) output
                           
Returns         :          An NRPSIO-object  
                     
=cut

sub new {

# Set up Method
    my $invocant  = shift;
    my $class     = ref($invocant)  ||  $invocant;
    my %aParams  =  @_;
    my $self      = {};
    bless ($self, $class);
    
    # Open NRPSpredictor output file and load content into @aResults
    open (NRPSPred, $aParams{'-file'} ) || die "Error: Can't open $aParams{'-file'}";
    
    my @aResults = <NRPSPred>;

    # Store Arrayref to @aResults into object
        
    $self->{_raResults} = \@aResults;
    
    # ... and close filehandle
    close (NRPSPred);
    
    return $self;    
}

=head2

Title           :          _readline

Usage           :          only used internally

Function        :          Retrieve next line from @_raResults buffer

					       Note: The line is retrieved by shifting of the array,
					       so no rewinding is possible with this implementation!

Returns         :          A "chomped" string                      

=cut

sub _readline {
	my $self = shift;
	my %aParams  = @_;
	my $line = shift (@{$self->{_raResults}}) or return;
	
	chomp ($line);
	return ($line);
}

=head2

Title           :          next_result

Usage           :          my $oNRPS=$ONRPSIO->next_result

Function        :          Parses the report of a single domain analyzed by
                           NRPSPredictor.  

Returns         :          an CLUSEAN::NRPSPred object (or undef, if all reports have already been processed)                      

=cut

sub next_result {
	my $self = shift;
	my %aParams  = @_;
	my $readln = $self->_readline || return;
	
	my $oNRPS=CLUSEAN::NRPSPred->new;
	
	# If there was not hit against A-domain HMM profile set NoHit Flag and return empty object
	if ($readln =~ /^\[no hit.*/) {
		$oNRPS->set_NoHit;
		while (($readln) && ($readln ne "//")) {
			$readln=$self->_readline;
		}
		return $oNRPS;
	}
	# First line must start with Module: and contains name and coordinates
	if ($readln !~ /Modul:\s(.*)\sspanning aa\s(\d+)\.\.(\d+)/) {
		die "Error: NRPSPredictor entry doesn't start with Module:,\n found \"$readln\" instead\n"
	}
	my $sEntryName=$1;
	$oNRPS->set_start($2);
	$oNRPS->set_end($3);
	$oNRPS->set_name($sEntryName);
	
	# next line contains 8A aa
	$readln = $self->_readline || die "broken entry $sEntryName\n";
	(undef, my $EightACode) = split (/\:\s/, $readln);
	$oNRPS->set_eightAA($EightACode);
	# next line constains 10 Stachelhaus aa
	$readln = $self->_readline || die "broken entry $sEntryName\n";
	(undef, my $StachAA) = split (/\:\s/, $readln);
	$oNRPS->set_StachAA($StachAA);
	# now we have to check wheter there are results and, if yes, skip the large-clusters description line
	$readln = $self->_readline || die "broken entry $sEntryName\n";
	# Skip evaluation of NRPSPredictor SVM result parsing if there are none...
	if ($readln ne "[no predictions]") {
		# next line contains large cluster spec and score
		$readln = $self->_readline || die "broken entry $sEntryName\n";
		# As there may be more than one significant hit we have to check each of the next lines
		# until Small-Cluster Header arrives
		my @LCArray;
		my @LCScoreArray;
		
		if ($readln =~ /^\[no predictions.*/) {
			# we have no prediction so skip to next line
			push (@LCArray, 'No predictions for large clusters available'); 
			$readln = $self->_readline || die "broken entry $sEntryName\n";
		}
		else {
			until ($readln =~ /.*small clusters\"\)\:$/) {
				my ($sLargeClust, $flc_score) = $readln =~ /(^.*-like) specificity Score\:(.*)/;
				push (@LCArray, $sLargeClust);
				push (@LCScoreArray, $flc_score);
				$readln = $self->_readline || die "broken entry $sEntryName\n";
			}
		}
		# Store arrays:
		$oNRPS->set_large_clustArray(\@LCArray);
		$oNRPS->set_lc_scoreArray(\@LCScoreArray);
		
		# now $readln should contain the small-clusters description line
		
		# next line contains small cluster spec and score
		$readln = $self->_readline || die "broken entry $sEntryName\n";
		
		my @SCArray;
		my @SCScoreArray;
		if ($readln =~ /^\[no predictions.*/) {
			# we have no prediction so note this and skip to next line
			push (@SCArray, 'No predictions for small clusters available'); 
			$readln = $self->_readline || die "broken entry $sEntryName\n";
		}
		else {
			until ($readln =~ /^Alis.*/) {
				my ($sSmallClust, $fsc_score) = $readln =~ /(^.*-like) specificity Score\:(.*)/;
				push (@SCArray, $sSmallClust);
				push (@SCScoreArray, $fsc_score);
				$readln = $self->_readline || die "broken entry $sEntryName\n";
			}
		}
		$oNRPS->set_small_clustArray(\@SCArray);
		$oNRPS->set_sc_scoreArray(\@SCScoreArray);
	}
	# $readln now should contain the Stachelhaus heading
	
	# next line contains Stachelhaus code results
	$readln = $self->_readline || die "broken entry $sEntryName: Can't find Stachelhaus results\n";
	if ($readln eq "[no matches]") {
		$oNRPS->set_StachPred("No matches against A-domain signatures!")	
	}
	else {
		my ($StScore, $StIdent, $StMatch) = $readln =~ /^Score=(\d+), identity=(\d+) with subject (.*)$/;
		$oNRPS->set_Stach_score($StScore);
		$oNRPS->set_Stach_ident($StIdent);
		$oNRPS->set_StachPred($StMatch);
	}	
	# remove entry terminator from buffer//
	$readln = $self->_readline || die "broken entry $sEntryName: can't find terminator string //\n";
	
	return $oNRPS;
}
1;