#
# 	$Id: AssignFunction.pm,v 1.5 2008/02/07 16:19:06 weber Exp $
#
#  CLUSEAN Module to assign putative function by analyzing
#  Blast and HMMer reports
#
#

=head1 NAME

CLUSEAN::AssignFunction

=head1 SYNOPSIS

$oAssignment=CLUSEAN::AssignFunction->new ('-reportfile'  => "./blastprep.bls");
@AoAAssignments = $oAssignment->doAssignment;

=head1 DESCRIPTION

This module postulated gene functions by analyzing blastp or HMMPfam reports
of the CDS protein sequence.
Each Hit is compared to an keyword matrix and evaluated (at the moment
only by the Keywords and the BLASTP/HMMPfam-E-Value)

The Module Returns an AoA containing an sorted assignment table which
can be accessed like

    foreach $Ass ($oAssignment->doAssignment) {
	print "Assignment: $Ass->[0], Score: $Ass[1]\n"
	}

=head1 INCLUSIONS

AssignFunction needs the following perl modules

Bio::SearchIO (Blast-parsing)
Sort::ArrayOfArrays (sorting)
Config::General (Config file parsing)
CLUSEAN::HMMParse (HMMPfam parsing)

=head1 EXAMPLES

use CLUSEAN::AssignFunction;
use strict;


my $sCLUSEANROOT = $ENV{CLUSEANROOT} || "/usr/local/CLUSEAN";

my $oAssig=CLUSEAN::AssignFunction->new ('-reportfile' => "$CLUSEANROOT/t/C20.bls");

my @AoA=$oAssig->doAssignment;

my @line;

foreach my $line (@AoA) {

    print "Assignment: $line->[0]\tScore: $line->[1]\n";

}

=head1 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 University of Tuebingen
 72076 Tuebingen
 Germany

 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

#*****************************************************************
#*****************************************************************
#CODE BEGIN
#**************************************************
package CLUSEAN::AssignFunction;
use strict;

#use warnings;
use Bio::SearchIO;
use Sort::ArrayOfArrays;
use Config::General;
use FindBin qw ($Bin);
use CLUSEAN::HMMParse;

=head1 Methods

=cut

# methods
sub new;                        # Constructor method
sub _BLAST_init;                # Initialization of BlastP parsing
sub _BLAST_sigFactor;           # Calculates a significance factor
sub _BLAST_analyzeRep;          # Does the BLAST analysis
sub _BLAST_searchAssignment;    # Compares Blast Desc with Table
sub _HMM_init;                  # Initialization of HMMPfam parsing
sub _HMM_getReqFuncHash;        # Returns RecFunc Hash
sub _HMM_sigFactor;             # Calculates a significance factor
sub _Rep_min_eval;              # returns minimal E-Val of Blast/HMMPfam-Report
sub _Rep_max_eval;              # dito with maximal E-Val
sub _Rep_min_score;             # dito with minimal score
sub _Rep_max_score;             # dito with maximal score
sub _doAss_BlastMatch;          # Assignment by analyzing BLAST reports
sub _doHMMerMatch;              # Assignment by analyzing HMMer reports
sub _get_merged_assignments;    # Returns Assignment-Hash from new parameter
sub _CutOff;                    # returns Blast/HMMPfam-Cutoff;
sub doAssignment;               # Wrapper: Calls the actual analysis subroutines
sub getHitArray;                # Returns an AoA of BlastHits
sub getAssignmentArray;         # Returns AssignmentArray
sub getAssignmentName;          # Returns Assignment Name
sub getAssignmentScore;         # Returns Assignment Hit
                                # from AssignmentArray
sub getNoOfAssignments;         # Returns the number of Assignments
                                # or better the higest index of AssignmentArray
sub getAnalysisMethod;          # Returns selected Analysis method

#*****************************************************************
#Method: new
#**************************************************

=head2 new

Title        :      new

Usage        :      my $oGenAss= $CLUSEAN::AssignFunction->new (
						    '-table'     => "recogTable.tab",
						    '-reportfile' => "myDir/report.bls")

Function     :      Sets up a Assignment object.

                    Input parameters:
                        -table :      Recognition table (default $CLUSEANTABLES/recogTable.tab)
                        -reportfile:  BlastP-Report-File
                        -blastRepObj: Bio::Search::Result object with Blast data
                        -HMMObj:      CLUSEAN::HMMParse object with HMMPfam data
                        -method:      Analysis method: Currently implemented:
                                      BlastTab_all (the complete recognition table is
                                                    evaluated)
                                      BlastTab_first (only first hit in recognition
                                                      table is evaluated)
                                      HMMTab
                        -CutOff:      Blast Cut Off E-Value
                        -mergeAss:    Merges previous results to current run
                                      Parameter: Reference to assignment-result table
                                      from previous run
                        -configfile:  Path to the CLUSEAN.cfg file


                        Either -reportfile or -blastRepObj/-HMMObj are required parameters

Returns      :      A CLUSEAN::AssignFunction object

=cut

sub new {

    # Variables
    my @AoAKeywordTable;    # AoA of @aAssignmentLine

    # Set up Method
    my $self = shift;
    my $class = ref($self) || $self;

    # Bless the object
    $self = {};
    bless( $self, $class );

    # Get parameters
    my %hParams = (
        "-method"      => "BlastTab_all",
        "-reportfile"  => undef,
        "-blastRepObj" => undef,
        "-HMMObj"      => undef,
        "-mergeAss"    => undef,
        "-table"       => undef,
        "-CutOff"      => 1e-2,
        "-configfile"  => undef,
        @_
    );

    # load the configfile
    my $configfile;
    if ( defined $hParams{"-configfile"} ) {
        $configfile = $hParams{"-configfile"};
    } elsif ( defined $ENV{CLUSEANROOT} ) {
        $configfile = $ENV{CLUSEANROOT} . "/etc/CLUSEAN.cfg";
    } else {
        $configfile = $Bin;
        $configfile =~ s/(.*)\/.*?$/$1/;
        $configfile .= "/etc/CLUSEAN.cfg"
    }

    # Parse Config file
    my $oConfig =
      Config::General->new( '-ConfigFile' => $configfile );
    my %hConfig = $oConfig->getall;

    # Default configuration data
    # %RecogTables contains a hash with information about the default recognition
    # tables that are used with the different algorithms
    my %_RecogTables = %{ $hConfig{'RecogTables'} };

    # Check parameters
    # Assign recognition table
    if ( !defined $hParams{"-table"} ) {
        $hParams{"-table"} =
          $hConfig{'CLUSEANROOT'} . "/" . $_RecogTables{ $hParams{"-method"} };
    }

    # Assign analysis method
    $self->{AnalysisMethod} = $hParams{"-method"};

    # Assign CutOff
    $self->{CutOff} = $hParams{"-CutOff"};

    # Compatibility issue: If Parameter -blastfile is given assign
    # -reportfile with same content
    if ( defined $hParams{"-blastfile"} ) {
        $hParams{"-reportfile"} = $hParams{"-blastfile"};
    }

    # Method specific initialization
    if ( $self->getAnalysisMethod =~ /BlastTab/ ) {
        $self->_BLAST_init(%hParams);
    }
    elsif ( $self->getAnalysisMethod =~ /HMMTab/ ) {
        $self->_HMM_init(%hParams);
    }
    elsif ( $self->getAnalysisMethod =~ /ECpred/ ) {
        $self->_BLAST_init(%hParams);
    }
    else {
        die "unknown analysis method: $self->getAnalysisMethod";
    }

    # Prepare Assignment merging
    $self->{mergeAss} = $hParams{"-mergeAss"};

    # Return Object
    return $self;
}

#*****************************************************************
#Method: _BLAST_init
#**************************************************

=head2 _Blast_init

Title             :         _BLAST_init

Usage             :         internal function

                            $self->_BLAST_init(%hParams)

Function          :         Initializes Blast-Reports

=cut

sub _BLAST_init {
    my @AoAHitA;            # AoA containig Every BlastHit
                            # (i)(Desc, Eval, Score)
    my @AoAKeywordTable;    # AoA of @aAssignmentLine

    # set up method
    my $self    = shift;
    my %hParams = @_;

    # Assign HitArray
    @AoAHitA = $self->_BLAST_analyzeRep(
        "-reportfile"  => $hParams{"-reportfile"},
        "-blastRepObj" => $hParams{"-blastRepObj"}
    );
    $self->{HitA} = [@AoAHitA];

    # Assign Min/Max Blast-Evals and Scores
    if ( $#AoAHitA >= 0 ) {
        $self->{Rep_max_eval}  = $AoAHitA[$#AoAHitA][1];
        $self->{Rep_min_eval}  = $AoAHitA[0][1];
        $self->{Rep_min_score} = $AoAHitA[$#AoAHitA][2];
        $self->{Rep_max_score} = $AoAHitA[0][2];
    }

    # Load Assignment Table
    open( ASSTAB, $hParams{"-table"} )
      or die "Failed to load assignment table\n";

    # Read Assignments to @AoAKeywordTable
    # @AoAKeywordTable[LineNo]=(Assignment, RegEx, AssScore)
    while ( my $readln = <ASSTAB> ) {
        my @aAssignmentLine = split( /\t/, $readln );

        # skip comments and blank lines
        next if ( $readln =~ /^[\#\n ]/ );

        # To save performace the regexes should be precompiled
        # (case insensitive)
        $aAssignmentLine[1] = qr/$aAssignmentLine[1]/i;

        # Add Assignment to Array
        push @AoAKeywordTable, [@aAssignmentLine];
    }
    close ASSTAB;
    push @AoAKeywordTable, [ "unclassified", ".*", 0.1 ];

    # Add AoA to object
    $self->{KeywordTable} = [@AoAKeywordTable];
}

#*****************************************************************
#Method: _Rep_min_eval
#**************************************************

=head2 _Rep_min_eval

Title    : _Rep_min_eval

internal : Returns the lowest Blast/HMMPfam-E-Value in @AoAHitA

=cut

sub _Rep_min_eval {
    my $self = shift;
    return $self->{Rep_min_eval};
}

#*****************************************************************
#Method: _Rep_max_eval
#**************************************************

=head2 _Rep_max_eval

Title    : _BLAST_max_eval

internal : Returns the highest Blast/HMMPfam-E-Value in @AoAHitA

=cut

sub _Rep_max_eval {
    my $self = shift;
    return $self->{Rep_max_eval};
}

#*****************************************************************
#Method: _Rep_min_score
#**************************************************

=head2 _Rep_min_score

Title    : _Rep_min_score

internal : Returns the lowest Blast/HMMPfam-E-Value in @AoAHitA

=cut

sub _Rep_min_score {
    my $self = shift;
    return $self->{Rep_min_score};
}

#*****************************************************************
#Method: _Rep_max_score
#**************************************************

=head2 _Rep_max_score

Title    : _Rep_max_score

internal : Returns the highest Blast/HMMPfam-Score in @AoAHitA

=cut

sub _Rep_max_score {
    my $self = shift;
    return $self->{Rep_max_score};
}

#*****************************************************************
#Method: _CutOff
#**************************************************

=head2 _CutOff

Title    : _CutOff

internal : Returns the Blast/HMMPfam-CutOff value provided by method new

=cut

sub _CutOff {
    my $self = shift;
    return $self->{CutOff};
}

#*****************************************************************
#Method: _BLAST_sigFactor
#**************************************************

=head2 _BLAST_sigFactor

Title    : _BLAST_sigFactor

internal : Returns a significance factor

=cut

sub _BLAST_sigFactor {
    my ( $self, $fInputScore ) = @_;
    my $fSigScore;
    my $fEvalMax  = $self->_Rep_max_eval;
    my $fEvalMin  = $self->_Rep_min_eval;
    my $fEvalDiff = $fEvalMax - $fEvalMin;

    # Hits with Evals<25% of EvaMax get 1 credit
    if ( $fInputScore <= $fEvalMax - ( $fEvalDiff / 4 ) * 3 ) {
        $fSigScore = 1;
    }

    # Hits with Evals <50% of EvalMax get 0.5 credits...
    elsif ( $fInputScore <= $fEvalMax - ( $fEvalDiff / 4 ) * 2 ) {
        $fSigScore = 0.75;
    }

    # dito with Evals <75% of EvalMax get 0.25 credits
    elsif ( $fInputScore <= $fEvalMax - ( $fEvalDiff / 4 ) ) {
        $fSigScore = 0.5;
    }

    # and hits with Evals = EvalMin get 0.1 credits
    else {
        $fSigScore = 0.25;
    }
    return $fSigScore;
}

#*****************************************************************
#Method: _BLAST_analyzeRep
#**************************************************

=head2 _BLAST_analyzeRep

Title    : _Blast_analyzeRep

internal : Analyzes the BlastP-File or the Blast results object and sets up @AoAHitA

=cut

sub _BLAST_analyzeRep {
    my $self    = shift;
    my %hParams = (
        "-reportfile"  => undef,
        "-blastRepObj" => undef,
        @_
    );

    # Variables
    my @aRepLine;        # (BlastDescription, Evalue, Score)
    my @AoABlastRep;     # AoA of @aRepLine
    my $oBlastRepIO;     # Bio::SearchIO - Object
    my $oBlastResult;    # Bio::Search::Result;
    my $fCutOff = $self->_CutOff;

    # Assign Bio::SearchIO object
    if ( defined $hParams{"-reportfile"} ) {
        $oBlastRepIO = Bio::SearchIO->new(
            "-format" => "blast",
            "-file"   => $hParams{"-reportfile"}
        );
        $oBlastResult = $oBlastRepIO->next_result();
    }
    elsif ( defined $hParams{"-blastRepObj"} ) {
        $oBlastResult = $hParams{"-blastRepObj"};
    }
    else {
        die
"AssignFunction error: either -reportfile or -blastRepObj is required";
    }

    # and load BlastHits to @AoABlastRep
    while ( my $oBlastHit = $oBlastResult->next_hit ) {
        my @aRepLine = (
            $oBlastHit->description, $oBlastHit->significance, $oBlastHit->bits
        );
        last if ( $aRepLine[1] > $fCutOff );
        push @AoABlastRep, [@aRepLine];
    }
    return @AoABlastRep;
}

#*****************************************************************
#Method: _doBlastMatch
#**************************************************

=head2 _doBlastMatch

Title    :  _doBlastMatch

internal : compares the BlastP-Report against recognition table
           depending on the method specified via "-method", the complete
           table is searched (BlastTab_all) or searchin is stopped when
           the first hit is found (BlastTab_first)

=cut

sub _doBlastMatch {
    my $self = shift @_;

    # Variables
    my %hAssignmentHash = $self->_get_merged_assignments;
    my $fScore          = 0;
    my @aResultLine;
    my @aUnassigned;
    my @AoAResult;
    my $sAssignment;
    my $fAssScore;
    my $sMethod = $self->getAnalysisMethod;
    my $bHitFound;    # Flag indicating that at least one hit was observed
    my $bClassified;

    # Let's go through every line of the BlastReport...
    foreach my $aRepLine ( @{ $self->getHitArray } ) {

        # Reset HitFlag
        $bHitFound   = undef;
        $bClassified = undef;

        # ... and compare it with the KeywordTable
        foreach my $aSubjLine ( @{ $self->{KeywordTable} } ) {

            # If Report Line Matches add Hit to %hAssignmentHash
            if ( $$aRepLine[0] =~ /$$aSubjLine[1]/ ) {
                $sAssignment = $$aSubjLine[0];

        # calculate score:
        # AssignmetScore for current BlastHit is
        # Score = AssScore (from Assig-table) * significance factor (from E-Val)
        #         * BlastScore/(Maximal Blast Score)
                $fAssScore =
                  $$aSubjLine[2] * $self->_BLAST_sigFactor( $$aRepLine[1] );

        # Handle division by 0; Should not occurr often, but especially if small
        # databases are used we cannot rule out that there is no score at all...
                if ( $self->_Rep_max_score == 0 ) {
                    $fAssScore = 0;
                }
                else {
                    $fAssScore *= $$aRepLine[2] / $self->_Rep_max_score;
                }
                $fScore = $hAssignmentHash{$sAssignment}?$hAssignmentHash{$sAssignment}:0 + $fAssScore;

                # As the regex for unclassified is .*, we have to bail out here,
                # if a previous hit was found
                if ( ($bHitFound) and ( $sAssignment eq "unclassified" ) ) {
                    $bClassified = "TRUE";
                    next;
                }

                # Add Assignment
                $hAssignmentHash{$sAssignment} = $fScore;
                $bHitFound = "TRUE";
                last if ( $sMethod eq "BlastTab_first" );
            }
        }
        if ( !defined $bClassified ) {
            push @aUnassigned, $$aRepLine[0];
        }
    }
    if ( $#{ $self->getHitArray } eq -1 ) {
        $hAssignmentHash{'unassigned'} = 99999.999;
    }

    # Now we need the data in an AoA
    foreach my $Assignment ( keys %hAssignmentHash ) {
        push @AoAResult, [ ( $Assignment, $hAssignmentHash{$Assignment} ) ];
    }

    # As Sort::ArrayOfArrays requires minimal two assignments we
    # have to check this before execution
    if ( $#AoAResult > 0 ) {

        # Lets set up our two dimensional sorter
        my $oSorter = Sort::ArrayOfArrays->new(
            {
                results     => \@AoAResult,
                sort_column => "-1",
                sort_code => {'-1' => 'na'}
            }
        );
        my @aSortedAoA = @{ $oSorter->sort_it };
        $self->{AssignmentArray} = [@aSortedAoA];
    }

    # Otherwise return unsorted 1 element result
    else {
        $self->{AssignmentArray} = [@AoAResult];
    }
    $self->{Unassigned} = [@aUnassigned];
    return @{ $self->{AssignmentArray} };
}

sub _doECpred {
    my $self = shift @_;

    # Variables
    my %hAssignmentHash;
    my $fScore = 0;
    my @aResultLine;
    my @aUnassigned;
    my @AoAResult;
    my $sAssignment;
    my $fAssScore;
    my $sMethod = $self->getAnalysisMethod;
    my $bHitFound;    # Flag indicating that at least one hit was observed
    my $bClassified;

    # Let's go through every line of the BlastReport...
    foreach my $aRepLine ( @{ $self->getHitArray } ) {

        # Reset HitFlag
        $bHitFound   = undef;
        $bClassified = undef;

        if ( $$aRepLine[0] =~ /.*(\d+\.\d+\.[\d-]+\.[\d-]*).*/ ) {
            my $sEC = $1;
            if ( $$aRepLine[1] == 0 ) {
                $$aRepLine[1] = 1000;
            }
            my $fECscore = $$aRepLine[2] * log( $$aRepLine[1] ) * -1;
            if ( defined $hAssignmentHash{$sEC} ) {
                $fECscore += $hAssignmentHash{$sEC};
            }
            $hAssignmentHash{$sEC} = $fECscore;

        }
    }
    if ( scalar keys(%hAssignmentHash) == 0 ) {
        $hAssignmentHash{'unassigned'} = 0;
    }

    # Now we need the data in an AoA
    foreach my $EC ( keys %hAssignmentHash ) {
        push @AoAResult, [ ( $EC, $hAssignmentHash{$EC} ) ];
    }

    # As Sort::ArrayOfArrays requires minimal two assignments we
    # have to check this before execution
    if ( $#AoAResult > 0 ) {

        # Lets set up our two dimensional sorter
        my $oSorter = Sort::ArrayOfArrays->new(
            {
                results     => \@AoAResult,
                sort_column => "-1",
                sort_code => {'-1' => 'na'}
            }
        );
        my @aSortedAoA = @{ $oSorter->sort_it };
        $self->{AssignmentArray} = [@aSortedAoA];
    }

    # Otherwise return unsorted 1 element result
    else {
        $self->{AssignmentArray} = [@AoAResult];
    }

    $self->{Unassigned} = [@aUnassigned];
    return @{ $self->{AssignmentArray} };
}

#*****************************************************************
#Method: _HMM_init
#**************************************************

=head2 _HMM_init

Title             :      _HMM_init

Usage             :         internal function

                              $self->_HMM_init(%hParams)
Function          :         Initializes HMMPfam-Reports

=cut

sub _HMM_init {

    # Variables
    my @AoAHitA;
    my @TempAoAHitA;
    my @aAssignmentLine;
    my @AoAKeywordTable;    # AoA of @aAssignmentLine
    my @aReqFuncLine;
    my @aReqFuncs;
    my @AoAReqFuncTable;
    my @aHMMRes;
    my $oHMMRes;

    # Set up Object
    my $self    = shift;
    my %hParams = @_;

    # Assign HitArray
    if ( defined $hParams{"-reportfile"} ) {
        $oHMMRes = CLUSEAN::HMMParse->new( "-file" => $hParams{"-reportfile"} );
    }
    elsif ( defined $hParams{"-HMMObj"} ) {
        $oHMMRes = $hParams{"-HMMObj"};
    }
    else {
        die "AssignFunction error: either -reportfile or -HMMObj is required";
    }
    @aHMMRes = $oHMMRes->getResultSet( "-evalue" => $hParams{"-CutOff"} );
    foreach my $hHMMHit (@aHMMRes) {
        push @TempAoAHitA,
          [
            (
                $hHMMHit->{"HMMName"}, $hHMMHit->{"Evalue"}, $hHMMHit->{"Score"}
            )
          ];
    }

   # As HMMRes returns Hits not sorted by Evalues, we have to do that seperately
    if ( $#TempAoAHitA > 0 ) {
        my $oSorter = Sort::ArrayOfArrays->new(
            {
                results     => \@TempAoAHitA,
                sort_column => "1",
                sort_code => {'1' => 'na'}
            }
        );
        @AoAHitA = @{ $oSorter->sort_it };
    }
    else {
        @AoAHitA = @TempAoAHitA;
    }
    $self->{HitA} = [@AoAHitA];

    # Assign Min/Max HMM-Evals and Scores
    if ( $#AoAHitA >= 0 ) {
        $self->{Rep_max_eval}  = $AoAHitA[$#AoAHitA][1];
        $self->{Rep_min_eval}  = $AoAHitA[0][1];
        $self->{Rep_min_score} = $AoAHitA[$#AoAHitA][2];
        $self->{Rep_max_score} = $AoAHitA[0][2];
    }

    # Load Assignment Table
    open( ASSTAB, $hParams{"-table"} ) or die "Can't load assignment table\n";

    # Read Assignments to @AoAKeywordTable
    # @AoAKeywordTable[LineNo]=(RegEx, AssScore)
    while ( my $readln = <ASSTAB> ) {

        # skip comments and blank lines
        next if ( $readln =~ /^[\#\n ]/ );
        @aAssignmentLine = split( /\t/, $readln );

        # To save performace the regexes should be precompiled
        # (case insensitive)
        @aAssignmentLine =
          ( $aAssignmentLine[0], qr/$aAssignmentLine[0]/i,
            $aAssignmentLine[1] );

        # Add Assignment to Array
        push @AoAKeywordTable, [@aAssignmentLine];
    }
    close ASSTAB;

    # Add AoA to object
    $self->{KeywordTable} = [@AoAKeywordTable];
    my $srecFuncTable = $hParams{"-table"};
    $srecFuncTable =~ s/Keywords/reqFunc/;
    open( RECFUNC, $srecFuncTable );
    while ( my $readln = <RECFUNC> ) {

        # skip comments and blank lines
        next if ( $readln =~ /^[\#\n ]/ );
        chomp($readln);
        @aReqFuncLine = split( /\t/, $readln );
        @aReqFuncs    = split( ",",  $aReqFuncLine[1] );
        push( @AoAReqFuncTable, [ ( $aReqFuncLine[0], [@aReqFuncs] ) ] );
    }
    $self->{ReqFuncTable} = [@AoAReqFuncTable];
}

#*****************************************************************
#Method: _HMM_getReqFuncHash
#**************************************************

=head2 _HMM_getReqFuncHash

Title    : _HMM_getReqFuncHash

internal : Returns a hash containing Assignment as Hash reference
           and required Pfam-Motifs as ArrayRef

=cut

sub _HMM_getReqFuncHash {
    my $self = shift;
    my %hReqFuncs;
    my @aReqFuncs;
    foreach my $aReqFuncs ( @{ $self->{ReqFuncTable} } ) {
        $hReqFuncs{ $$aReqFuncs[0] } = $$aReqFuncs[1];
    }
    return %hReqFuncs;
}

#*****************************************************************
#Method: _HMM_sigFactor
#**************************************************

=head2 _HMM_sigFactor

Title    : _HMM_sigFactor

internal : Returns a significance factor

=cut

sub _HMM_sigFactor {
    my ( $self, $fInputScore ) = @_;

    # I don't have an idea how to do that, so this is only a workaround
    my $fOutputScore = 1 - ( $fInputScore * 100 );

    # to give HMMer a litte more weight, multiply by 20
    $fOutputScore *= 20;
    return $fOutputScore;
}

#*****************************************************************
#Method: _doHMMerMatch
#**************************************************

=head2 _doHMMerMatch;

Title    :  _doHMMerMatch

Usage    :  @AoA=$oAssign->_doHMMerMatch;

Function :  Does HMMPfam-report analysis
            In a first step all occurring domains are collected in
            %hPfamHits. In a second step relations are determined.

=cut

sub _doHMMerMatch {
    my $self = shift @_;

    # Variables
    # Define %hAssignmentHash and merge previous results (if present)
    my %hAssignmentHash = $self->_get_merged_assignments;
    my @aUnassigned;
    my %hPfamHits;
    my %hReqFuncs;
    my $bHitFound;
    my $fPfamHitScore;
    my $fPfamHitScoreSum;
    my @AoAResult;

    foreach my $aRepLine ( @{ $self->getHitArray } ) {
        $bHitFound = undef;

        # Identify HMMpfam hits
        foreach my $aSubjLine ( @{ $self->{KeywordTable} } ) {
            if ( $$aRepLine[0] =~ /$$aSubjLine[1]/ ) {
                $fPfamHitScore =
                  $$aSubjLine[2] * $self->_HMM_sigFactor( $$aRepLine[1] );
                $fPfamHitScore *= $$aRepLine[2] / $self->_Rep_max_score;
                $fPfamHitScoreSum =
                  $hPfamHits{ $$aSubjLine[0] }?$hPfamHits{ $$aSubjLine[0] }:0 + $fPfamHitScore;
                $hPfamHits{ $$aSubjLine[0] } = $fPfamHitScoreSum;
                $bHitFound = "TRUE";
            }
        }
        if ( !$bHitFound ) {
            push @aUnassigned, $$aRepLine[0];
        }
    }
    $self->{Unassigned} = [@aUnassigned];
    %hReqFuncs = $self->_HMM_getReqFuncHash;

    # Check whether required domains are present
    foreach my $sAssignment ( keys %hReqFuncs ) {
        my @aRequiredFunctions   = ();
        my @aNotAllowedFunctions = ();
        my $fPfamScore           = 0;
        foreach my $sReqFunction ( @{ $hReqFuncs{$sAssignment} } ) {
            foreach my $sPresFunction ( keys %hPfamHits ) {

                # if a hit is detected add value to @aRequiredFunctions;
                # do this also for negated hits preceedet with "-"
                if ( $sReqFunction eq $sPresFunction ) {
                    push( @aRequiredFunctions, $sReqFunction );
                    $fPfamScore += $hPfamHits{$sPresFunction};
                }
                if ( $sReqFunction eq "-" . $sPresFunction ) {
                    push( @aNotAllowedFunctions, $sReqFunction );
                }
            }
        }

        # Only if number of elements in @aRequiredFunctions and number of
        # required elements from %hReqFunc is equal
        # (I get this number by removing negated elements with grep statement)
        # and @aNotAllowedFunctions is empty do assignment
        if (
            (
                scalar @aRequiredFunctions eq
                scalar( grep ( !/^\-/, @{ $hReqFuncs{$sAssignment} } ) )
            )
            && ( scalar @aNotAllowedFunctions eq 0 )
          )
        {
            $hAssignmentHash{$sAssignment} += $fPfamScore;

            # last;
        }
    }

    # Now we need the data in an AoA
    foreach my $Assignment ( keys %hAssignmentHash ) {
        push @AoAResult, [ ( $Assignment, $hAssignmentHash{$Assignment} ) ];
    }

    # As Sort::ArrayOfArrays requires minimal two assignments we
    # have to check this before execution
    if ( $#AoAResult > 0 ) {

        # Lets set up our two dimensional sorter
        my $oSorter = Sort::ArrayOfArrays->new(
            {
                results     => \@AoAResult,
                sort_column => "-1",
                sort_code => {'-1' => 'na'}
            }
        );
        my @aSortedAoA = @{ $oSorter->sort_it };
        $self->{AssignmentArray} = [@aSortedAoA];
    }

    # Otherwise return unsorted 1 element result
    else {
        $self->{AssignmentArray} = [@AoAResult];
    }
    return @{ $self->{AssignmentArray} };
}

#*****************************************************************
#Method: doAssignment
#**************************************************

=head2 doAssignment;

Title    :  doAssignment

Usage    :  @AoA=$oAssign->doAssignment;

Function :  Evaluates the Blast/HMMer reports and returns an @AoA containing
            the evaluation results

=cut

sub doAssignment {
    my $self    = shift;
    my $sMethod = $self->getAnalysisMethod;
    my @returnValue;

    # Now Lets call the different algorithms
    if ( $sMethod =~ /BlastTab_all/ ) {
        @returnValue = $self->_doBlastMatch;
    }
    elsif ( $sMethod =~ /BlastTab_first/ ) {
        @returnValue = $self->_doBlastMatch;
    }
    elsif ( $sMethod =~ /HMMTab/ ) {
        @returnValue = $self->_doHMMerMatch;
    }
    elsif ( $sMethod =~ /ECpred/ ) {
        @returnValue = $self->_doECpred;
    }
    else {
        die "invalid method specified";
    }
    return (@returnValue);
}

#*****************************************************************
#Method: getUnassigned
#**************************************************

=head2 getUnassigned

Title    : getUnassigned

Usage    : @Array = $oAssign->getUnassigned;

Function : returns an Array of Blast/HMMPfam hits that match no description in
           recogTable

=cut

sub getUnassigned {
    my $self = shift @_;
    if ( !defined $self->{Unassigned} ) {
        $self->doAssignment;
    }
    return ( @{ $self->{Unassigned} } );
}

#*****************************************************************
#Method: getHitArray
#**************************************************

=head2 getHitArray

Title     : getHitArray

Usage     : @AoA=$oAssign->getHitArray;

Function  : returns an AoA containing [HitNo](BlastDesc, E-Value, Score)

=cut

sub getHitArray {
    my $self = shift @_;
    return $self->{HitA};
}

#*****************************************************************
#Method: getAssignmentArray
#**************************************************

=head2 getAssignmentArray

Title    :   getAssignment Array

Function :   returns AssignmentArray

=cut

sub getAssignmentArray {
    my $self = shift @_;
    return $self->{AssignmentArray};
}

#*****************************************************************
#Method: getAssignmentName
#**************************************************

=head2 getAssignmentName

Title    :   getAssignmentName

Usage    :   $Ass=$oAssig->getAssigmentName(0)

Function :   Returns assignment-string
             Parameter: Index of Assignment array, 0 is the most significant
                        hit, default 0

=cut

sub getAssignmentName {
    my ( $self, $iIndex ) = @_;
    if ( !( defined $iIndex ) ) { $iIndex = 0 }
    return $self->{AssignmentArray}->[$iIndex]->[0];
}

#*****************************************************************
#Method: getAssignmentScore
#**************************************************

=head2 getAssignmentScore

Title    :   getAssignmentScore

Usage    :   $Ass=$oAssig->getAssigmentScore(0)

Function :   Returns assignment-score
             Parameter: Index of Assignment array, 0 is the most significant
                        hit, default 0

=cut

sub getAssignmentScore {
    my ( $self, $iIndex ) = @_;
    if ( !( defined $iIndex ) ) { $iIndex = 0 }
    return $self->{AssignmentArray}->[$iIndex]->[1];
}

#*****************************************************************
#Method: getNoOfAssignments
#**************************************************

=head2 getNoOfAssignments

Title    : getNoOfAssignments

Usage    : $no=$oAssig->getNoOfAssignments

Function : returns the highest index of AssignArray (=Number-1)

=cut

sub getNoOfAssignments {
    my $self = shift @_;
    return scalar $#{ $self->{AssignmentArray} };
}

#*****************************************************************
#Method: getAnalysisMethod
#**************************************************

=head2 getAnalysisMethod

Title    : getAnalysisMethod

Usage    : $no=$oAssig->getAnalysisMethod

Function : returns a string of the actual analysis method

=cut

sub getAnalysisMethod {
    my $self = shift;
    if (@_) {
        my $NewMethod = shift;
        $self->{AnalysisMethod} = $NewMethod;
    }
    return $self->{AnalysisMethod};
}

=head2 _get_merged_assignments

Title    : _get_merged_assignments

internal : Imports results table from previous run to assignment object

=cut

sub _get_merged_assignments {
    my $self = shift;
    my %hAssignmentHash;
    my @mergeAssLine;
    if (   ( defined $self->{mergeAss} )
        && ( defined $self->{mergeAss}->[0]->[1] ) )
    {
        foreach my $mergeAssLine ( @{ $self->{mergeAss} } ) {
            $hAssignmentHash{ $$mergeAssLine[0] } = $$mergeAssLine[1];
        }
    }
    else {
        %hAssignmentHash = ();
    }
    return %hAssignmentHash;
}

#********************************************************
# Module end
#********************************************************
#******************************************************************
1;
