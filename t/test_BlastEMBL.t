#!/usr/bin/perl -w
use strict;

use Test::More tests => 7;
use File::Temp qw(tempdir);
use Bio::SeqIO;

my $tmpdir = tempdir("test_BlastEMBL_XXXX",
                     DIR => "t",
                     CLEANUP => 1);

my $cmd = "bin/BlastEMBL.pl --input t/data/prepare.embl --output $tmpdir/output_blastp.embl";

system($cmd);

ok( -e "$tmpdir/output_blastp.embl", "Check if output file was created");

my $seq = Bio::SeqIO->new( '-file' => "$tmpdir/output_blastp.embl", '-format' => 'EMBL')->next_seq;

ok(defined($seq), "Check if output tag is a valid EMBL file");
is(ref($seq), "Bio::Seq::RichSeq", "Check if correct object is parsed from EMBL file");

my ($first) = grep ( $_->primary_tag eq 'CDS', $seq->top_SeqFeatures );

ok(defined($first), "Check if first feature exists");
ok($first->has_tag("blastp_file"), "Check if blastp_file tag can be found");
my ($tag) = $first->get_tag_values("blastp_file");
is($tag, "blastp/PREPARE01.orf1.blastp", "Check if blastp_file tag has correct value");

my $fasta = $tag;
$fasta =~ s/\.blastp$/.fasta/;
ok(-e "$tmpdir/$fasta", "Check if blastp_file exists");

