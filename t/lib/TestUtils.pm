package TestUtils;

sub fixup_config
{
use Config::General;
use Cwd;
my $tmpdir = shift @_;
my $settings = shift @_;
my $config_file = Config::General->new ('-ConfigFile' => "t/data/CLUSEAN.cfg");
my %config = $config_file->getall;

# set the default values
$$settings{'CLUSEANROOT'} = cwd() unless defined($$settings{'CLUSEANROOT'});
$$settings{'CPUS'} = '1' unless defined($$settings{'CPUS'});
$$settings{'BLASTDB'} = 't/data/' unless defined($$settings{'BLASTDB'});
$$settings{'HMMERDB'} = 't/data/' unless defined($$settings{'HMMERDB'});

# Fix up the config file settings
$config{'CLUSEANROOT'} = $$settings{'CLUSEANROOT'};
$config{'Environment'}{'PATH'} = [ $$settings{'CLUSEANROOT'} . "/NRPSPredictor" ];
$config{'Environment'}{'CPUS'} = $$settings{'CPUS'};
$config{'Environment'}{'BLASTDB'} = $$settings{'BLASTDB'};
$config{'Environment'}{'HMMERDB'} = $$settings{'HMMERDB'};
$config{'NRPSPredictor'}{'NRPSBinDir'} = $$settings{'CLUSEANROOT'} . "/NRPSPredictor";

$config_file->save_file("$tmpdir/CLUSEAN.cfg", \%config);
}

1;
