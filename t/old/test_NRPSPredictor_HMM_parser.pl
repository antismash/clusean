#!/usr/bin/perl -w
use strict;
use Data::Dumper;

use Test::Simple tests => 4;

use lib "../NRPSPredictor";

use digestHmmpfamModule;
use digestHmmscanModule;

my @seq_array2;
my %seq_descs2;
my %detected_hmms2;

digestHmmpfam("test_hmmer2.hsr", \@seq_array2, \%seq_descs2, \%detected_hmms2);

print Dumper(\@seq_array2);
print Dumper(\%seq_descs2);
print Dumper(\%detected_hmms2);

my @seq_array3;
my %seq_descs3;
my %detected_hmms3;

digestHmmscan("test_hmmer3.hsr", \@seq_array3, \%seq_descs3, \%detected_hmms3);

print Dumper(\@seq_array3);
print Dumper(\%seq_descs3);
print Dumper(\%detected_hmms3);

my ($len2, $len3) = ($#seq_array2, $#seq_array3);

ok($len2 == $len3);

for (my $i = 0; $i <= $len2; $i++) {
    ok($seq_array2[$i] eq $seq_array3[$i]);
}

my @desc_keys2 = sort(keys(%seq_descs2));
my @desc_keys3 = sort(keys(%seq_descs3));

ok($#desc_keys2 == $#desc_keys3);

for (my $i = 0; $i <= $len2; $i++) {
    ok($desc_keys2[$i] eq $desc_keys3[$i]);
}

