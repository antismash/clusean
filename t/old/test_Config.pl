#!/usr/bin/perl
# $Id$

use strict;
use Config::General;
use Data::Dumper;
use FindBin qw ($Bin $Script $RealBin $RealScript);


my $oConf = Config::General->new('O:/CLUSEAN/etc/CLUSEAN.cfg');

my %ConfHash = $oConf->getall;

print Dumper(\%ConfHash);

print "Testscript $Bin $Script $RealBin $RealScript \n\n";

my $CLUSEANROOT = $Bin;
$CLUSEANROOT =~ s/(.*)\/.*?$/$1/;
print "CLUSEANROOT: $CLUSEANROOT \n";

print "CLUSEANROOT is $ConfHash{'CLUSEANROOT'}\n";
print "Tables available:\n";

foreach my $table (keys %{$ConfHash{RecogTables}}) {
	print "\t$table ===> ",$ConfHash{RecogTables}->{$table},"\n";
}

my %testhash = %{$ConfHash{RecogTables}};

print Dumper (\%testhash);
print $testhash{HMMTab};