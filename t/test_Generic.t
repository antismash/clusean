#!/usr/bin/perl -w

use strict;
use Test::More tests => 12;
use Test::Exception;

use_ok('CLUSEAN::Generic');

my $obj = CLUSEAN::Generic->new;

isa_ok($obj, 'CLUSEAN::Generic');

my @first = ('af', 'b', 'c', 'd', 'e');
my @second = ();

throws_ok(sub { $obj->compareArray('-Array1' => \@first,
                                   '-Array2' => \@second)
              },
          qr/Cannot compare arrays of different sizes/,
          "Comparing arrays of different size fails");

@second = ('f', 'g', 'h', 'i', 'j');

my @res = $obj->compareArray('-Array1' => \@first, '-Array2' => \@second);

is_deeply(\@res, [0, 0, 0, 0, 0], "Mismatching arrays have all-zero result array");

@res = $obj->compareArray('-Array1' => \@first, '-Array2' => \@first);

is_deeply(\@res, [1, 1, 1, 1, 1], "Identical arrays have all-one result array");

@second = ('af', 'f', 'g', 'd', 'h');

@res = $obj->compareArray('-Array1' => \@first, '-Array2' => \@second);
is_deeply(\@res, [1, 0, 0, 1, 0], "On partial matches, only matching elements are set to 1");

$obj = CLUSEAN::Generic->new('-foo' => 23, '-bar' => 42);
isa_ok($obj, 'CLUSEAN::Generic');

ok($obj->_accessible('_foo'), "_foo is accessible");
ok(! $obj->_accessible('_baz'), "nonexisting _baz isn't accessible");

is($obj->get_foo, 23, "getting _foo works");
is($obj->get_bar, 42, "getting _bar works");
$obj->set_bar(17);
is($obj->get_bar, 17, "modifying _bar works");
