#!/usr/bin/perl -w
use strict;

use Test::More tests => 2;
use File::Temp qw(tempdir);
use Cwd;
use lib "t/lib";
use TestUtils;

my $tmpdir = tempdir("test_testPrereq_XXXX",
                     DIR => "t",
                     CLEANUP => 1);

my $oldcwd = cwd();

my %env = ( CPUS => '1',
            BLASTDB => "t/data",
            HMMERDB => "t/data",
            DB => "clusean",
            BLAST_EXECUTABLE => "blastp",
            DIALOG => "dialog",
            PATH => "\${PATH}:$oldcwd/NRPSPredictor" );

TestUtils::fixup_config($tmpdir, \%env);

ok(-e "$tmpdir/CLUSEAN.cfg", "Checking if config file exists");

chdir($tmpdir);

my $cmd = "PERL5LIB=../../ ../../bin/testPrereq.pl CLUSEAN.cfg >/dev/null";
system($cmd);
ok($? == 0, "Check if testPrereq.pl runs successfully");

# leave temp dir so it will be cleaned up
chdir($oldcwd);
