NAME
    annotateNRPSPred.pl

SYNOPSIS
    annotateNRPSPred.pl --input <input-EMBL-file> --output
    <output-EMBL-file> [--tag NRPSPred_file --feattag CDS_motif --csvout
    <CSV-filename> --score 0.1 --verbose --help]

DESCRIPTION
    Enters data contained in CDS associated HMMer result files into
    EMBL-Style annotation.

  PARAMETERS
   --input <filename>                :       Input file (in EMBL-format)
   --output <filename>               :       Output filename (EMBL-format)
   --tag <tag>                       :       Tag used for HMM-result-reference
                                             default: NRPSPred_file
   --feattag <tag>                   :       Tag used for feature annotation
                                             default: misc_feature
       --csvout                                                      :       Filename of CSV-outputfile
       --verbose                         :       verbose output
       --help                            :       print help

   AUTHOR
     Tilmann Weber
     Microbiology/Biotechnology
     Auf der Morgenstelle 28
     72076 Tuebingen
     Germany
     Email: tilmann.weber@biotech.uni-tuebingen.de

