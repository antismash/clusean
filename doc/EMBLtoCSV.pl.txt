NAME
    EMBLtoCSV.pl

SYNOPSIS
    EMBLtoCSV.pl <EMBL-Filename> [<delimiter> --printDNA --printAA
    --printAssTab --printDesc --printBlastP --uniprot --noBlastSkip --excel
    <filename> --paperlist(2) --noHeader --help]

DESCRIPTION
    EMBLtoCSV.pl extracts CDS information of an EMBL file and generates a
    CSV spreadsheet out of the data including DNA and aa sequence,
    description, best BlastHit assigned function,...

    Best used with files generated with ABDB secondary metabolite
    biosynthesis gene annotation pipeline.

OPTIONS
  required parameter
      <EMBL-Filename>     EMBL-File to be processed

  optional parameters
      <delimiter>         Delimiter char used in csv, default "|"

      --printDNA, -d      Include DNA sequence

      --printAA, -p       Include protein sequence

      --printAssTag, -a   Include assignment table

      --printDesc, -t     Include EMBL file description (Bio::Seq->description)

      --printBlastP, -b   Include best BlastP hitbut not hit against own sequence
  
      --noBlastSkip       Don't skip best BlastP-Hit if 100% sequence identiy

      --uniprot, -u  BLAST search was done against UNIPROT DB

      --excel, -e         Export Data as a MS-Excel Spreadsheet. Pleas give filename as parameter

      --paperlist         Generate publication-like table
      --paperlist2

      --noHeader          Don't output headerline

      --help, -h          Show this help Text

AUTHOR
     Dr. Tilmann Weber
     Microbiology/Biotechnology
     University of Tuebingen
     72076 Tuebingen
     Germany

     Email: tilmann.weber@biotech.uni-tuebingen.de

